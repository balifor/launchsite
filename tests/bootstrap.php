<?php
/**
 * Bootstrap the app 
 */
$path = explode(DIRECTORY_SEPARATOR, __DIR__);
array_pop($path);
$site_path = implode(DIRECTORY_SEPARATOR, $path) . DIRECTORY_SEPARATOR;
//Base of site directory
define('SITE', $site_path);
//Base of launchsite directory
define('FRAMEWORK', $site_path);
array_pop($path);
$application_path = implode(DIRECTORY_SEPARATOR, $path) . DIRECTORY_SEPARATOR;
//Base of application
define('PATH', $application_path);

require_once PATH . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require_once FRAMEWORK . 'app' . DIRECTORY_SEPARATOR . 'launcher' . DIRECTORY_SEPARATOR . 'launcher.php';

function bootstrap()
{
	//Get launcher
	$launcher = \Launchsite\launcher\launcher::launch();

	//Bootstrap the app.
	$launcher->bootstrap();

	return $launcher;
}
