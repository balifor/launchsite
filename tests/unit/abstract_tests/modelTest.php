<?php
/**
 * Launcher tests.
 *
 * Test the launcher.
 */

namespace Launchsite\tests;

use \Launchsite\launcher\launcher;

/**
 * Launcher test class.
 *
 * Test the launcher methods.
 */
class launcherTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Check that the launcher starts properly.
	 *
	 * @return launcher The launcher instance.
	 */
	public function test_launch_works()
	{
		//Get the launcher.
		$launcher = launcher::launch();

		//Check the launcher exists.
		$this->assertInstanceOf('\Launchsite\launcher\launcher', $launcher, "The launcher wasn't a subclass of the framework launcher.");

		//Check the start time has been set.
		$start_time = $launcher->start_time;
		$this->assertTrue(is_float($start_time), "Start time isn't a float.");

		//Check the same launcher is returned each time.
		$new_launcher = launcher::launch();

		$this->assertTrue($start_time == $new_launcher->start_time, "The same Launcher isn't being returned each time.");
		$this->assertEquals($launcher, $new_launcher, "The launcher isn't returning the same type of launcher each time.");

		return $launcher;
	}

	/**
	 * Test that the bootstrap runs.
	 *
	 * @depends test_launch_works
	 *
	 * @param launcher $launcher The launchsite instance to test.
	 *
	 * @returns launcher The bootstrapped launcher.
	 */
	public function test_bootstrap_works(launcher $launcher)
	{
		//Bootstrap the app.
		$launcher->bootstrap();

		//check the encoding has been set.
		$this->assertTrue(mb_internal_encoding() === 'UTF-8', "UTF-8 encoding hasn't been set.");
	}

	/**
	 * Test that the default constants needed by launchsite have been set.
	 */
	public function test_set_default_constants_works()
	{
		bootstrap();

		//Check FRAMEWORK is set.
		$this->assertTrue(is_readable(FRAMEWORK), "FRAMEWORK isn't set.");

		//Check SITE is set.
		$this->assertTrue(is_readable(SITE), "SITE isn't set");
	}

	/**
	 * Test that the config files have been loaded.
	 */
	public function test_get_config_files_works()
	{
		$launcher = launcher::launch();

		//Check config files have been loaded.
		$this->assertTrue(isset($launcher->config_files), "No Config files have been loaded.");
		$this->assertTrue(isset($launcher->config_files['CONFIGS']), "Config files not loaded.");
		$this->assertTrue(!empty($launcher->config_files['CONFIGS']), "Config files haven't supplied Config options.");

		//Check the loaded config files exist.
		foreach ($launcher->config_files['CONFIGS'] as $path) {
			$this->assertTrue(is_readable($path), "Not all config files are readable.");
		}
	}

	/**
	 * Test that the debug options have been correctly set up.
	 */
	public function test_set_debug_level_works() 
	{
		$launcher = launcher::launch();

		if ($launcher->get_config('DEBUG')) {
			$this->assertEquals(error_reporting(), $launcher->get_config('DEBUG_LEVEL'), "Error reporting isn't set correctly.");
			$this->assertEquals(ini_get('display_errors'), 'On', "Error reporting hasn't been turned on.");
		} else {
			$this->assertEquals(ini_get('display_errors'), 0, "Error reporting hasn't been turned off.");
		}
	}

	/**
	 * Test that the autoloader has been loaded correctly.
	 */
	public function test_get_autoloader_works()
	{
		$launcher = launcher::launch();

		$this->assertInstanceOf('\Launchsite\launcher\autoloader', $launcher->autoloader, "The autoloader loaded isn't the framework autoloader.");
		$this->assertTrue(is_array($launcher->get_config('FRAMEWORK_AUTOLOAD_PATHS')), "FRAMEWORK_AUTOLOAD_PATHS aren't loaded correctly.");
		$this->assertInstanceOf('\Launchsite\models\user', new \Launchsite\models\user(), "The autoloader isn't loading via psr4.");
		$this->assertInstanceOf('\Launchsite\controllers\user_controller', new \Launchsite\controllers\user_controller(), "The autoloader isn't loading via psr4.");
	}

	/**
	 * Check that the session has been started.
	 */
	public function test_start_session_worked()
	{
		$launcher = launcher::launch();

		//Since its cli it should return false;
		$launcher->start_session();
		$this->assertTrue(isset($_SESSION));
		
		if (null !== ($launcher->get_config('SESSION_TIMEOUT'))) {
			$session_timeout = strtotime('-' . $launcher->get_config('SESSION_TIMEOUT'));

			$this->assertTrue($session_timeout < strtotime('now'), "Session timeout is being set correctly.");
		}
	}

	/**
	 * Test that the routes have been loaded.
	 */
	public function test_load_routes_work()
	{
		$launcher = launcher::launch();

		$this->assertTrue(count($launcher->get_engine('routing')->get_routes()) > 1);
	}

	/**
	 * Test the database has been loaded.
	 */
	public function test_get_db_works()
	{
		$launcher = launcher::launch();

		//No DB setup in framework, so it returns null.
		$this->assertEquals(null, $launcher->get_db(), "Shouldn't have returned a DB.");

		//Test Exception occurs on bad DSN.
		$launcher->set_config('DB_BAD', array(
					'TYPE' => 'mysql',
					'HOST' => '127.0.0.1',
					'NAME' => 'launchsite',
					'USER' => 'root',
					'PASS' => 'asdgasdbgasdga',
					));

		//Check bad data throws a \PDOException.
		try {
			$launcher->get_db('BAD');
		} catch (\PDOException $e) {
			$this->assertInstanceOf('\PDOException', $e, "Bad DB connection strings aren't throwing errors.");
		}

		//Try a known good database.
		$launcher->set_config('DB_MAIN', array(
					'TYPE' => 'mysql',
					'SOCKET' => '/Applications/MAMP/tmp/mysql/mysql.sock',
					'NAME' => 'launchsite',
					'USER' => 'root',
					'PASS' => 'root',
					));

		$this->assertInstanceOf('Launchsite\interfaces\database_engine', $launcher->get_db('MAIN'), "Didn't return a DB.");
	}

	/**
	 * Check that a request can be handled.
	 */
	public function test_handle_request_works()
	{
		$launcher = launcher::launch();

		$test = $launcher->handle_request('/index.php');

		$this->assertTrue(is_array($test), "Handle request didn't return true.");

		$this->assertTrue(strlen($launcher->get_engine('templating')->page_content) > 1);
	}
}
