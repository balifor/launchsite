<?php
/**
 * PDO database model.
 *
 * wrapper for PDO db connections.
 */

namespace Launchsite\databases;

/**
 * PDO_DB class.
 *
 * PDO DB implementation.
 */
class pdo_db extends \Launchsite\abstracts\core 
{
	/**
	 * Active Database Instance.
	 *
	 * @var db.
	 */
	public $db;

	/**
	 * Name to find db by.
	 *
	 * @var string.
	 */
	public $name;

	/**
	 * Details array.
	 *
	 * @var array.
	 */
	public $details;

	/**
	 * Database username.
	 *
	 * @var string.
	 */
	private $user;

	/**
	 * Database username password.
	 *
	 * @var string.
	 */
	private $pass;

	/**
	 * Prepared statement, if any.
	 *
	 * @var \PDOStatement.
	 */
	public $prepared = false;

	/**
	 * Variables for the prepared statement, if any.
	 *
	 * @var array.
	 */
	public $prepared_vars = false;

	/**
	 * The last query that was run.
	 *
	 * @var \PDOStatement.
	 */
	public $last_query;

	/**
	 * Connect to the database.
	 *
	 * @return db Return the database instance.
	 */
	public function get_connection($to_db = true)
	{
		try {
			if ($this->type == 'pgsql') {
				$this->db = new \PDO($this->make_dsn($to_db));
			} else {
				$this->db = new \PDO($this->make_dsn($to_db), $this->user, $this->pass);
			}

			$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

			return $this->db;
		} catch (\PDOException $e) {
			$this->get_engine('response')->add_error("Couldn't connect to the database: " . $e->getMessage(), 500, 'db_connect_error.php');
			throw $e;
		}
	}

	/**
	 * Generate SQL from an array.
	 *
	 * @param string $table_to_use The table to run the select on.
	 *
	 * @param array $options An array of where statements, empty for select * from $table_to_use.
	 *
	 * @return array A generated paramatised SQL statement and parameters.
	 */
	public function generate_select($table_to_use, $options = false)
	{
		$select = '';
		$joins = '';
		$group = '';
		$order = '';
		$limit = '';
		$offset = '';
		$where_sql = '';

		if ($options === false || empty($options)) {
			$select = 'SELECT * FROM ' . $table_to_use;

			return array($select, array());
		} else {
			$values = array();
			foreach ($options as $column => $value) {
				switch ($column) {
					//Select cloumns to return
					case 'COLUMNS':
						if (is_string($value)) {
							$select = $value;
						} elseif (is_array($value) && empty($value)) {
							$select_unformatted = implode(", ", $value);
							$select = rtrim($select_unformatted, ', ');
						} else {
							$select = '*';
						}
						break;

						//Join statements
					case in_array($column, array('JOIN', 'LEFT JOIN', 'RIGHT JOIN', 'INNER JOIN', 'OUTER JOIN')):
						$joins .= $column . PHP_EOL;
						$joins .= $value . PHP_EOL;
						break;

						//Group By statement
					case 'GROUP':
						$group .= 'GROUP BY' . PHP_EOL;
						if (is_array($value)) {
							foreach ($value as $grouping) {
								$grouping = rtrim($grouping, ',');
								$group .= $grouping . ',' . PHP_EOL;
							}
							$group = rtrim($group, ',');
						} else {
							$group .= $value . PHP_EOL;
						}
						break;

						//Order By statement
					case 'ORDER':
						$order .= 'ORDER BY' . PHP_EOL;
						if (is_array($value)) {
							foreach ($value as $ordering) {
								$ordering = rtrim($ordering, ',');
								$order .= $ordering . ',' . PHP_EOL;
							}
							$order = rtrim($order, ',');
						} else {
							$order .= $value . PHP_EOL;
						}
						break;

						//Limit Statement
					case 'LIMIT' :
						$limit .= 'LIMIT ' .  $value . PHP_EOL;
						break;

						//Offset
					case 'OFFSET' :
						$offset .= 'OFFSET ' . $value . PHP_EOL;
						break;

					case 'SQL':
						if (count($value)) {
							foreach ($value as $val) {
								if (is_array($val)) {
									if (isset($val[1]) && !empty($val[1])) {
										foreach ($val[1] as $v) {
											$values[] = $v;
										}
									}

									$where_sql .= <<<SQL

									{$val[0]}
									AND
SQL;
								} else {
									$where_sql .= <<<SQL

									{$val}
									AND
SQL;
								}
							}
						}

						$where_sql = substr(rtrim($where_sql), 0, -3) . PHP_EOL;

						break;

					default :
						if (is_array($value)) {
							$operator = $value[0];
							if (strtolower($operator) == 'in') {
								$values[] = "(" . $value[1] . ")";
							} else {
								$values[] = $value[1];
							}

							$where_sql .= <<<SQL

							{$column} {$operator} ?
							AND
SQL;
						} else {
							$values[] = $value;

							$where_sql .= <<<SQL

							{$column} = ?
							AND
SQL;
						}
				}
			}

			$pre_generated_where_sql = trim($where_sql);
			$generated_where_sql = substr($pre_generated_where_sql, - strlen(PHP_EOL . 'AND')) == PHP_EOL . 'AND' || substr($pre_generated_where_sql, - strlen('	AND')) == '	AND' || substr($pre_generated_where_sql, - strlen(' AND')) == ' AND' ? substr(rtrim($pre_generated_where_sql), 0, -3) : $pre_generated_where_sql;
			$where_statement = strlen($generated_where_sql) ? 'WHERE' . PHP_EOL . $generated_where_sql : '';
			$select_sql = strlen($select) ? $select : '*';

			//Generate the SQL statement
			$sql = <<<SQL
				SELECT
				{$select_sql}
			FROM
			{$table_to_use}
			{$where_statement}
			{$group}
			{$order}
			{$limit} {$offset}
SQL;

			return array(trim($sql), $values);
		}
	}

	/**
	 * Get a table's columns.
	 *
	 * @param string $table The table to get the column names from.
	 *
	 * @return bool|array Returns false on failure to get columns or columns on success.
	 */
	public function get_columns($table)
	{
		//Try getting a cached version
		$json_columns = $this->get_engine('cache')->get_cached('models/' . $table . '_columns', 3600);

		if ($json_columns) {
			$columns = json_decode($json_columns);
		} else {
			$sql = "select column_name from INFORMATION_SCHEMA.COLUMNS where table_name = '$table' AND table_schema = '{$this->details['NAME']}';";
			$result_set = $this->query($sql);
			$result = $result_set->fetchALL(\PDO::FETCH_ASSOC);

			if ($result == false) {
				return false;
			} else {
				$columns = array();
				foreach ($result as $set) {
					foreach ($set as $column) {
						$columns[] = $column;
					}
				}

				$cache = json_encode($columns);
				$this->get_engine('cache')->cache('models/' . $table . '_columns', $cache);
			}
		}

		return $columns;
	}

	/**
	 * PDO database select.
	 *
	 * @param string $sql Paramatised SQL statement.
	 *
	 * @param array @vars An array of parameters.
	 *
	 * @return bool|array Returns an array of results on success, false on failure.
	 */
	public function select($sql, $vars = false)
	{
		$result = $this->prepared_query($sql, $vars);
		return $result->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Insert into a table.
	 *
	 * @param array $vars A key value array of column => value.
	 *
	 * @param string $table The table to insert into.
	 *
	 * @throws \PDOException if there is an error in the SQL provided.
	 *
	 * @return bool true on success.
	 */
	public function insert($vars, $table)
	{
		$columns_string = '';
		$values_string = '';

		$values = array();
		foreach ($vars as $column => $value) {
			$columns_string .= $column . ',';
			$values_string .= "?" . ',';
			$values[] = $value;
		}

		$columns_sql = rtrim($columns_string, ',');
		$values_sql = rtrim($values_string, ',');

		$sql = <<<SQL
			INSERT INTO {$table}
		({$columns_sql})
			VALUES
			({$values_sql});
SQL;

		$this->prepared_query($sql, $values);

		return true;
	}

	/**
	 * Update a table row. Must include ID.
	 *
	 * @param $vars Where statment in array column => value.
	 *
	 * @param $table The table to run the update on.
	 *
	 * @throws \PDOException If there is a problem with the SQL provided.
	 *
	 * @return true on success.
	 */
	public function update($vars, $table)
	{
		$set_string = '';
		$values = array();
		foreach ($vars as $column => $value) {
			$set_string .= $column . ' = ?, ';
			$values[] = $value;
		}

		$set_sql = rtrim($set_string, ', ');

		$sql = <<<SQL
			UPDATE {$table}
		SET
		{$set_sql}
		WHERE
			id = {$vars['id']};
SQL;

		$this->prepared_query($sql, $values);

		return true;
	}

	/**
	 * Run a PDO query.
	 *
	 * @param string $sql The SQL to run.
	 *
	 * @param array $vars The parameters values.
	 *
	 * @throws \PDOException if there is a problem with the SQL provided.
	 *
	 * @return array An array of results on success, error on failure.
	 */
	public function query($sql, $values = false)
	{
		return $this->prepared_query($sql, $values);
	}

	/**
	 * Prepare a query, execute it and return the result.
	 *
	 * @param string $sql The paramatised SQL to run.
	 *
	 * @param array $vars The parameters values.
	 *
	 * @return array An array of results on success, error on failure.
	 */
	public function prepared_query($sql, $vars = false)
	{
		if (!$vars) {
			$vars = array();
		}

		try {
			$this->prepared_vars = $vars;
			$this->prepared = $this->db->prepare($sql);
			$this->prepared->execute($this->prepared_vars);

			$this->last_query = array($sql, $vars);
			return $this->prepared;
		} catch (\PDOException $e) {
			$this->last_query = array($sql, $vars);
			$this->get_engine('response')->add_error("Problem with the SQL query: " . PHP_EOL . $sql . PHP_EOL . "Using values: " . print_r($vars, 1) . PHP_EOL . "Led to: " . $e->getMessage());
			throw $e;
		}
	}

	/**
	 * Run a select and turn results into objects.
	 *
	 * @param string $class The name of the class to return results as.
	 *
	 * @param string $query The query string to get data.
	 *
	 * @param array $vars An array of paramaters for the SQL statement.
	 *
	 * @return array Returns an array of the results as objects.
	 */
	public function objectify($class, $query, $vars = false)
	{
		$result = $this->select($query, $vars);

		if (empty($result)) {
			return $result;
		} else {
			$objects_array = array();
			foreach ($result as $row) {
				if (is_object($class)) {
					$class = get_class($class);
				}

				if (class_exists($class)) {
					$obj = new $class();
					$obj->set($row);
					$objects_array[] = $obj;
				} else {
					$this->get_engine('response')->add_error("Couldn't find a class: $class");
					throw new \Exception("class: $class doesn't exist.");
					break;
				}
			}

			return $objects_array;
		}
	}

	/**
	 * Begin a transaction.
	 *
	 * @return bool.
	 */
	public function begin()
	{
		try {
			$this->db->beginTransaction();
			$this->transaction = true;
			return true;
		} catch (\PDOException $e) {
			$this->get_engine('response')->add_error("Couldn't begin transation: " . $e->getMessage());
			throw $e;
		}
	}

	/**
	 * Commit a transaction.
	 *
	 * @return bool.
	 */
	public function commit()
	{
		try {
			$this->db->commit();
			$this->transaction = false;
			return true;
		} catch (\PDOException $e) {
			$this->get_engine('response')->add_error("Couldn't commit transation: " . $e->getMessage());
			throw $e;
		}
	}

	/**
	 * Roll back a transaction.
	 *
	 * @return bool.
	 */
	public function rollback()
	{
		try {
			$this->db->rollBack();
			$this->transaction = false;
			return true;
		} catch (\PDOException $e) {
			$this->get_engine('response')->add_error("Couldn't commit transation: " . $e->getMessage());
			throw $e;
		}
	}

	/**
	 * Set details
	 *
	 * @param $name The name of the database
	 *
	 * @param $details The connection details array
	 */
	public function set_details($name, $details)
	{
		$this->name = $name;
		$this->details = $details;
		$this->type = $details['TYPE'];
		$this->user = $details['USER'];
		$this->pass = $details['PASS'];
	}

	/**
	 * Create the DSN
	 *
	 * @return string The DSN to use.
	 */
	private function make_dsn($to_db = true)
	{
		$dsn = $this->details['TYPE'] . ':';
		if (isset($this->details['SOCKET']) && is_readable($this->details['SOCKET'])) {
			$dsn .= "unix_socket={$this->details['SOCKET']}";
		} else {
			$dsn .= isset($this->details['HOST']) ? "host={$this->details['HOST']}" : '';
			$dsn .= isset($this->details['PORT']) ? ";port={$this->details['PORT']}" : '';
		}

		if ($to_db) {
			$dsn .= isset($this->details['NAME']) ? ";dbname={$this->details['NAME']}" : '';
		}

		if ($this->type == 'pgsql') {
			$dsn .= ';user=' . $this->user . ';password=' . $this->pass;
		}

		return $dsn;
	}

	/**
	 * Get a skeleton for the database.
	 *
	 * @param string $name The name of the database to create a skeleton for.
	 *
	 * @returns string A sql staement to create the database skeleton.
	 */
	public function db_skeleton_query($name)
	{
		return <<<SQL

CREATE DATABASE {$name};

USE {$name};

CREATE TABLE users (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	firstname VARCHAR(50),
	lastname VARCHAR(50),
	username VARCHAR(50) NOT NULL UNIQUE,
	email VARCHAR(50) NOT NULL UNIQUE,
	email_verified BOOLEAN DEFAULT FALSE,
	password VARCHAR(255) NOT NULL,
	middle_name TEXT,
	gender TEXT,
	mobile_phone TEXT,
	landline TEXT,
	token TEXT,
	token_time INT(11),
	created_at INT(11) NOT NULL,
	created_by_id INT NOT NULL,
	deleted BOOLEAN DEFAULT 0,
	deleted_at INT(11),
	deleted_by_id INT
);

CREATE TABLE auths (
	id INT(32) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30) UNIQUE
);

CREATE TABLE users_auths (
	user_id INT(32) UNSIGNED,
	auth_id INT(6) UNSIGNED
);

CREATE TABLE auth_groups (
	id INT(32) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(30) UNIQUE
);

CREATE TABLE auth_group_auths (
	auth_id INT(32) UNSIGNED,
	auth_group_id INT(32) UNSIGNED
);

CREATE TABLE users_auth_groups (
	user_id INT(32) UNSIGNED,
	auth_group_id INT(32) UNSIGNED
);

CREATE TABLE api_clients (
	id INT(32) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	secret VARCHAR(80) UNIQUE,
	name VARCHAR(80),
	user_id INT(32) UNSIGNED
);

CREATE TABLE api_tokens (
	id INT(32) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	client_id INT(32) UNSIGNED,
	issued INT(11) UNSIGNED,
	expires INT(11) UNSIGNED,
	token VARCHAR(50)
);

CREATE TABLE client_auths (
	client_id INT(32) UNSIGNED,
	auth_id INT(6) UNSIGNED
);

CREATE TABLE client_auth_groups (
	client_id INT(32) UNSIGNED,
	auth_group_id INT(32) UNSIGNED
);

CREATE TABLE images (
	id INT(32) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	uploader_id INT(32) UNSIGNED,
	type VARCHAR(50),
	extension VARCHAR(10),
	name VARCHAR(255),
	path VARCHAR(255),
	private BOOLEAN DEFAULT FALSE
);

CREATE TABLE downloads (
	id INT(32) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	uploader_id INT(32) UNSIGNED,
	type VARCHAR(50),
	extension VARCHAR(10),
	name VARCHAR(255),
	path VARCHAR(255),
	private BOOLEAN DEFAULT FALSE
);

SQL;
	}

	/**
	 * Get the migration skeleton
	 */
	public function migration_skeleton_query()
	{
		return <<<SQL

CREATE TABLE launchsite_info (
	version INT(6)
);

INSERT INTO launchsite_info (version) VALUES (0);

SQL;
	}

	/**
	 * Drop a database
	 *
	 * @param string $name The name of the database to drop
	 */
	public function drop_db($name)
	{
		$sql = "DROP DATABASE $name;";
		$this->db->query($sql);
	}

	/**
	 * Get the migration version
	 *
	 * @param string $name The name of the table to get migration version from
	 */
	public function get_migration_version_query($table)
	{
		return "SELECT version FROM $table LIMIT 1;";
	}

	/**
	 * Set the migration version
	 *
	 * @param string $name The name of the table to set migration version for
	 */
	public function set_migration_version_query($table)
	{
		return "UPDATE $table SET version = ?;";
	}

	/**
	 * Show a list of databases on the current host.
	 *
	 * @return array A list of databases on the host.
	 */
	public function show_databases()
	{
		if ($this->type == 'mysql') {
			$sql = 'SHOW DATABASES;';
		} else if ($this->type == 'pgsql') {
			$sql = '\list';
		} else {
			$sql = 'SHOW DATABASES;';
		}

		$result = $this->db->query($sql);
		return $result->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Show a list of tables on the current database.
	 *
	 * @return array A list of the tables and columns in a database.
	 */
	public function show_tables() 
	{
		if ($this->type == 'mysql') {
			$sql = 'SHOW TABLES;';
		} else if ($this->type == 'pgsql') {
			$sql = '\dt';
		} else {
			$sql = 'SHOW TABLES;';
		}

		$result = $this->db->query($sql);
		$tables = $result->fetchAll(\PDO::FETCH_ASSOC);		

		$return = array();
		if (count($tables)) {
			foreach ($tables as $table) {
				$table_name = end($table);
				$sql = "DESCRIBE " . $table_name . ';';
				$result = $this->db->query($sql);
				$columns = $result->fetchAll(\PDO::FETCH_ASSOC);		

				$return[$table_name] = $columns;
			}
		}

		return $return;
	}
}
