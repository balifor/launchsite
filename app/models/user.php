<?php
/**
 * User Model.
 *
 * Implements a basic user system with auths and auth groups.
 */

namespace Launchsite\models;

/**
 * User class.
 *
 * Class to allow for users on the site.
 */
class user extends \Launchsite\abstracts\model
{
	/**
	 * A list of the users authorisations. 
	 *
	 * @var array A list of all auths the user has, including ones from groups.
	 */
	public $auths = array();

	/**
	 * A list of auth groups the user belongs to.
	 *
	 * @var array A list of all auth groups the user belongs to.
	 */
	public $auth_groups = array();

	/**
	 * DB table.
	 *
	 * @var string The db table name.
	 */
	public $table = 'users';

	/**
	 * Check if the user is authorised to view a page.
	 *
	 * @param string $required_auth A string for the name of the auth to be checked.
	 *
	 * @param bool $group Bool flag to say whether the auth is a direct auth or auth group.
	 *
	 * @return true if authed, false if not.
	 */
	public function authed($required_auth, $group = false)
	{
		if ($group !== false) {
			$auths = $this->get_auth_groups();
		} else {
			$auths = $this->get_auths();
		}

		return in_array($required_auth, $auths) ? true : false;
	}

	/**
	 * Get a list of the users auths.
	 *
	 * @param bool $force_update Normally auths are stored on the user once loaded from db, this forces the user to recheck DB.
	 *
	 * @return array An array of the users authorisations.
	 */
	function get_auths($force_update = false)
	{
		if (empty($this->auths) || $force_update === true) {
			$auth = new auth();
			$direct_auths = $auth->get_auths($this->get('id', 0));

			$auth_group = new auth_group();
			$auth_group_auths = $auth_group->get_auth_group_auths($this->get('id', 0));

			$auths = array_merge($direct_auths, $auth_group_auths); 
			$auth_list = array();
			if (count($auths)) {
				foreach ($auths as $auth) {
					$auth_list[] = $auth['name'];
				}
			}

			$this->auths = array_unique($auth_list);

			return $this->auths;
		} else {
			return $this->auths;
		}
	}

	/**
	 * Get a list of the users auth groups.
	 *
	 * @param bool $force_update Normally auth_groups are stored on the user once loaded from db, this forces the user to recheck DB.
	 *
	 * @return array A list of the auth groups the user is assigned to.
	 */
	function get_auth_groups($force_update = false)
	{
		if (empty($this->auth_groups) || $force_update === true) {
			$auth_group = new auth_group();
			$auth_groups = $auth_group->get_auth_groups($this->get('id', 0));

			$auth_group_list = array();
			if (count($auth_groups)) {
				foreach ($auth_groups as $group) {
					$auth_group_list[] = $group['name'];
				}
			}

			$this->auth_groups = $auth_group_list;

			return $this->auth_groups;
		} else {
			return $this->auth_groups;
		}
	}

	/**
	 * Check a login
	 *
	 * @param string login_value The value to login with
	 *
	 * @param string password The password to match
	 *
	 * @param string field_name the field name the value corresponds to.
	 * 
	 * @return bool  
	 */
	function check_login($value, $pass, $field = 'email')
	{
		$result = array(
			'success' => false,
			'message' => ''
		);

		try {
			$users = $this->find(array($field => $value), $this->get_engine('user')->get_user_class());
		} catch (\PDOException $e) {
			$this->get_engine('logging')->log($this->get_engine('logging')->get_details($e), 'db_errors.txt');
			$result['message'] = "There's a problem with the database. Try again later";

			return $result;
		}

		if(empty($users)) {
			$result['message'] = 'Your email or password is wrong';
		} elseif (count($users) === 1) {
			if (password_verify($_POST['pass'], $users[0]->get('password'))) {
				$this->get_engine('user')->set_current_user($users[0]);

				$result['success'] = true;
			} else {
				$result['message'] = 'Your email or password is wrong';
			}
		} elseif (count($users) > 1) {
			$log = "Problem with the users table, there's multiple users with the same email address" . PHP_EOL;
			$log .= print_r($user, 1) . PHP_EOL;
			$this->get_engine('logging')->log($log, 'users.txt');

			$result['message'] = "There's a problem with your account, we'll get back to you as soon as possible";
		}

		return $result;
	}

	/**
	 * Load current user
	 *
	 * @return object user The instance of the user class
	 */
	function load_current_user()
	{
		if (isset($_SESSION['current_user_id']) && is_numeric($_SESSION['current_user_id'])) {
			try {
				$user = $this->find(array('id' => $_SESSION['current_user_id']), $this->get_engine('user')->get_user_class());
				
				$user_count = count($user);
				if ($user_count == 1) {
					return $user[0];
				} else {
					return false;
				}

			} catch (\PDOException $e) {
				$this->get_engine('logging')->log($this->get_engine('logging')->get_details($e), 'db_errors.txt');
				$vars['load_user_error'] = "There's a problem with the database. <br /> Try again later";
			}
		}

		return false;
	}
}
