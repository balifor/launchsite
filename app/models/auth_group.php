<?php
/**
 * Auth Groups for auth engine.
 *
 * Auth groups allow an easy way to make user roles.
 */

namespace Launchsite\models;

/**
 * Auth Group class.
 *
 * Allows grouping auths.
 */
class auth_group extends \Launchsite\abstracts\model 
{
	/**
	 * The database table for the class.
	 *
	 * @var string.
	 */
    public $table = 'auth_groups';
    
	/**
	 * Load a users auth groups.
	 *
	 * @param int $user_id The user id to get auth groups for.
	 *
	 * @throws \PDOException If the table doesn't exist.
	 *
	 * @return array Returns an array of auth groups.
	 */
    function get_auth_groups($user_id)
    {
        $sql = <<<SQL
    SELECT
        ag.*
    FROM
        users_auth_groups uag
    LEFT JOIN
        auth_groups ag on ag.id = uag.auth_group_id
    WHERE
        uag.user_id = {$user_id}
SQL;

        $auths = $this->select($sql);

        return $auths;            
    }
    
	/**
	 * Load a users auth group auths.
	 * 
	 * @param int $user_id The user id to load auth group auths.
	 *
	 * @return array An array of auths that users auth groups have.
	 */
    function get_auth_group_auths($user_id)
    {
        $auth_groups = $this->get_auth_groups($user_id);
        
        $auth_group_list = array();
        if (count($auth_groups)) {
            foreach ($auth_groups as $group) {
                $auth_group_list[] = $group['id'];
            }
        }
        
        if (count($auth_group_list)) {
            $in_string = implode(',', $auth_group_list);
            
            $sql = <<<SQL
SELECT
    a.name
FROM
    auths a
LEFT JOIN
    auth_group_auths aga on aga.auth_id = a.id
WHERE
    aga.auth_group_id in ({$in_string});
SQL;

            $auths = $this->select($sql);
        } else {
            $auths = array();
        }

        return $auths;            
    }
}
