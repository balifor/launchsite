<?php
/**
 * Migration model.
 *
 * Migration functionality
 */

namespace Launchsite\models;

use \Exception;

/**
 * Migration class.
 *
 * Functions for migrations.
 */
class migration extends \Launchsite\abstracts\model 
{
	/**
	 * The database table for the class.
	 *
	 * @var string.
	 */
    public $table = 'launchsite_info';

	/**
	 * Override the save function.
	 */
	public function save($db = '') {}

	/**
	 * Get the current migration version number
	 */
	public function get_current_version($db_to_use = 'MAIN')
	{
		$query = $this->get_engine('database')->get_db($db_to_use)->get_migration_version_query($this->table);

		try {
			$result = $this->get_engine('database')->get_db($db_to_use)->select($query);

			if (isset($result[0]['version'])) {
				return $result[0]['version'];
			} else {
				$message = "Couldn't find version number in $this->table.";  
				$this->get_engine('response')->add_error($message, 500, 'db_error.php');
				throw new Exception($message);
			}
		} catch (Exception $e) {
			$this->get_engine('response')->add_error("DB error when trying to find the version number: " . $e->getMessage(), 500, 'db_error.php');
			throw $e;
		}
	}

	/**
	 * Set the current version.
	 *
	 * @param $version the version to set as.
	 */
	public function set_current_version($version, $db_to_use = 'MAIN')
	{
		$query = $this->get_engine('database')->get_db($db_to_use)->set_migration_version_query($this->table);
		$vars = array($version);

		try {
			$this->get_engine('database')->get_db($db_to_use)->prepared_query($query, $vars);
			return true;
		} catch (PDOException $e) {
			$message = "There was an error attempting to update to: $version; " . $e->getMessage(); 
			$this->get_engine('response')->add_error($message, 500, 'db_error.php');
			throw $e;
		}
	}

	/**
	 * Get max available version.
	 */
	public function get_max_available()
	{
		$site_migrations = $this->launchsite()->get_config('MIGRATIONS'); 
		if (is_readable($site_migrations)) {
			$migrations = scandir($site_migrations, SCANDIR_SORT_DESCENDING);
			if (in_array($migrations[0], array('.', '..', '.gitkeep'))) {
				$version = '0';
			} else {
				$version = $migrations[0];
				foreach(array('migration_', '.php') as $replace) {
					$version = str_replace($replace, '', $version);
				}
			}

			return $version;
		} else {
			$message = "Couldn't access migrations directory at: " . $site_migrations; 
			$this->get_engine('response')->add_error($message, 500, 'no_directory.php');
			throw new Exception($message);
		}
	}

	/**
	 * Get a range of migrations.
	 *
	 * @param int $beginning The beginning of the range. 
	 *
	 * @param int $end The end of the range.
	 *
	 * @return array $migrations a list of migration objects.
	 */
	public function get_migrations($beginning, $end)
	{
		$max_version = $this->get_max_available();

		if ($beginning > $max_version || $end > $max_version) {
			$message = "Max migration available is: $max_version";
			$this->get_engine('response')->add_error($message, 500, 'no_file.php');
			throw new Exception($message);
		}

		$site_migrations = $this->launchsite()->get_config('MIGRATIONS'); 
		$migrations = scandir($site_migrations, SCANDIR_SORT_ASCENDING);

		//Remove . and ..
		array_shift($migrations);
		array_shift($migrations);

		$instances = array();
		foreach (range($beginning, $end) as $version) {
			if ($version > 0) {
				$instance_version = 'migration_' . $version . '.php';
				if(in_array($instance_version, $migrations)) {
					require_once $site_migrations . $instance_version;
					$instance_class = str_replace('.php', '', $instance_version);
					$instances[$version] = new $instance_class();
				} else {
					$message = "Couldn't find migration version: $version"; 
					$this->get_engine('response')->add_error($message, 500, 'no_file.php');
					throw new Exception($message);			
				}
			}
		}

		return $instances;
	}

	/**
	 * Run an update migration.
	 */
	public function update()
	{
		throw new Exception("No update method.");
	}

	/**
	 * Run a rollback migration.
	 */
	public function rollback()
	{
		throw new Exception("No rollback method.");
	}
}
