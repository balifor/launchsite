<?php
/**
 * An asset model.
 */

namespace Launchsite\models;

/**
 * Asset model class.
 */
class asset extends \Launchsite\abstracts\model 
{
	/**
	 * The database table for the class.
	 *
	 * @var string.
	 */
    public $table = 'assets';

	public function get_info($type, $id) 
	{
		$info = $this->launchsite()->get_config('ASSET_INFO');

		if (!isset($info[$type]) || !isset($info[$type]['table'])) {
			return false;
		}

		$assets = $this->find(array('id' => $id), false, $info[$type]['table']);

		if (!empty($assets)) {
			return $assets[0];
		}

		return false;
	}

	public function get_asset_path($type, $id, $type_vars = array())
	{
		$info = $this->launchsite()->get_config('ASSET_INFO');

		if (!isset($info[$type]) || !isset($info[$type]['path'])) {
			return false;
		}

		$type_path = $info[$type]['path'];

		if (!empty($type_vars)) {
			$type_path_parts = explode(DIRECTORY_SEPARATOR, $type_path);
			if (!empty($type_path_parts)) {
				foreach($type_path_parts as $key => $part) {
					if (isset($part[0]) && $part[0] == ':') {
						$part_parts = explode(':', $part);
						if (isset($part_parts[1]) && $type_vars[$part_parts[1]]) {
							$type_path_parts[$key] = $type_vars[$part_parts[1]];
						}
					}
				}
			}

			$type_path = implode(DIRECTORY_SEPARATOR, $type_path_parts) . DIRECTORY_SEPARATOR;
		}

		$path = $type_path . $id;
		if (file_exists($path)) {
			return $path;
		}

		return false;
	}
}
