<?php
/**
 * Calendar model.
 */

namespace Launchsite\models;

use \DateTime;

/**
 * Calendar class.
 */
class calendar extends \Launchsite\abstracts\model
{
	private $datetime = false;

	public function __construct($date) 
	{
		$this->datetime = $date;
	}

	public function get_day() 
	{
		return (int) $this->datetime->format('j');
	}

	public function get_week_day() 
	{
		return (int) $this->datetime->format('N');
	}

	public function get_first_week_day() 
	{
		$year = $this->get_current_year();
		$month = $this->get_current_month();
		$date = (new DateTime())->setDate($year, $month, 1);
		$current = $date->format('N'); 

		return (int) $current;
	}

	public function get_number_of_days_in_this_month() 
	{
		return (int) $this->datetime->format('t');
	}

	public function get_number_of_days_in_previous_month() 
	{
		$year = $this->get_current_year();
		$month = $this->get_current_month();

		$date = (new DateTime())->setDate($year, $month, 1);
		$date->modify('-1 month');

		return (int) $date->format('t');
	}

	public function get_first_week() 
	{
		$year = $this->get_current_year();
		$month = $this->get_current_month();

		return $this->get_days_in_week($year, $month, 1);
	}

	public function get_calendar() 
	{
		$month_array = array();

		$year = $this->get_current_year();
		$month = $this->get_current_month();
		$day = $this->get_day();
		$date = (new DateTime())->setDate($year, $month, 1);
		$previous_week = $this->get_previous_week();
		$c_month = $month;
		
		while ($c_month === $month) {
			$week = (int) $date->format('W');
			$day = $date->format('j');
			$week_array = $this->get_days_in_week($year, $month, $day);
			
			if ($week === $previous_week) {
				foreach($week_array as $key => $value) {
					$week_array[$key] = true;
				}
			}

			$month_array[$week] = $week_array;
			$date->modify('+7 days');
			$c_month = (int) $date->format('n');
		}

		$last_keys = array_keys($week_array);
		if (end($last_keys) < $this->get_number_of_days_in_this_month() && !in_array(1, $last_keys)) {
			$week = (int) $date->format('W');
			$day = $date->format('j');
			$week_array = $this->get_days_in_week($year, $c_month, $day);
			$month_array[$week] = $week_array;
		}

		return $month_array;
	}

	public function get_current_year() 
	{
		return (int) $this->datetime->format('Y');
	}

	public function get_current_month() 
	{
		return (int) $this->datetime->format('n');
	}

	public function get_current_week() 
	{
		return (int) $this->datetime->format('W');
	}

	public function get_previous_month() 
	{
		$year = $this->get_current_year();
		$month = $this->get_current_month();

		$date = (new DateTime())->setDate($year, $month, 1);
		$date->modify('-1 month');
		
		return (int) $date->format('n');
	}

	public function get_previous_week() 
	{
		$year = $this->get_current_year();
		$month = $this->get_current_month();
		$day = $this->get_day();

		$date = (new DateTime())->setDate($year, $month, $day);
		$date->modify('-1 week');

		return (int) $date->format('W');
	}

	public function get_days_in_week($year, $month, $day) 
	{
		$date = (new DateTime())->setDate($year, $month, $day);
		$week = $date->format('W');
		$c_week = $week;
		
		while($c_week === $week) {
			$date->modify('-1 day');
			$c_week = $date->format('W');
		}
		
		$week_array = array();
		while(count($week_array) < 7) {
			$date->modify('+1 day');
			$c_day = $date->format('j');
			$week_array[$c_day] = false;
		}
		
		return $week_array;
	}
}
