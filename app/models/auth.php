<?php
/**
 * Auth engine.
 *
 * An individual authorisation.
 */

namespace Launchsite\models;

/**
 * Auth class.
 *
 * Framework class to allow a requirement of authorisation.
 */
class auth extends \Launchsite\abstracts\model
{
	/**
	 * Model table.
	 *
	 * @var string.
	 */
	public $table = 'auths';

	/**
	 * Get a list of the users authorisations.
	 *
	 * @param int $user_id The user id to get auths for.
	 *
	 * @throws \PDOException If table doesn't exist.
	 *
	 * @return array A list of auths.
	 */
	function get_auths($user_id)
	{
		$sql = <<<SQL

SELECT
	a.name
FROM
	users_auths ua
LEFT JOIN
	auths a on a.id = ua.auth_id
WHERE
ua.user_id = {$user_id};

SQL;

		$auths = $this->select($sql);

		return $auths;            
	}
}
