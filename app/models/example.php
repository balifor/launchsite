<?php
/**
 * An example model.
 */

namespace Launchsite\models;

/**
 * example model class.
 */
class example extends \Launchsite\abstracts\model 
{
	/**
	 * The database table for the class.
	 *
	 * @var string.
	 */
    public $table = 'example_table';
}
