<?php
/**
 * Templating engine interface.
 *
 * The interface for replacing the page class.
 */

namespace Launchsite\interfaces;

/**
 * Templating engine interface class.
 *
 * An interface that a replacement page class needs to implement.
 */
interface templating_engine 
{
	/**
	 * Add a view path.
	 * 
	 * Add a directory path to find views at.
	 *
	 * @param string $path The directory path.
	 *
	 * @return void
	 */
	public function add_view_path($path);

	/**
	 * Add a template path.
	 *
	 * Add a directory path to find templates at.
	 *
	 * @param mixed $path The directoy path.
	 *
	 * @return void
	 */
	public function add_template_path($path);

	/**
	 * Create a page.
	 *
	 * From controller, method and vars, construct the page.
	 *
	 * @param string $controller The name of the controller to use.
	 *
	 * @param string $method The name of the method to use.
	 *
	 * @param an array of vars to use for the controller.
	 *
	 * @return void Stores the page contents internally.
	 */
	public function setup_page($controller, $method, $vars);

	/**
	 * Show the contents of the page.
	 *
	 * Echo to output the contents of the request.
	 *
	 * @return void Echo's content.
	 */
	public function show_page();

	/**
	 * Process a file and return the contents.
	 *
	 * Get the contents of a view.
	 *
	 * @param string $contents The path to the file.
	 *
	 * @param array $vars The variables to display in the page.
	 *
	 * @param string $type Assumes a view, but will allow absolute paths if set.
	 *
	 * @return string The contents of the page.
	 */
	public function get_contents($contents, $vars = false, $type = 'view');

	/**
	 * Check whether a page should render.
	 *
	 * @return bool True on success, false on failure.
	 */
	static function should_render();
}
