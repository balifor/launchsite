<?php
/**
 * Logging engine interface.
 *
 * The interface for replacing the logger class.
 */

namespace Launchsite\interfaces;

/**
 * Logging engine interface.
 *
 * An interface that a replacement logging engine needs to implement.
 */
interface logging_engine
{
	/**
	 * Log something.
	 *
	 * Log an item to the log array as key => value.
	 *
	 * @param mixed $message something to log.
	 *
	 * @param string $to_file Optionally give a filename to save to.
	 *
	 * @param mixed $email Optionally True to email sysadmin email or array/string of emails to send to.
	 *
	 * @return void.
	 */
	public function log($message, $to_file = false, $email = false);

	/**
	 * Get the log
	 *
	 * Get either a log key or the whole log.
	 *
	 * @param mixed $key Optional string key to get if not set, get the whole array.
	 *
	 * @return mixed Either the whole log, or the contents of log key if it exists, null if not.
	 */
	public function get_log($key);

	/**
	 * Get the details of an exception.
	 *
	 * Return an array of exception details.
	 *
	 * @param \Exception An exception to get details of.
	 *
	 * @return array an array of exception details.
	 */
	static function get_details($e);
}
