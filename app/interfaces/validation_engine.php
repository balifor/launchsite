<?php
/**
 * Validator engine interface.
 *
 * The interface for replacing the Validator class.
 */

namespace Launchsite\interfaces;

/**
 * Validator engine interface.
 *
 * An interface that a replacement validator engine needs to implement.
 */
interface validation_engine
{
	/**
	 * Validate an email address.
	 *
	 * @param string $email The email address to validate.
	 *
	 * @param mixed $if_false_return Optional return value on false.
	 *
	 * @return string The email or an empty string if the optional return value isn't set.
	 */
	static function validate_email($email, $if_false_return = false);
}
