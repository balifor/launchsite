<?php
/**
 * Database engine interface.
 *
 * The interface for replacing the database class.
 */

namespace Launchsite\interfaces;

/**
 * Database engine interface.
 *
 * An interface that a replacement database engine needs to implement.
 */
interface database_engine
{
	/**
	 * PDO database select.
	 *
	 * @param string $sql Paramatised SQL statement.
	 *
	 * @param array @vars An array of parameters.
	 *
	 * @return bool|array Returns an array of results on success, false on failure.
	 */
	public function select($sql, $vars = false);

	/**
	 * Insert into a table.
	 *
	 * @param array $vars A key value array of column => value.
	 *
	 * @param string $table The table to insert into.
	 *
	 * @throws \PDOException if there is an error in the SQL provided.
	 *
	 * @return bool true on success.
	 */
	public function insert($vars, $table);

	/**
	 * Update a table row. Must include ID.
	 *
	 * @param $vars Where statment in array column => value.
	 *
	 * @param $table The table to run the update on.
	 *
	 * @throws \PDOException If there is a problem with the SQL provided.
	 *
	 * @return true on success.
	 */
	public function update($vars, $table);

	/**
	 * Run a PDO query.
	 *
	 * @param string $sql The SQL to run.
	 *
	 * @throws \PDOException if there is a problem with the SQL provided.
	 *
	 * @return array An array of results on success, error on failure.
	 */
	public function query($sql);

	/**
	 * Prepare a query, execute it and return the result.
	 *
	 * @param string $sql The paramatised SQL to run.
	 *
	 * @param array $vars The parameters values.
	 *
	 * @throws \PDOException If there is a problem with the sql provided.
	 *
	 * @return array An array of results on success, error on failure.
	 */
	public function prepared_query($sql, $vars);

	/**
	 * Run a select and turn results into objects.
	 *
	 * @param string $class The name of the class to return results as.
	 *
	 * @param string $sql A SQL statement, optionally paramatised.
	 *
	 * @param array $vars An array of paramaters for the SQL statement.
	 *
	 * @throws \PDOException if there is an error in the provided SQL.
	 *
	 * @return array Returns an array of the results as objects.
	 */
	public function objectify($class, $sql, $vars = false);

	/**
	 * Begin a transaction.
	 *
	 * @return bool.
	 */
	public function begin();

	/**
	 * Commit a transaction.
	 *
	 * @return bool.
	 */
	public function commit();

	/**
	 * Roll back a transaction.
	 *
	 * @return bool.
	 */
	public function rollback();
}
