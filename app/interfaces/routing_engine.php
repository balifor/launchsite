<?php
/**
 * Routing engine interface.
 *
 * The interface for replacing the route class.
 */

namespace Launchsite\interfaces;

/**
 * Routing engine interface.
 *
 * An interface that a replacement routing engine needs to implement.
 */
interface routing_engine
{
	/**
	 * Add a route to the routing engine.
	 *
	 * @param string $name The name to reference the route.
	 *
	 * @param array $route The array route. 
	 *
	 * @param string $type The HTTP method that the route is used for.
	 *
	 * return bool True on success, false on failure.
	 */
	public function add_route($name, $route, $type = 'ANY');

	/**
	 * Match the current route to a controller and method, with an array of any variables in the url.
	 *
	 * @param string $url The URL to match a route to. False if you want to use the current URL.
	 *
	 * @return mixed Returns an instance of the router.
	 */
	public function match_route($url = false);

	/**
	 * Return the routes that match either the current HTTP method, or any.
	 *
	 * @param bool $all Optionally return all routes.
	 *
	 * @return array An array of routes.
	 */
	public function get_routes($all = false);

	/**
	 * Get a named route
	 *
	 * @param string $name The name of route to get
	 *
	 * @param string $type The HTTP method type the route is for.
	 *
	 * @return array|bool Returns route array if found, false if not.
	 */
	public function get_named($name, $type = 'ANY');

	/**
	 * Go to a specified url
	 *
	 * @param string $name The name of the route to go to 
	 *
	 * @param array $url_vars Any URL vars that the route has.
	 *
	 * @param string $type The HTTP method the route is for.
	 *
	 * @return void Sets location header to the new url.
	 */
	public function go_to($name, $url_vars, $type = 'ANY');

	/**
	 * Get a URL for a route.
	 *
	 * @param string $name The name of the route to go to 
	 *
	 * @param array $url_vars Any URL vars that the route has.
	 *
	 * @param string $type The HTTP method the route is for.
	 *
	 * @return string The URL that has been generated.
	 */
	public function url($name, $url_vars, $type = 'ANY');

	/**
	 * Get the name of the current route.
	 *
	 * @return string The name of the current route.
	 */
	public function get_current_route_name();

	/**
	 * Check whether the given route name is the current route.
	 *
	 * @param string $route_name The route name to match.
	 *
	 * @param mixed $return Optionally change the true return value.
	 *
	 * @return bool|mixed Returns true on match or false on no match, true return is changeable.
	 */
	public function if_current_route_is($route_name, $return = true);

	/**
	 * Check whether the given routes names match the current route.
	 *
	 * @param array $route_names An array of routes to check.
	 *
	 * @param mixed $return Optionally change the true return value.
	 *
	 * @return bool|mixed Returns true on match or false on no match, true return is changeable.
	 */
	public function if_current_route_in($route_names, $return = true);
}
