<?php
/**
 * Response engine interface.
 *
 * The interface for replacing the response class.
 */

namespace Launchsite\interfaces;

/**
 * Response engine interface.
 *
 * An interface that a replacement response engine needs to implement.
 */
interface response_engine
{

}
