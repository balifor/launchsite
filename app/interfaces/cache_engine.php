<?php
/**
 * Cache engine interface.
 *
 * The interface for replacing the cache class.
 */

namespace Launchsite\interfaces;

/**
 * Cache engine interface.
 *
 * An interface that a replacement cache engine needs to implement.
 */
interface cache_engine
{
	/**
	 * Add an item to the cache.
	 *
	 * @param string $file_path The path to the file inside the cache directory.
	 *
	 * @param string $data The data to be stored in the cache file.
	 *
	 * @param bool $append Optionally append data instead of replacing.
	 *
	 * @return bool True on success, false on failure.
	 */
	public function cache($file_path, $data, $append = false);

	/**
	 * Remove an item from the cache.
	 *
	 * @param string $file_path The file path of the file to be removed from the cache.
	 *
	 * @return bool True on success, false on failure.
	 */
	public function delete_cached($file_path);

	/**
	 * Get an item from the cache.
	 *
	 * @param string $file_path The path to the file in the cache directory.
	 *
	 * @param string $time_limit A string in strtotime format, I.E: 5 mins.
	 *
	 * @return bool True on success, false on failure.
	 */
	public function get_cached($file_path, $time_limit = false);
}
