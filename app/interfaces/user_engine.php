<?php
/**
 * User engine interface.
 *
 * The interface for replacing the user class.
 */

namespace Launchsite\interfaces;

/**
 * User engine interface.
 *
 * An interface that a replacement user engine needs to implement.
 */
interface user_engine
{
	/**
	 * Check if the user is logged in.
	 *
	 * @return bool 
	 */
	public function logged_in();

	/**
	 * Return the user from the session.
	 *
	 * @return user Return either the logged in user if exists or blank user.
	 */
	public function get_current_user();

	/**
	 * Return the users id from the session.
	 *
	 * @return bool false if not exists, int if it does.
	 */
	public function get_current_user_id();

	/**
	 * Set the current user.
	 *
	 * @param mixed $user The user class to use.
     *
	 * Add the user to the session if not CLI and load their auths.
	 *
	 * @return bool True on success, false on failure.
	 */
	public function set_current_user($user);
}
