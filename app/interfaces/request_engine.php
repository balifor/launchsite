<?php
/**
 * Request engine interface.
 *
 * The interface for replacing the request class.
 */

namespace Launchsite\interfaces;

/**
 * Request engine interface.
 *
 * An interface that a replacement request engine needs to implement.
 */
interface request_engine
{
	/**
	 * Function to set the url.
	 *
	 * @param string $url The url for the request.
	 *
	 * @return void
	 */
	public function set_url($url);

	/**
	 * Function to set the current route.
	 *
	 * @param array $route The route for the request.
	 *
	 * @return void
	 */
	public function set_route($route);
}
