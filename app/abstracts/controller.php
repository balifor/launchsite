<?php
/**
 * Abstract Controller.
 *
 * The class that controllers should extend from.
 */

namespace Launchsite\abstracts;

/**
 * Abstract Controller. 
 * 
 * A class to hold functions that are core to all controllers.
 */
abstract class controller extends \Launchsite\abstracts\core
{
	/**
	 * A flag to set html or api responses as default
	 *
	 * @var bool $use_api
	 */
	public $use_api = false;

	/**
	 * A flag to require login for the controller.
	 *
	 * @var bool $login_required
	 */
	public $login_required = false;

	/**
	 * Array of methods in exception to login requirement.
	 * 
	 * @var array $login_required_excluded_methods method
	 */
	public $login_required_excluded_methods = array();

	/**
	 * An array of auths required for the route.
	 *
	 * @var array $required_auths method => auths
	 */
	public $required_auths = array();

	/**
	 * A switch to choose output type.
	 *
	 * @var string $response_type
	 */
	public $response_type = 'html';

	/**
	 * An array of methods and output types.
	 *
	 * @var array $response_types method => type
	 */
	public $response_types = array();

	/**
	 * Create the controller, and run authentication.
	 */
	public function __construct() 
	{
		$this->set_response_type();
	}

	public function check_login() 
	{
		if ($this->use_api) {
			$api = $this->get_engine('api');
			$logged_in = $api->check_token(); 
		} else {
			$user = $this->get_engine('user');
			$logged_in = $user->logged_in();
		}

		$login_required = $this->check_login_required();

		if ($login_required && !$logged_in) {
			return false;
		} else {
			return true;
		}
	}

	public function check_authed()
	{
		$auths_required = $this->get_auths_required_for_route();

		if (!empty($auths_required)) {
			if ($this->use_api) {
				$api = $this->get_engine('api');
				$auths = $api->get_client_auths();
			} else {
				$user = $this->get_engine('user')->get_current_user();
				$auths = array();
				if ($user) {
					$auths = $user->get_auths();
				}
			}

			if (!empty($auths)) {
				foreach ($auths as $auth) {
					if (is_string($auths_required)) {
						if ($auth === $auths_required) {
							return true;
						}
					} else {
						if (in_array($auth, $auths_required)) {
							return true;
						}
					}
				}

				return false;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	public function set_response_type($type = false) 
	{
		if (!$type) {
			$type = $this->check_response_type();
		}

		$this->get_engine('response')->set_type($type);
	}

	/**
	 * Check whether there is a response type set
	 *
	 * @param string $method The method to check
	 */
	public function check_response_type($method = false)
	{
		if (!$method) {
			$method = $this->get_engine('request')->current_route['method'];
		}

		if (in_array($method, array_keys($this->response_types))) {
			return $this->response_types[$method];
		} else {
			return $this->response_type;
		}
	}

	/**
	 * Check whether the method requires login
	 *
	 * @param string $method The method to check
	 */
	public function check_login_required($method = false)
	{
		if (!$method) {
			$method = $this->get_engine('request')->current_route['method'];
		}

		if ($this->login_required) {
			if (in_array($method, $this->login_required_excluded_methods)) {
				return false;
			} else {
				return true;
			}			
		} else {
			if (in_array($method, $this->login_required_excluded_methods)) {
				return true;
			} else {
				return false;
			}
		}
	}

	/**
	 * Get the auths required for the route
	 *
	 * @param string $method The method to check
	 */
	public function get_auths_required_for_route($method = false)
	{
		if (!$method) {
			$method = $this->get_engine('request')->current_route['method'];
		}

		return isset($this->required_auths[$method]) ? $this->required_auths[$method] : array();
	}
}
