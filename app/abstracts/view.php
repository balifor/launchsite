<?php
/**
 * Abstract View.
 *
 * A class to hold models that all views have.
 */

namespace Launchsite\abstracts;

use \Launchsite\models\route as route;

/**
 * Abstract View.
 *
 * A class to hold functions core to all models.
 */
abstract class view extends core
{
	/**
	 * Generate tab control javascript and html.
	 *
	 * @param string $prefix The unique prefix to identify tab by.
	 *
	 * @param array $contents An array of display name => element id to identify tabs with. 
	 *
	 * @param string $default The default tab to start on.
	 *
	 * @return string A block of html to manage tabs.
	 */
	static function manage_tabs($prefix, $tabs, $default = false, $update = false)
	{
		$active_tab = isset($_SESSION['TABS'][$prefix]) ? $_SESSION['TABS'][$prefix] : $default;
		$json_tabs = json_encode($tabs);

		$js = <<<JS
<ul id="{$prefix}"></ul>
<script type="text/javascript">
domReady(function() {
	launchsite.generate_tabs('{$prefix}', {$json_tabs}, '{$active_tab}', '{$update}')
});
</script>	

JS;

		echo $js;
	}

	/**
	 * A function to create a basic HTML table.
	 *
	 * @param array $rows A set of arrays to tabulate. 
	 *
	 * @param array $headings An optional set of arrays to generate a heading row.
	 *
	 * @return string An HTML table from the rows provided.
	 */
	static function tabulate($rows, $headings = false)
	{
		$table = '<table>' . PHP_EOL;

		//Create the headings row
		if ($headings != false) {
			$table .= '<tr>' . PHP_EOL;

			foreach ($headings as $heading) {
				$table .= <<<HTML
					<th>
					{$heading}     
				</th>

HTML;
			}

			$table .= "</tr>" . PHP_EOL;
		}


		//Create rows for each row
		foreach ($rows as $row) {
			$table .= "<tr>" . PHP_EOL;

			//Add each column
			foreach ($row as $column) {
				$table .= <<<HTML
					<td>
					$column
					</td>

HTML;
			}

			$table .= "</tr>" . PHP_EOL;
		}

		$table .= "</table>" . PHP_EOL;

		return $table;
	}

	/**
	 * Function to escape output.
	 *
	 * @param string $string A string to output.
	 *
	 * @return string A string that is safe to output to the browser.
	 */
	static function escape($string)
	{
		return htmlentities(mb_convert_encoding($string, 'UTF-8', 'UTF-8'), ENT_QUOTES, 'UTF-8');
	}

	/**
	 * Create an HTML link.
	 *
	 * @param string $url the URL to go to.
	 *
	 * @param string $text The display text of the link.
	 *
	 * @param string $attr_string A string of classes ids etc.
	 *
	 * @return string An HTML link.
	 */
	static function link($url, $text, $attr_string = '')
	{
		$html = '<a ' . $attr_string . ' href="' . $url . '">' . $text . '</a>';

		return $html;
	}

	/**
	 * Create a link that will receive active-nav class if its to the current route.
	 *
	 * @param string $route_to_match The route name for the route that activates the link.
	 *
	 * @param string $text The text of the link.
	 *
	 * @param string $auth Optional: if auth is required.
	 *
	 * @param string $type The HTTP method type for the route.
	 */
	static function nav_link($route_to_match, $text, $also_match = array())
	{
		$route = self::get_engine('routing')->get_named($route_to_match, 'ANY');
		if ($route == false) {
			return '';
		}

		$active = false;
		$attr_string = '';
		$current_route = self::get_engine('routing')->match_route();
		if (isset($route['route_name']) && ($route['route_name'] == $current_route['route_name'] || in_array($current_route['route_name'], $also_match))) {
			$active = true;
		}

		if (isset($current_route['parents']) && is_array($current_route['parents'])) {
			if (in_array($route['route_name'], $current_route['parents'])) {
				$active = true;
			}
		}

		if ($active === true) {
			$attr_string = 'class="active-nav"';
		}

		return self::link($route['url'], $text, $attr_string);
	}

	/**
	 * Create an HTML select box.
	 *
	 * @param mixed $details If its a string it will be used as the name attribute, if an array, they will be added as key="value".
	 *
	 * @param array $options An array of option value => display text.
	 *
	 * @param mixed $previous The previous option value if any.
	 *
	 * @return string An HTML select box.
	 */
	static function selectbox($details, $options, $previous = false)
	{
		if (is_array($details)) {
			$details_string = '';

			foreach ($details as $key => $value) {
				$details_string .= " {$key}='{$value}'"; 
			}
		} else {
			$details_string = "name='$details'";
		}

		$html = "<select $details_string>" . PHP_EOL;

		$html .= self::html_options($options, $previous);

		$html .= "</select>" . PHP_EOL;

		return $html;
	}

	/**
	 * Create an HTML list of options.
	 *
	 * @param array $options An array of option value => display text for options.
	 *
	 * @param mixed $previous The previous option value, if applicable.
	 *
	 * @return string An html block of options.
	 */
	static function html_options($options, $previous = false)
	{
		$html = '';

		if (count($options)) {
			foreach ($options as $key => $value) {
				if ($previous == $key) {
					$html .= "<option value='$key' selected='selected'>$value</option>" . PHP_EOL;
				} else {
					$html .= "<option value='$key'>$value</option>" . PHP_EOL;
				}
			}
		}

		return $html;
	}

	/**
	 * Paginate a set of data.
	 * 
	 * TODO: all of this in the fire.
	 *
	 * @param array $data The data to be paginated.
	 *
	 * @param string $prefix The prefix for the pagination data.
	 *
	 * @param array $per_page_options_array An array of per page sizes.
	 *
	 * @return array An array of paginated data. 
	 */
	static function paginate($data, $prefix = '', $per_page_options_array = false)
	{
		$result_count = count($data);
		list($current_page, $per_page) = self::paginate_vars($prefix);
		$page_data['current_page'] = $current_page;
		$page_data['per_page'] = $per_page;

		$page_data['num_pages'] = ceil(floatval($result_count / $per_page));

		$offset = intval($per_page) * intval($current_page -1);
		while($offset > 0) {
			array_shift($data);
			$offset--;
		}

		$results = array();
		while($per_page > 0 && count($data)) {
			$results[] = array_shift($data);
			$per_page--;
		}

		$page_data['page_results'] = $results;
		$page_data['count_results'] = count($page_data['page_results']);

		if($per_page_options_array === false) {
			$per_page_options_array = $this->launchsite->get_option('PER_PAGE_OPTIONS');
		}               

		$return = array_merge($page_data, self::paginate_options($page_data, $prefix, $per_page_options_array));

		return $return;
	}

	/**
	 * Generate a box of paginate options.
	 *
	 * @param array $page_data Information about the number of pages and options.
	 *
	 * @param string $prefix The prefix for the pagination data.
	 *
	 * @param array $per_page_options_array An array of amount per page options.
	 *
	 * @return string An HTML block of pagaination options.
	 */
	static function paginate_options($page_data, $prefix, $per_page_options_array)
	{
		list($current_page, $per_page) = self::paginate_vars($prefix);
		$per_page_options = '';

		foreach ($per_page_options_array as $option) {
			if ($option == $per_page) {
				$per_page_options .= <<<HTML
					<option selected="selected">{$option}</option>           
HTML;
			} else {
				$per_page_options .= <<<HTML
					<option>{$option}</option>           
HTML;
			}
		}

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$url = strtok($_SERVER['HTTP_REFERER'], '?') . '?';
		} else {
			$url = strtok($_SERVER["REQUEST_URI"], '?') . '?';
		}

		$form_id = $prefix . '_paginate_options_form';
		$next_page = ($current_page + 1) > $page_data['num_pages'] ? $page_data['num_pages'] : $current_page + 1;
		$previous_page = ($current_page - 1) >= 1 ? $current_page - 1 : 1;
		$prefix_current_page = $prefix . "_current_page";
		$current_page_number = $page_data['current_page'] == 0 ? 1 : $page_data['current_page']; 
		$prefix_per_page = $prefix . "_per_page";
		$first_page =  $url . $prefix_current_page . '=1&' . $prefix_per_page . '=' . $per_page;
		$last_page_number = $page_data['num_pages'] == 0 ? 1 : $page_data['num_pages'];
		$last_page = $url . $prefix_current_page . '=' . $page_data['num_pages'] . '&' . $prefix_per_page . '=' . $per_page;
		$back_one = $url . $prefix_current_page . '=' . $previous_page . '&' . $prefix_per_page . '=' . $per_page;
		$forwards_one = $url . $prefix_current_page . '=' . $next_page . '&' . $prefix_per_page . '=' . $per_page;

		$return['paginate_options'] = <<<HTML
			<ul class='paginate_options' id="{$prefix}_paginate_options">
			<li><button onclick="window.location.href = '{$back_one}'">&#8592;</button></li>
			<li>
			<a href="{$first_page}">{$current_page_number}</a> of <a href="{$last_page}">{$last_page_number}</a>
			</li>
			<li><button onclick="window.location.href = '{$forwards_one}'">&#8594;</button></li>
			<li><form method="get" id="{$form_id}"><span><label for='{$prefix_current_page}'>Go to </label><input class='paginate_options_current_page' name="{$prefix_current_page}" id="{$prefix_current_page}" type="number" value="{$page_data['current_page']}"/></span><span><select name="{$prefix_per_page}" onchange="\$('#{$prefix_current_page}').val('1')">{$per_page_options}</select><label for='{$prefix_per_page}' >Per Page</label><input type="submit" value="Go"/></span></form></li>
			</ul>
HTML;

		return $return;
	}

	/**
	 * Get the pagination vars from the $_GET array.
	 *
	 * @param string $prefix The prefix for the pagination data.
	 *
	 * @return array The current page and current per page option.
	 */
	static function paginate_vars($prefix = '')
	{
		$session_current_page = isset($_SESSION['PAGINATION'][$prefix . '_current_page']) ? $_SESSION['PAGINATION'][$prefix . '_current_page'] : 1;
		$current_page = isset($_GET[$prefix . '_current_page']) ? $_GET[$prefix . '_current_page'] : $session_current_page;
		$session_per_page = isset($_SESSION['PAGINATION'][$prefix . '_per_page']) ? $_SESSION['PAGINATION'][$prefix . '_per_page'] : 10;
		$per_page = isset($_GET[$prefix . '_per_page']) ? $_GET[$prefix . '_per_page'] : false;

		if ($per_page == false) {
			$per_page = $session_per_page;
		}

		if ($per_page != $session_per_page) {
			$current_page = 1;
		}

		$_SESSION['PAGINATION'][$prefix . '_per_page'] = $per_page;
		$_SESSION['PAGINATION'][$prefix . '_current_page'] = $current_page;

		return array($current_page, $per_page);
	}

	/**
	 * Generate an errors table
	 */
	static function request_errors_table() {
		$html = '';

		if (count(self::get_engine('response')->errors)) {

			$html .= <<<HTML
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th class="alert-danger text-center" colspan="3">System Errors</th>
		</tr>
		<tr>
			<th class="alert-danger">Error</th>
			<th class="alert-danger">File</th>
			<th class="alert-danger">Line</th>
		</tr>
	</thead>
	<tbody>
HTML;

			foreach(self::get_engine('response')->errors as $error) {

				$html .= <<<HTML
		<tr>
			<td>{$error['message']}</td>
			<td>{$error['file']}</td>
			<td>{$error['line']}</td>
		</tr>
HTML;
			}
			
			$html .= <<<HTML
	</tbody>
</table>
HTML;
		}

		return $html;
	}

	/**
	 * Generate a messages table
	 */
	static function request_messages_table() {
		$html = '';

		if (count(self::get_engine('response')->messages)) {

			$html .= <<<HTML
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th class="alert-info text-center">System Messages</th>
		</tr>
		<tr>
			<th class="alert-info">Message</th>
		</tr>
	</thead>
	<tbody>
HTML;

			foreach(self::get_engine('response')->messages as $message) {

				$html .= <<<HTML
		<tr>
			<td><?=$message?></td>
		</tr>
HTML;
			}

			$html .= <<<HTML
	</tbody>
</table>
HTML;
		}

		return $html;
	}

	/**
	 * Generate a submit button which uses ajax
	 */
	static function ajax_submit($form, $url, $text, $callback = false) 
	{
		if (!$callback) {
			$callback = 'launchsite.xhr_page_update';
		}

		$html = <<<HTML
<button class="btn btn-lg btn-primary" id="{$form}_submit" onclick="event.preventDefault(); launchsite.form_update('{$url}', '{$form}', {$callback});">{$text}</button>
HTML;

		return $html;
	}

	/**
	 * Generate an alert
	 */
	static function generate_alert($type, $text)
	{
		switch ($type) 
		{
			case 'error':
				$alert_type = 'alert-danger';
				break;
			case 'success':
				$alert_type = 'alert-success';
				break;
			case 'warning':
				$alert_type = 'alert-warning';	
				break;
			case 'info':
			default:
				$alert_type = 'alert-info';
				break;
		}

		$html = <<<HTML
<div class="alert {$alert_type}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{$text}
</div>
HTML;

		return $html;
	}
}
