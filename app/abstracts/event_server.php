<?php
/**
 * Websocket.
 *
 * An abstract class to generate a basic websocket server.
 */

namespace Launchsite\abstracts;

/**
 * Launchsite websocket.
 */
abstract class event_server extends \Launchsite\abstracts\core
{
	/**
	 * Create the event Loop 
	 */
	public function run() {
		while (true) {
			$this->pre_tick();
			$this->tick();
			$this->post_tick();
		}
	}

	/**
	 * Runs before every tick
	 */
	public function pre_tick() {
		//Overwrite as necessary
	}

	/**
	 * The main bulk of what the event server should be checking each tick.
	 */
	public function tick() {
		//Overwrite as necessary
	}

	/**
	 * Runs after every tick
	 */
	public function post_tick() {
		//Overwrite as necessary
	}
}
