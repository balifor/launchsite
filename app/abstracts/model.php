<?php
/**
 * Abstract model.
 *
 * A class to hold methods relevant to all models.
 */

namespace Launchsite\abstracts;

/**
 * Abstract Model.
 *
 * A class to held functions common to all models.
 */
abstract class model extends core
{
	/**
	 * Use this to set the table name to search for in SQL.
	 *
	 * @var string The table to use for the model.
	 */
	protected $table = '';

	/**
	 * Set the primary key(s) of the table.
	 *
	 * @var array The primary key of the object.
	 */
	protected $primary_keys = array('id');

	/**
	 * Does the model have a history?
	 *
	 * @var bool If the class has a history table or not.
	 */
	protected $has_history = false;

	/**
	 * An array of model name => table name to define the tables that link to other models.
	 *
	 * @var array A list of linked models and the link table.
	 */
	public $linked = array();

	/**
	 * The set properties of the class.
	 *
	 * @var array An array of ['name of property'] = 'value of property'.
	 */
	private $data = array();

	/**
	 * Load a class by id if given one. Alternatively use a find format.
	 *
	 * @param mixed $id The id of the object to load.
	 *
	 * @param string $column Optionally choose a different column to pick than id.
	 */
	public function __construct($id = false, $column = false) 
	{
		if ($id != false) {
			$this->load($id, $column);
		}
	}

	/**
	 * Magic Getter
	 */
	public function __get($name) 
	{
		if(isset($this->data[$name])) {
			return $this->data[$name];
		}
	}

	/**
	 * Create a link in a link table.
	 *
	 * @param mixed The model to link with.
	 *
	 * @param string $table The table to put the link in.
	 *
	 * @param array $data The data to be added to the link table.
	 *
	 * @return bool True on success, false on failure.
	 */
	function link_with($model, $table = false, $data = array())
	{
		if (!$this->link_exists($model, $table, $data)) {
			$table = $table === false ? self::get_link_table($model) : $table;

			if ($table === false) {
				return false;
			}

			$this_field = get_class($this) . '_id';
			$this_id = $this->get('id');

			$model_field = get_class($model) . '_id';
			$model_id = $model->get('id');

			$values = array($this_id, $model_id);
			$columns_string = '';
			$values_string = '';

			if (!empty($data)) {
				foreach ($data as $column => $value) {
					$columns_string = ", $column";
					$values_string = ', ?';
					$values[] = $value;
				}
			}

			$sql = "INSERT INTO {$table} ({$this_field}, {$model_field}{$columns_string}) VALUES (?, ?{$values_string});";

			$db = $this->get_db();

			try {
				$db->prepared_query($sql, $values);
				return true;
			} catch (Exception $e) {
				$this->get_engine('logging')->log(array("Caught:" => $this->get_engine('logging')->get_details($e), "Whilst trying to get link:" => print_r($this, 1) . PHP_EOL . 'To: ' . print_r($model, 1) . PHP_EOL . 'On the table: ' . $table . PHP_EOL, 'With this data' => $data), 'db_errors.txt');
				return false;
			}
		}
	}

	/**
	 * Remove a link in a link table.
	 *
	 * @param mixed $model The model to remove the link with.
	 *
	 * @param string $specific_table The table to insert the link into.
	 *
	 * @param array An optional array to specify which link to remove.
	 *
	 * @return bool True on success, false on failure.
	 */
	function remove_link($model, $specific_table = false, $data = array())
	{
		$table = $specific_table === false ? self::get_link_table($model) : $specific_table;

		$this_field = get_class($this) . '_id';
		$this_id = $this->get('id');

		$model_field = get_class($model) . '_id';
		$model_id = $model->get('id');

		$db = $this->get_db();

		$values = array($this_id, $model_id);
		$sql = "DELETE FROM {$table} WHERE {$this_field} = ? AND {$model_field} = ?";

		if (!empty($data)) {
			foreach ($data as $column => $value) {
				$sql .= " AND {$column} = ?";
				$values[] = $value;
			}
		}

		$sql .= ';';

		$db->prepared_query($sql, $values);

		return true;
	}

	/**
	 * Check if a link exists.
	 *
	 * @param mixed $model The class to check if the link exists with.
	 *
	 * @param string $specific_table The table to check for the link in.
	 *
	 * @param array $data An array of data to match in the link file to ensure its the right link.
	 *
	 * @return bool True on exists, false on failure.
	 */
	function link_exists($model, $specific_table = false, $data = array())
	{
		$table = $specific_table === false ? self::get_link_table($model) : $specific_table;

		$this_field = get_class($this) . '_id';
		$this_id = $this->get('id');

		$model_field = get_class($model) . '_id';
		$model_id = $model->get('id');

		$db = $this->get_db();

		$values = array($this_id, $model_id);
		$sql = "SELECT 1 FROM {$table} WHERE {$this_field} = ? AND {$model_field} = ?";

		if (!empty($data)) {
			foreach ($data as $column => $value) {
				$sql .= " AND {$column} = ?";
				$values[] = $value;
			}
		}

		$sql .= ';';

		$result = $db->select($sql, $values);

		return empty($result) ? false : true;
	}

	/**
	 * Get Links for a model.
	 *
	 * @param mixed $model Get the links between this class and the supplied class.
	 *
	 * @param string $table The table to search for links in.
	 *
	 * @param array $data An array of data to get a limit by.
	 *
	 * @return array An array of links.
	 */
	function get_links($model = false, $table = false, $data = array())
	{
		try {
			if ($table === false) {
				$table = $this->get_link_table($model);
			}
		} catch (Exception $e) {
			$this->get_engine('templating')->render_error('no_table.php', array('Exception' => $this->get_engine('logging')->get_details($e)));
		}

		$this_id = $this->get('id');
		$this_field = get_class($this) . '_id';
		if ($model === false) {
			$values = array($this_id);
			$sql = <<<SQL
				SELECT * FROM {$table} 
			WHERE {$this_field} = ?

SQL;
		} else {
			$model_field = get_class($this) . '_id';
			$model_id = $model->get('id');

			$values = array($this_id, $model_id);
			$sql = <<<SQL
				SELECT * FROM {$table} 
			WHERE {$this_field} = ?
				AND {$model_field} = ?
				.   
SQL;
		}

		if (!empty($data)) {
			foreach ($data as $column => $value) {
				$sql .= " AND {$column} = ?";
				$values[] = $value;
			}
		}

		$sql .= ';';

		return $this->select($sql, $values);
	}

	/**
	 * Get linked models.
	 *
	 * @param mixed $model The class to link with.
	 *
	 * @param string $table The table to find links in.
	 *
	 * @param array $data Limit data by these column => value matches.
	 *
	 * @return array An array of models.
	 */
	public function get_linked($model, $table = false, $data = array())
	{
		try {
			if ($table === false) {
				$table = $this->get_link_table($model);
			}
		} catch (Exception $e) {
			$vars = array('Exception' => $this->get_engine('logging')->get_details($e), 'data' => $data, 'model' => $model);
			$this->get_engine('logging')->log($vars, 'db_error.txt');
			$this->get_engine('templating')->render_error('no_table.php', $vars);
		}

		$model_field = get_class($this) . '_id';
		$model_id = $model->get('id');

		$this_id = $this->get('id');
		$this_field = get_class($this) . '_id';
		if ($this_id == false && $model_id == false) {
			$values = array();
			$sql = <<<SQL
				SELECT m.* FROM {$table} t
				LEFT JOIN {$this->table} m ON t.{$this_field} = m.id
				.   
SQL;
		} elseif ($this_id == false) {
			$values = array($model_id);
			$sql = <<<SQL
				SELECT m.* FROM {$table} t
				LEFT JOIN {$this->table} m ON t.{$this_field} = m.id
				WHERE m.{$model_field} = ?; 
			.   
SQL;
		} elseif ($model_id == false) {
			$values = array($this_id);
			$sql = <<<SQL
				SELECT m.* FROM {$table} t
				LEFT JOIN {$this->table} m ON t.{$this_field} = m.id
				WHERE t.{$this_field} = ?; 
			.   
SQL;
		} else {
			$values = array($this_id, $model_id);
			$sql = <<<SQL
				SELECT * FROM {$table} t
				LEFT JOIN {$this->table} m ON t.{$this_field} = m.id
				WHERE t.{$this_field} = ? AND t.{$model_field} = ?

SQL;
		}

		if (!empty($data)) {
			foreach ($data as $column => $value) {
				$sql .= " AND {$column} = ?";
				$values[] = $value;
			}
		}

		$sql .= ';';

		return $this->objectify($sql, $values);
	}

	/**
	 * Get the link table between two models.
	 *
	 * @param mixed The model to find the link table for.
	 *
	 * @return string The table to find links in.
	 */
	function get_link_table($model)
	{
		if (is_object($model)) {
			$model = get_class($model);
		}

		if (isset($this->linked[$model])) {
			return $this->linked[$model];
		} else {
			throw new Exception(json_encode(array('Model' => $model, 'Links' => $this->linked)));
		}
	}

	/**
	 * Set the table that SQL queries will use.
	 *
	 * @param string The name of the table this model corresponds to.
	 *
	 * @return true on complete.
	 */
	function set_table($value)
	{
		$this->table = $value;

		return true;
	}

	/**
	 * Set a column in the class data array.
	 * 
	 * @param string $column The name of column, or array('column name' => 'column value').
	 *
	 * @param mixed $value If using $column as string provide value here.
	 *
	 * @return bool True on completion.
	 */
	function set($column, $value = false)
	{
		if (is_array($column) && $value === false) {
			foreach ($column as $column => $value) {
				$this->data[$column] = $value;
			}
		} else {
			$this->data[$column] = $value;
		}

		return true;
	}

	/**
	 * Unset variables from the data array.
	 * 
	 * @param mixed $property empty to unset all, string to remove a single property and array of strings for multiple properties.
	 *
	 * @return bool True on success, false on failure.
	 */
	function un_set($property = false)
	{
		if ($property === false) {
			$this->data = array();
		} elseif (is_array($property)) {
			foreach($this->data as $col => $value) {
				if (!in_array($col, $property)) {
					$data[$col] = $value;
				} else {
					unset($this->$col);
				}
			}

			$this->data = $data;

			return true;
		} elseif (isset($this->data[$property])) {
			unset($this->data[$property]);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get variables from the data array.
	 * 
	 * @param mixed $property Either false to return full array, string to return a specific property or array or properties to return.
	 *
	 * @param string $backup Optionally set a return value.
	 *
	 * @return mixed data if found, $backup or false if not.
	 */
	function get($property = false, $backup = false)
	{
		if ($property === false) {
			return $this->data;
		}
		elseif (is_array($property)) {
			if (!count($property)) {
				return $this->data;
			} else {
				$return = array();

				foreach ($property as $name) {
					if (isset($this->data[$name])) {
						$return[] = $this->data[$name];
					}
				}

				if (count($return)) {
					return $return;
				} else {
					return false;
				}
			}
		} else {
			if (isset($this->data[$property])) {
				return $this->data[$property];
			} elseif ($backup !== false) {
				return $backup;
			} else {
				return false;
			}
		}
	}

	/**
	 * Save the data array to the table set as $this->table.
	 *
	 * @throws \Exception if table isnt set.
	 *
	 * @return bool True if complete.
	 */
	function save($save_to_db = 'MAIN')
	{
		$db = $this->get_db($save_to_db);

		if (!strlen($this->table)) {
			throw new Exception(json_encode(array('Model' => $this, 'DB' => $db)));
		}

		$table_columns = $db->get_columns($this->table);

		$save = array();
		if ($table_columns != false) {
			$data_to_keep = array_intersect($table_columns, array_keys($this->data));

			foreach ($data_to_keep as $keep) {
				$save[$keep] = $this->data[$keep];
			}
		}

		$test = $this->already_exists($save);

		if ($test) {
			$db->update($save, $this->table, $this->primary_keys);

			if ($this->has_history) {
				$this->save_history($test, $this->data);
			}
		} else {
			$db->insert($save, $this->table, $this->primary_keys);
			$this->set('id', $db->db->lastInsertId());

			if ($this->has_history) {
				$this->save_history(array(), $this->data);
			}
		}

		return false;
	}

	/**
	 * Function to save changes to objects.
	 *
	 * @param mixed $orig A string or array of original data.
	 *
	 * @param mixed $new A string or array of original data.
	 *
	 * @throws \PDOException if there is an error with the SQL.
	 *
	 * @return bool true on complete.
	 */
	function save_history($orig, $new)
	{
		$vars['change_from'] = json_encode($orig);
		$vars['change_to'] = json_encode($new);
		$vars['change_user'] = user::get_current_user_id();
		$vars['change_time'] = date('Y-m-d H:i:s', time());
		$table = $this->table . '_histories';

		$db = $this->get_db();

		$db->insert($vars, $table);

		return true;
	}

	/**
	 * Find out whether a row already exists.
	 *
	 * @param mixed $data The data to match. 
	 *
	 * @return bool True on exists, false if it doesn't.
	 */
	function already_exists($data = false)
	{
		if ($data === false) {
			$data = $this->get();
		}

		$count = count($this->primary_keys);
		$matched = 0;

		if (!empty($this->primary_keys)) {
			foreach ($this->primary_keys as $key) {
				if (isset($data[$key])) {
					$matched++;
					$find_data[$key] = $data[$key];
				}
			}

			if ($matched == $count) {
				$test = $this->find($find_data);
			} else {
				$test = false;
			}
		} else {
			$test = $this->find($data);
		}

		return $test;
	}

	/**
	 * Load rows from the set table and choose whether to turn them into objects.
	 *
	 * @param array $options An array of either column => value or sql statements as 'SQL' => array('SQL', array(values)).
	 *
	 * @param mixed $class Either a string name for the class or an object of that class.
	 *
	 * @return array An array of results to return.
	 */
	function find($options = false, $class = false, $table = false)
	{
		if (!$table) {
			$table = $this->table;
		}

		$db = $this->get_db();
		list($sql, $values) = $db->generate_select($table, $options);

		if ($class === false) {
			return $db->select($sql, $values);
		} else {       
			return $db->objectify($class, $sql, $values);
		}
	}

	/**
	 * Function to get the count of a find array.
	 *
	 * @param array $options An array of either column => value or sql statements as 'SQL' => array('paramatised SQL string', array(values)). 
	 *
	 * @return int Count of matching rows.
	 */
	function count_of($options)
	{
		$options['SQL'][] = 'count(1) as "count"';
		$result = $this->find($options);

		return $result[0]['count'];
	}

	/**
	 * Paginate a set of data.
	 *
	 * TODO: all of this in the fire.
	 *
	 * @param array $data The data to be paginated.
	 *
	 * @param string $prefix The prefix for the pagination data.
	 *
	 * @param array $per_page_options_array An array of per page sizes.
	 *
	 * @return array An array of paginated data.
	 */
	public function paginate($data, $prefix = '', $per_page_options_array = false)
	{
		$orig_data = $data;
		list($current_page, $per_page) = $this->get_engine('templating')->paginate_vars($prefix);
		$page_data['current_page'] = $current_page;
		$page_data['per_page'] = $per_page;

		try {
			$model = new $orig_data[0]();
			$result_count = $model->count_of($orig_data[1]);
		} catch (Exception $e) {
			$this->get_engine('logging')->log(array("Caught" => $this->get_engine('logging')->get_details($e), "Whilst trying to get paginate count of:" => $orig_data), 'db_error.txt');
			$result_count = 0;
		}

		$page_data['num_pages'] = ceil(floatval($result_count / $per_page));

		$offset = intval($per_page) * intval($current_page -1);
		$results = array();

		$orig_data[1]['OFFSET'] = $offset;
		$orig_data[1]['LIMIT'] = $per_page;

		try {
			if (isset($orig_data[2])) {
				$results = $model->find($orig_data[1], $model);
			} else {
				$results = $model->find($orig_data[1]);
			}
		} catch (Exception $e) {
			$this->get_engine('logging')->log(array("Caught:" => $this->get_engine('logging')->get_details($e), "Whilst trying to paginate" => $orig_data), 'db_error.txt');
			$results = array();
		}

		$page_data['page_results'] = $results;
		$page_data['count_results'] = count($page_data['page_results']);

		if ($per_page_options_array === false) {
			$per_page_options_array = $this->launchsite()->get_config('PER_PAGE_OPTIONS');
		}               

		$return = array_merge($page_data, $this->get_engine('templating')->paginate_options($page_data, $prefix, $per_page_options_array));

		return $return;
	}


	/**
	 * Check if a column value is unique in this table.
	 *
	 * @param string $column The column to match.
	 *
	 * @param string $value The value to match.
	 *
	 * @param bool $case Match the case.
	 *
	 * @return bool.
	 */
	function unique($column, $value, $case = true)
	{
		if ($case === false) {
			$data = $this->find(array($column => array('LIKE', $value)));
		} else {
			$data = $this->find(array($column => $value));
		}

		if (count($data) > 0 && $data !== false) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Set this class's attributes to a row in the DB. 
	 * 
	 * @param mixed $id Either the objects id, a value to use with $use_column or a find array.
	 *
	 * @param string $use_column Optionally use a specific column to laod by.
	 *
	 * @throws \Exception Throws an exception if model can't be loaded.
	 *
	 * @throws \PDOException if there is a problem with the inputs.
	 *
	 * @return bool True if successfully loaded.
	 */
	function load($id, $use_column = false)
	{
		$column = $use_column === false ? 'id' : $use_column;

		if ($column === 'id' && $id === 0) {
			$this->get_engine('response')->add_error("Couldn't load with " . print_r($column, 1) . ' ' . print_r($id, 1));
			return false;
		}

		//Get a model of the class
		$this_class = get_class($this);
		$class = new $this_class();

		if (!strlen($this->table)) {
			$this->get_engine('response')->add_error(array('Model' => $this, 'ID' => $id, 'Column' => $column, 'Class' => get_class($this)));
			return false;
		} elseif (is_array($id)) {
			$obj_array = $class->find($id, $class);            
		} else {
			$obj_array = $class->find(array($column => $id), $class);
		}

		if (!empty($obj_array)) {
			$this->set_table($obj_array[0]->table);
			$this->set($obj_array[0]->get());
			return true;
		} else {
			$this->get_engine('response')->add_error("Couldn't Load $this_class with " . print_r($column, 1) . ' ' . print_r($id, 1));
			return false;
		}
	}

	/**
	 * Run a select and return array.
	 *
	 * @param string $sql Optionally paramatised SQL string.
	 *
	 * @param array $vars The values for paramaters.
	 *
	 * @throws \PDOException If there's a problem with the SQL.
	 *
	 * @return array An array of found results in array format.
	 */
	function select($sql, $vars = false)
	{
		return $this->get_db()->select($sql, $vars);
	}

	/**
	 * Run a select and return objects.
	 *
	 * @param string $sql Optionally paramatised SQL string.
	 *
	 * @param array $vars The values for paramaters.
	 *
	 * @param mixed $class The name of the model to return as if not this one.
	 *
	 * @return array An array of found results in performing class format.
	 */
	function objectify($sql, $vars = false, $class = false)
	{
		$return_class = $class === false ? get_class($this) : (is_object($class) ? get_class($class) : $class); 

		return $this->get_db()->objectify($return_class, $sql, $vars);
	}

	/**
	 * Get an array of key => value from an array of objects.
	 *
	 * @param array $list The array of objects to get columns from.  
	 *
	 * @param string $key The column to put as the array key.
	 *
	 * @param string $value The column to put as the array value.
	 *
	 * @param mixed $value_default An optional default return value if $value isn't set.
	 *
	 * @return array An array of key => value.
	 */
	function to_key_value(array $list, $key, $value, $value_default = false)
	{
		$return = array();
		if (count($list)) {
			foreach ($list as $obj) {
				$return[$obj->get($key)] = $obj->get($value, $value_default);
			}
		}

		return $return;
	}
}
