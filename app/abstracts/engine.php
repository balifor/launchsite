<?php
/**
 * Abstract Engine.
 *
 * The class that engines should extend from.
 */

namespace Launchsite\abstracts;

/**
 * Abstract Engine. 
 * 
 * A class to hold functions that are core to all engines.
 */
abstract class engine extends \Launchsite\abstracts\core
{
	/**
	 * Load the engine.
	 *
	 * @return void
	 */
	public function load_engine()
	{
		return true;
	}
}
