<?php
/**
 * Websocket.
 *
 * An abstract class to generate a basic websocket server.
 */

namespace Launchsite\abstracts;

/**
 * Launchsite websocket.
 */
abstract class websocket_server extends \Launchsite\abstracts\event_server
{
	//Log info
	public $log_name = 'websocket_server';

	//Socket connection info
	protected $address;
	protected $port;
	protected $buffer_length;

	//Socket instances
	protected $sockets = array();
	protected $master_socket;

	//Server users
	protected $users = array();

	//Held Messages
	protected $held_messages = array();

	//Security
	protected $allowed_hosts = array();

	protected $require_origin = false;
	protected $allowed_origins;

	protected $require_protocol = false;
	protected $allowed_protocols;

	protected $require_extension = false;
	protected $allowed_extensions;

	/**
	 * Make a basic websocket server
	 */
	public function __construct($address = '0.0.0.0', $port = '9001', $buffer_length = 2048) {
		$this->address = $address;
		$this->port = $port;
		$this->buffer_length = $buffer_length;

		$this->master_socket = $this->create_socket(); 
		$this->sockets['master'] = $this->master_socket;
	}

	/**
	 * Create a socket
	 */
	public function create_socket() {
		$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP) or die("Couldn't create the socket");
		socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1) or die("Couldn't set the socket option REUSEADDR");
		socket_bind($socket, $this->address, $this->port) or die("Couldn't bind to the address");
		socket_listen($socket, 20) or die("Couldn't start listening");

		return $socket;
	}

###############
# Server Loop #
###############

	/**
	 * What happens every tick of the event server.
	 */
	public function tick() {
		//Make sure the master socket is set
		if (empty($this->sockets)) {
			$this->sockets['master'] = $this->master_socket;
		}

		$this->retry_held_messages();

		$this->websocket_tick();

		$read = $this->sockets;
		$write = $except = null;
		@socket_select($read, $write, $except, 1);
		foreach ($read as $socket) {
			if ($socket == $this->master_socket) {
				$client = socket_accept($socket);
				if ($client < 0) {
					$this->get_engine('logging')->log("Failed to accept client.", $this->log_name, false, true);
					continue;
				} else {
					$this->connect($client);
				}
			} else {
				$num_bytes = @socket_recv($socket, $buffer, $this->buffer_length, 0);
				if ($num_bytes === false) {
					$this->handle_error();
				} elseif ($num_bytes == 0) {
					$this->disconnect($socket);
					$this->get_engine('logging')->log("Client disconnected. TCP connection lost: " . $socket, $this->log_name, false, true);
				} else {
					$user = $this->get_socket_user($socket);
					if (!$user['handshake']) {
						$tmp = str_replace("\r", '', $buffer);
						if (strpos($tmp, "\n\n") === false ) {
							continue; 
						}

						$this->perform_handshake($user, $buffer);
					} else {
						//split packet into frame and send it to deframe
						$this->split_packet($num_bytes, $buffer, $user);
					}
				}
			}
		}
	}

	/**
	 * Handle a connection
	 */
	public function connect($socket) {
		$id = uniqid('u');
		while (in_array($id, array_keys($this->users))) {
			$id = uniqid('u');
		}

		//Create the users connection
		$user = array(
			'id' => $id, 
			'socket' => $socket,
		  	'headers' => array(),
		    'handshake' => false,
			'handlingPartialPacket' => false,
			'partialBuffer' => "",
			'sendingContinuous' => false,
			'partialMessage' => "",
			'hasSentClose' => false,
		);

		$this->users[$id] = $user;
		$this->sockets[$id] = $socket;

		$this->get_engine('logging')->log("Client Connected: " . $id . " as: " . $socket, $this->log_name, false, true);

		//Run on_connect hook
		$this->pre_handshake($user);
	}

	/**
	 * Handle a socket error
	 */
	public function handle_socket_error() {
		$error = socket_last_error($socket);
		switch ($error)
		{
			case 102: // ENETRESET    -- Network dropped connection because of reset
			case 103: // ECONNABORTED -- Software caused connection abort
			case 104: // ECONNRESET   -- Connection reset by peer
			case 108: // ESHUTDOWN    -- Cannot send after transport endpoint shutdown -- probably more of an error on our part, if we're trying to write after the socket is closed.  Probably not a critical error, though.
			case 110: // ETIMEDOUT    -- Connection timed out
			case 111: // ECONNREFUSED -- Connection refused -- We shouldn't see this one, since we're listening... Still not a critical error.
			case 112: // EHOSTDOWN    -- Host is down -- Again, we shouldn't see this, and again, not critical because it's just one connection and we still want to listen to/for others.
			case 113: // EHOSTUNREACH -- No route to host
			case 121: // EREMOTEIO    -- Rempte I/O error -- Their hard drive just blew up.
			case 125: // ECANCELED    -- Operation canceled
				$this->get_engine('logging')->log("Unusual disconnect on socket " . $socket, $this->log_name, false, true);
				$this->disconnect($socket, true, $error);
				break;
			default:
				$this->get_engine('logging')->log('Socket error: ' . socket_strerror($sockErrNo), $this->log_name, false, true);
		}
	}

	/**
	 * Handle a disconnect
	 */
	public function disconnect($socket, $close = true, $error = null) {
		$user = $this->get_socket_user($socket);

		//Clear from users array
		if ($user) {
			unset($this->users[$user['id']]);
		}

		//Clear from sockets array
		if (array_key_exists($user['id'], $this->sockets)) {
			unset($this->sockets[$user['id']]);
		}

		//Clean the socket
		if (!is_null($error)) {
			socket_clear_error($socket);
		}

		//Hook for just before closing the socket
		$this->closed($user);
		socket_close($user['socket']);

        $this->get_engine('logging')->log("Client disconnected. " . $user['socket'], $this->log_name, false, true);
	}

	/**
	 * Find a user by socket
	 */
	public function get_socket_user($socket) {
		foreach ($this->users as $user) {
			if ($user['socket'] == $socket) {
				return $user;
			}
		}

		return false;
	}

	/**
	 * Perform the handshake between client and server
	 */
	public function perform_handshake($user, $buffer) {
		$rfc_guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
		$headers = array();
		$lines = explode("\n",$buffer);
		foreach ($lines as $line) {
			if (strpos($line,":") !== false) {
				$header = explode(":",$line,2);
				$headers[strtolower(trim($header[0]))] = trim($header[1]);
			} elseif (stripos($line,"get ") !== false) {
				preg_match("/GET (.*) HTTP/i", $buffer, $matches);
				$headers['get'] = trim($matches[1]);
			}
		}

		if (isset($headers['get'])) {
			$user['requestedResource'] = $headers['get'];
		} else {
			$handshake_response = "HTTP/1.1 405 Method Not Allowed\r\n\r\n";
		}

		if (!isset($headers['host']) || !$this->check_host($headers['host'])) {
			$handshake_response = "HTTP/1.1 400 Bad Request";
		}

		if (!isset($headers['upgrade']) || strtolower($headers['upgrade']) != 'websocket') {
			$handshake_response = "HTTP/1.1 400 Bad Request";
		}

		if (!isset($headers['connection']) || strpos(strtolower($headers['connection']), 'upgrade') === FALSE) {
			$handshake_response = "HTTP/1.1 400 Bad Request";
		}

		if (!isset($headers['sec-websocket-key'])) {
			$handshake_response = "HTTP/1.1 400 Bad Request";
		} 	

		if (!isset($headers['sec-websocket-version']) || strtolower($headers['sec-websocket-version']) != 13) {
			$handshake_response = "HTTP/1.1 426 Upgrade Required\r\nSec-WebSocketVersion: 13";
		}

		if (($this->require_origin && !isset($headers['origin']) ) || ($this->require_origin && !$this->check_origin($headers['origin']))) {
			$handshake_response = "HTTP/1.1 403 Forbidden";
		}

		if (($this->require_protocol && !isset($headers['sec-websocket-protocol'])) || ($this->require_protocol && !$this->check_protocol($headers['sec-websocket-protocol']))) {
			$handshake_response = "HTTP/1.1 400 Bad Request";
		}

		if (($this->require_extension && !isset($headers['sec-websocket-extensions'])) || ($this->require_extension && !$this->check_extensions($headers['sec-websocket-extensions']))) {
			$handshake_response = "HTTP/1.1 400 Bad Request";
		}

		if (isset($handshake_response)) {
			socket_write($user['socket'], $handshake_response, strlen($handshake_response));
			$this->disconnect($user['socket']);
			return;
		}

		$user['headers'] = $headers;
		$user['handshake'] = $buffer;

		$hash = sha1($headers['sec-websocket-key'] . $rfc_guid);

		$rawToken = "";
		for ($i = 0; $i < 20; $i++) {
			$rawToken .= chr(hexdec(substr($hash,$i*2, 2)));
		}

		$token = base64_encode($rawToken) . "\r\n";

		$subProtocol = (isset($headers['sec-websocket-protocol'])) ? $this->processProtocol($headers['sec-websocket-protocol']) : "";
		$extensions = (isset($headers['sec-websocket-extensions'])) ? $this->processExtensions($headers['sec-websocket-extensions']) : "";

		$handshake_response = "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: $token$subProtocol$extensions\r\n";

		socket_write($user['socket'], $handshake_response, strlen($handshake_response));

		$this->users[$user['id']] = $user;

		$this->get_engine('logging')->log("Handshake with user: " . $user['id'] . " on: " . $user['socket'] . " complete.", $this->log_name, false, true);

		$this->connected($user);
	}

	/**
	 * Select protocol to use
	 */
	protected function selected_protocol($protocol) {
		return ""; // return either "Sec-WebSocket-Protocol: SelectedProtocolFromClientList\r\n" or return an empty string.
		// The carriage return/newline combo must appear at the end of a non-empty string, and must not
		// appear at the beginning of the string nor in an otherwise empty string, or it will be considered part of
		// the response body, which will trigger an error in the client as it will not be formatted correctly.
	}

	/**
	 * Select Extensions to use
	 */
	protected function selected_extensions($extensions) {
		return ""; // return either "Sec-WebSocket-Extensions: SelectedExtensions\r\n" or return an empty string.
	}

	protected function processProtocol($protocol) {
		return ""; // return either "Sec-WebSocket-Protocol: SelectedProtocolFromClientList\r\n" or return an empty string.
		// The carriage return/newline combo must appear at the end of a non-empty string, and must not
		// appear at the beginning of the string nor in an otherwise empty string, or it will be considered part of
		// the response body, which will trigger an error in the client as it will not be formatted correctly.
	}

	protected function processExtensions($extensions) {
		return ""; // return either "Sec-WebSocket-Extensions: SelectedExtensions\r\n" or return an empty string.
	}

	/**
	 * Retry held messages
	 */
	public function retry_held_messages() {
		//For all held messages
		foreach ($this->held_messages as $key => $message) {
			$found = false;
			//Check each user to see if they match
			foreach ($this->users as $current_user) {
				//Send them the message if they do
				if ($message['user']->socket == $current_user['socket']) {
					$found = true;

					if ($current_user['handshake']) {
						unset($this->held_messages[$key]);
						$this->send($currentUser, $message['message']);
					}
				}
			}

			if (!$found) {
				unset($this->heldMessages[$key]);
			}
		}
	}

################
# Data Parsing #
################

	protected function send($user, $message) {
		if ($user['handshake']) {
			$message = $this->frame($message,$user);
			$result = @socket_write($user['socket'], $message, strlen($message));
		}
		else {
			// User has not yet performed their handshake.  Store for sending later.
			$holdingMessage = array('user' => $user, 'message' => $message);
			$this->heldMessages[] = $holdingMessage;
		}
	}

	protected function frame($message, $user, $messageType='text', $messageContinues=false) {
		switch ($messageType) {
			case 'continuous':
				$b1 = 0;
				break;
			case 'text':
				$b1 = ($user['sendingContinuous']) ? 0 : 1;
				break;
			case 'binary':
				$b1 = ($user['sendingContinuous']) ? 0 : 2;
				break;
			case 'close':
				$b1 = 8;
				break;
			case 'ping':
				$b1 = 9;
				break;
			case 'pong':
				$b1 = 10;
				break;
		}

		if ($messageContinues) {
			$user['sendingContinuous'] = true;
		} else {
			$b1 += 128;
			$user['sendingContinuous'] = false;
		}

		$length = strlen($message);
		$lengthField = "";
		if ($length < 126) {
			$b2 = $length;
		} elseif ($length < 65536) {
			$b2 = 126;
			$hexLength = dechex($length);
			//$this->stdout("Hex Length: $hexLength");
			if (strlen($hexLength)%2 == 1) {
				$hexLength = '0' . $hexLength;
			}

			$n = strlen($hexLength) - 2;

			for ($i = $n; $i >= 0; $i=$i-2) {
				$lengthField = chr(hexdec(substr($hexLength, $i, 2))) . $lengthField;
			}

			while (strlen($lengthField) < 2) {
				$lengthField = chr(0) . $lengthField;
			}
		} else {
			$b2 = 127;
			$hexLength = dechex($length);
			if (strlen($hexLength)%2 == 1) {
				$hexLength = '0' . $hexLength;
			}

			$n = strlen($hexLength) - 2;

			for ($i = $n; $i >= 0; $i=$i-2) {
				$lengthField = chr(hexdec(substr($hexLength, $i, 2))) . $lengthField;
			}

			while (strlen($lengthField) < 8) {
				$lengthField = chr(0) . $lengthField;
			}
		}

		return chr($b1) . chr($b2) . $lengthField . $message;
	}

	//check packet if he have more than one frame and process each frame individually
	protected function split_packet($length,$packet, $user) {
		//add PartialPacket and calculate the new $length
		if ($user['handlingPartialPacket']) {
			$packet = $user['partialBuffer'] . $packet;
			$user['handlingPartialPacket'] = false;
			$length=strlen($packet);
		}

		$fullpacket=$packet;
		$frame_pos=0;
		$frame_id=1;

		while($frame_pos<$length) {
			$headers = $this->extractHeaders($packet);
			$headers_size = $this->calcoffset($headers);
			$framesize=$headers['length']+$headers_size;

			//split frame from packet and process it
			$frame=substr($fullpacket,$frame_pos,$framesize);

			if (($message = $this->deframe($frame, $user,$headers)) !== FALSE) {
				if ($user['hasSentClose']) {
					$this->disconnect($user['socket']);
				} else {
					if ((preg_match('//u', $message)) || ($headers['opcode']==2)) {
						//$this->stdout("Text msg encoded UTF-8 or Binary msg\n".$message);
						$this->process($user, $message);
					} else {
						$this->stderr("not UTF-8\n");
					}
				}
			}

			//get the new position also modify packet data
			$frame_pos+=$framesize;
			$packet=substr($fullpacket,$frame_pos);
			$frame_id++;
		}
	}

	protected function calcoffset($headers) {
		$offset = 2;
		if ($headers['hasmask']) {
			$offset += 4;
		}
		
		if ($headers['length'] > 65535) {
			$offset += 8;
		} elseif ($headers['length'] > 125) {
			$offset += 2;
		}

		return $offset;
	}

	protected function deframe($message, &$user) {
		//echo $this->strtohex($message);
		$headers = $this->extractHeaders($message);
		$pongReply = false;
		$willClose = false;
		switch($headers['opcode']) {
			case 0:
			case 1:
			case 2:
				break;
			case 8:
				// todo: close the connection
				$user['hasSentClose'] = true;
				return "";
			case 9:
				$pongReply = true;
			case 10:
				break;
			default:
				//$this->disconnect($user); // todo: fail connection
				$willClose = true;
				break;
		}

		if ($this->checkRSVBits($headers,$user)) {
			return false;
		}

		if ($willClose) {
			// todo: fail the connection
			return false;
		}

		$payload = $user['partialMessage'] . $this->extractPayload($message,$headers);

		if ($pongReply) {
			$reply = $this->frame($payload,$user,'pong');
			socket_write($user['socket'],$reply,strlen($reply));
			return false;
		}

		if ($headers['length'] > strlen($this->applyMask($headers,$payload))) {
			$user['handlingPartialPacket'] = true;
			$user['partialBuffer'] = $message;
			return false;
		}

		$payload = $this->applyMask($headers,$payload);

		if ($headers['fin']) {
			$user['partialMessage'] = "";
			return $payload;
		}

		$user['partialMessage'] = $payload;
		return false;
	}

	protected function extractHeaders($message) {
		$header = array(
			'fin'     => $message[0] & chr(128),
			'rsv1'    => $message[0] & chr(64),
			'rsv2'    => $message[0] & chr(32),
			'rsv3'    => $message[0] & chr(16),
			'opcode'  => ord($message[0]) & 15,
			'hasmask' => $message[1] & chr(128),
			'length'  => 0,
			'mask'    => ""
		);

		$header['length'] = (ord($message[1]) >= 128) ? ord($message[1]) - 128 : ord($message[1]);

		if ($header['length'] == 126) {
			if ($header['hasmask']) {
				$header['mask'] = $message[4] . $message[5] . $message[6] . $message[7];
			}

			$header['length'] = ord($message[2]) * 256 + ord($message[3]);
		} elseif ($header['length'] == 127) {
			if ($header['hasmask']) {
				$header['mask'] = $message[10] . $message[11] . $message[12] . $message[13];
			}

			$header['length'] = ord($message[2]) * 65536 * 65536 * 65536 * 256
				+ ord($message[3]) * 65536 * 65536 * 65536
				+ ord($message[4]) * 65536 * 65536 * 256
				+ ord($message[5]) * 65536 * 65536
				+ ord($message[6]) * 65536 * 256
				+ ord($message[7]) * 65536
				+ ord($message[8]) * 256
				+ ord($message[9]);
		} elseif ($header['hasmask']) {
			$header['mask'] = $message[2] . $message[3] . $message[4] . $message[5];
		}

		return $header;
	}

	protected function extractPayload($message,$headers) {
		$offset = 2;
		if ($headers['hasmask']) {
			$offset += 4;
		}

		if ($headers['length'] > 65535) {
			$offset += 8;
		} elseif ($headers['length'] > 125) {
			$offset += 2;
		}

		return substr($message,$offset);
	}

	protected function applyMask($headers,$payload) {
		$effectiveMask = "";
		if ($headers['hasmask']) {
			$mask = $headers['mask'];
		} else {
			return $payload;
		}

		while (strlen($effectiveMask) < strlen($payload)) {
			$effectiveMask .= $mask;
		}

		while (strlen($effectiveMask) > strlen($payload)) {
			$effectiveMask = substr($effectiveMask,0,-1);
		}

		return $effectiveMask ^ $payload;
	}

	protected function checkRSVBits($headers,$user) { // override this method if you are using an extension where the RSV bits are used.
		if (ord($headers['rsv1']) + ord($headers['rsv2']) + ord($headers['rsv3']) > 0) {
			//$this->disconnect($user); // todo: fail connection
			return true;
		}

		return false;
	}

	protected function strtohex($str) {
		$strout = "";
		for ($i = 0; $i < strlen($str); $i++) {
			$strout .= (ord($str[$i])<16) ? "0" . dechex(ord($str[$i])) : dechex(ord($str[$i]));
			$strout .= " ";
			if ($i%32 == 7) {
				$strout .= ": ";
			}
			if ($i%32 == 15) {
				$strout .= ": ";
			}
			if ($i%32 == 23) {
				$strout .= ": ";
			}
			if ($i%32 == 31) {
				$strout .= "\n";
			}
		}
		return $strout . "\n";
	}

	protected function printHeaders($headers) {
		echo "Array\n(\n";
		foreach ($headers as $key => $value) {
			if ($key == 'length' || $key == 'opcode') {
				echo "\t[$key] => $value\n\n";
			}
			else {
				echo "\t[$key] => ".$this->strtohex($value)."\n";

			}

		}
		echo ")\n";
	}

######################
# Security Functions #
######################

	/**
	 * Check the host against a list of accepted hosts.
	 */
	protected function check_host($hostName) {
		return true; 
	}

	/**
	 * Check the origin of the request against a list of accepted origins
	 */
	protected function check_origin($origin) {
		return true;	
	}

	/**
	 * Check the protocol against a list of accepted protocols
	 */
	protected function check_protocol($protocol) {
		return true;
	}

	/**
	 * Check extensions against a list of accepted extensions
	 */
	protected function check_extensions($extensions) {
		return true;
	}

#####################
# Hook in functions #
#####################

	/**
	 * What happens every tick of the websocket server
	 */
	protected function websocket_tick() {
		//Overwrite as necessary
	}

	/**
	 * What happens after connect, but before handshake
	 */
	protected function pre_handshake($user) {

	}

	/**
	 * What happens after connect and handshake
	 */
	protected function post_handshake($user) {

	}

	/**
	 * What happens on disconnect
	 */
	protected function on_disconnect($user) {

	}
}
