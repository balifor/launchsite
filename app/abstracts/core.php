<?php
/**
 * Core functions.
 *
 * An abstract class to hold the core functions of the site.
 */

namespace Launchsite\abstracts;

use \Launchsite\launcher\launcher as launcher;

/**
 * Core launchsite functions.
 *
 * A class to hold functions that may be needed anywhere throughout a site.
 */
abstract class core
{
	/**
	 * Get the launcher.
	 *
	 * @return launcher.
	 */
	static function launchsite()
	{
		return launcher::launch();
	}

	/**
	 * Get the launcher engines.
	 *
	 * @param string $name The name of the engine to load.
	 *
	 * @return mixed The launcher engine.
	 */
	static function get_engine($name)
	{
		return launcher::launch()->get_engine($name);
	}

	/**
	 * Get the Database.
	 *
	 * @return db Return the active db.
	 */
	function get_db($name = false, $db_name = true)
	{
		return self::get_engine('database')->get_db($name, $db_name);
	}

	/**
	 * Recursive XML from array
	 * 
	 * @param array $array
	 */
	public function array_to_xml($array, $xmlObj = 'root', $obj = false) {
		if (!is_a($xmlObj, '\SimpleXMLElement')) {
			$xmlObj = new \SimpleXMLElement("<{$xmlObj} />");
		}

		foreach ($array as $key=>$value) {
			$xmlObj->addChild($key);
			if(!is_array($value)){
				$xmlObj->$key = $value;
			} else { //Value is array
				$this->array_to_xml($value, $xmlObj->$key);
			}
		}

		if ($obj) {
			return $xmlObj();
		} else {
			return $xmlObj->asXML();
		}
	}
}
