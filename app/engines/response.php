<?php
/**
 * Response engine.
 *
 * Handles the response.
 */

namespace Launchsite\engines;

/**
 * Response class.
 *
 * A class to handle a response.
 */
class response extends \Launchsite\abstracts\engine implements \Launchsite\interfaces\response_engine
{
	/**
	 * The HTTP response code to send.
	 *
	 * @var int $response_code
	 */
	public $response_code = 200;

	/**
	 * The type of response to send
	 *
	 * @var string $types
	 */
	public $response_type = 'html';

	/**
	 * The file the error occured in.
	 *
	 * @var string $error_file
	 */
	public $error_file = false;

	/**
	 * The line the error occured on.
	 *
	 * @var string $error_line
	 */
	public $error_line = false;

	/**
	 * The error page to use if applicable.
	 *
	 * @var string $error_page
	 */
	public $error_page = false;

	/**
	 * Errors generated during request.
	 *
	 * @var array $errors
	 */
	public $errors = array();

	/**
	 * Messages generated during request.
	 *
	 * @var array $messages
	 */
	public $messages = array();

	/**
	 * The controller generated vars.
	 *
	 * @var array $vars
	 */
	public $vars = array();

	/**
	 * The current route.
	 *
	 * @var array $current_route
	 */
	public $current_route = array();

	/**        
	 * Set the HTTP response code.
	 *
	 * @param int $code The HTTP response code to reply with.
	 *
	 * @return void.
	 */
	public function set_code($code)
	{
		$this->response_code = $code;
	}

	/**
	 * Function to set the current route.
	 *
	 * @param array $route The route for the request.
	 *
	 * @return void
	 */
	public function set_route($route)
	{
		$this->current_route = $route;
	}

	/**        
	 * Set the type of response.
	 *
	 * @param string $type Set whether to send JSON or HTML.
	 *
	 * @return void.
	 */
	public function set_type($type)
	{
		$this->response_type = $type;
	}

	/**        
	 * Set the error page.
	 *
	 * @param string $page The error page to use.
	 *
	 * @return void.
	 */
	public function set_error_page($page)
	{
		$this->error_page = $page;
	}

	/**
	 * Add an error.
	 *
	 * @param string $message The message to add.
	 */
	public function add_message($message)
	{
		$this->messages[] = $message;
	}

	/**
	 * Add an error.
	 *
	 * @param string $page The error page to use.
	 *
	 * @return void.
	 */
	public function add_error($message, $code = false, $page = false)
	{
		$bt = debug_backtrace();
		$caller = array_shift($bt);
		$this->error_file = $caller['file'];
		$this->error_line = $caller['line'];

		if ($code != false) {
			$this->set_code($code);
		}

		if ($page != false) {
			$this->set_error_page($page);
		}

		$this->errors[] = array('message' => $message, 'file' => $this->error_file, 'line' => $this->error_line);
	}

	/**        
	 * Set the page vars.
	 *
	 * @param mixed $vars The vars to use.
	 *
	 * @param string $key The key to use.
	 *
	 * @return void.
	 */
	public function set_vars($vars, $key = false)
	{
		if ($key) {
			$this->vars[$key] = $vars;
		} else {
			if (is_array($vars)) {
				$this->vars = array_merge($this->vars, $vars);
			} else {
				$this->vars[] = $vars;
			}
		}
	}

	/**        
	 * Send the response.
	 *
	 * @param array $vars The controller variables.
	 *
	 * @return void Echos out the page contents.
	 */
	public function respond($vars)
	{
		if ($vars) {
			$this->set_vars($vars);
		}
	
		$this->send_response_code($this->response_code);

		if ($this->response_type === 'html') {
			mb_http_output('UTF-8');
			header('Content-Type: text/html; charset=UTF-8');

			if ($this->error_page) {
				if (isset($this->vars['TEMPLATE'])) {
					$this->get_engine('templating')->setup_page('', array($this->error_page, $this->vars['TEMPLATE']), $this->vars, 'error');
				} else {
					$this->get_engine('templating')->setup_page('', $this->error_page, $this->vars, 'error');
				}
			} else {
				if (isset($this->vars['TEMPLATE'])) {
					if (isset($this->vars['RENDER'])) {
						$this->get_engine('templating')->setup_page($this->current_route['controller'], array($this->vars['RENDER'], $this->vars['TEMPLATE']), $this->vars);
					} else {
						$this->get_engine('templating')->setup_page($this->current_route['controller'], array($this->current_route['method'], $this->vars['TEMPLATE']), $this->vars);
					}
				} else {
					if (isset($this->vars['RENDER'])) {
						$this->get_engine('templating')->setup_page($this->current_route['controller'], $this->vars['RENDER'], $this->vars);
					} else {
						$this->get_engine('templating')->setup_page($this->current_route['controller'], $this->current_route['method'], $this->vars);
					}
				}
			}

			$this->get_engine('templating')->show_page();
		} elseif ($this->response_type == 'json') {
			echo json_encode($this->vars);
        } elseif ($this->response_type == 'xml') {
			echo <<<XML
<?xml version="1.0" encoding="UTF-8"?>
XML;
			echo $this->array_to_xml($this->vars, "response");
		} elseif ($this->response_type == 'cli') {
			$response['Response Code'] = $this->response_code;
			
			if(count($this->messages)) {
				$response['Response Messages'] = $this->messages;
			}

			if(count($this->errors)) {
				$response['Response Errors'] = $this->errors;
			}

			if(count($this->vars)) {
				$response['Response Vars'] = $this->vars;
			}

			echo print_r($response, 1);
		}

		return $this->vars;
	}

	public function get_response_code_text($code) {
		switch ($code) {
			case 100: 
				$text = 'Continue'; 
				break;
			case 101: 
				$text = 'Switching Protocols'; 
				break;
			case 200: 
				$text = 'OK'; 
				break;
			case 201: 
				$text = 'Created'; 
				break;
			case 202: 
				$text = 'Accepted'; 
				break;
			case 203: 
				$text = 'Non-Authoritative Information'; 
				break;
			case 204: 
				$text = 'No Content'; 
				break;
			case 205: 
				$text = 'Reset Content'; 
				break;
			case 206: 
				$text = 'Partial Content'; 
				break;
			case 300: 
				$text = 'Multiple Choices'; 
				break;
			case 301: 
				$text = 'Moved Permanently'; 
				break;
			case 302: 
				$text = 'Moved Temporarily'; 
				break;
			case 303: 
				$text = 'See Other'; 
				break;
			case 304: 
				$text = 'Not Modified'; 
				break;
			case 305: 
				$text = 'Use Proxy'; 
				break;
			case 400: 
				$text = 'Bad Request'; 
				break;
			case 401: 
				$text = 'Unauthorized'; 
				break;
			case 402: 
				$text = 'Payment Required'; 
				break;
			case 403: 
				$text = 'Forbidden'; 
				break;
			case 404: 
				$text = 'Not Found'; 
				break;
			case 405: 
				$text = 'Method Not Allowed'; 
				break;
			case 406: 
				$text = 'Not Acceptable'; 
				break;
			case 407: 
				$text = 'Proxy Authentication Required'; 
				break;
			case 408: 
				$text = 'Request Time-out'; 
				break;
			case 409: 
				$text = 'Conflict'; 
				break;
			case 410: 
				$text = 'Gone'; 
				break;
			case 411: 
				$text = 'Length Required'; 
				break;
			case 412: 
				$text = 'Precondition Failed'; 
				break;
			case 413: 
				$text = 'Request Entity Too Large'; 
				break;
			case 414: 
				$text = 'Request-URI Too Large'; 
				break;
			case 415: 
				$text = 'Unsupported Media Type'; 
				break;
			case 500: 
				$text = 'Internal Server Error'; 
				break;
			case 501: 
				$text = 'Not Implemented'; 
				break;
			case 502: 
				$text = 'Bad Gateway'; 
				break;
			case 503: 
				$text = 'Service Unavailable'; 
				break;
			case 504: 
				$text = 'Gateway Time-out'; 
				break;
			case 505: 
				$text = 'HTTP Version not supported'; 
				break;
			default:
				$text = 'Unknown http status code "' . htmlentities($code) . '"';
				break;
		}

		return $text;
	}

	public function send_response_code($code = false) {
		if (!$code) {
			$code = $this->response_code;
		}

		if (function_exists('http_response_code')) {
			http_response_code($code);
		} else {
			$text = $this->get_response_code_text($code);
			$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

			header($protocol . ' ' . $code . ' ' . $text);
		}
	}

	public function pre_print($data)
	{
		die('<pre>' . print_r($data, 1) . '</pre>');
	}
}
