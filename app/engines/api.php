<?php
/**
 * Api engine.
 *
 * Create or replace an engine for the framework.
 */

namespace Launchsite\engines;

/**
 * Engine class.
 */
class api extends \Launchsite\abstracts\engine
{
	public function authenticate($id, $secret)
	{
		$client = new \Launchsite\models\api_client();

		return $client->authenticate($id, $secret);
	}

	public function get_token()
	{
		$headers = $this->get_engine('request')->get_headers();
		$token = isset($headers['LS-TOKEN']) ? $headers['LS-TOKEN'] : false;

		return $token;
	}

	public function generate_token() 
	{
		$token = $this->generate_unique_string();
	}

	public function generate_unique_string()
	{
		return 'testing123';		
	}

	public function add_token_header($token = false) 
	{
		if (!$token) {
			$token = $this->generate_token();
		}

		header("LS-TOKEN: {$token}");
	}

	public function check_token($token = false) 
	{
		if (!$token) {
			$token = $this->get_token();
		}

		if ($token == 'testing123') {
			return true;
		} else {
			return false;
		}
	}

	public function get_client_from_token($token)
	{
		return array();
	}

	public function get_client_auths()
	{
		$this->client_auths = array('test');

		return $this->client_auths;
	}

	public function generate_error($code, $message, $data = false)
	{
		return array(
			'code' => $code,
			'message' => $message,
			'data' => $data
		);
	}

	public function generate_success($code, $message, $data = false)
	{

	}
}
