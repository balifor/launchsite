<?php
/**
 * Sorting engine.
 */

namespace Launchsite\engines;

/**
 * Engine class.
 */
class sorting extends \Launchsite\abstracts\engine
{
	/**
	 * A function that is called when the engine is first loaded.
	 *
	 * @return mixed 
	 */
	public function load_engine()
	{
		//On load here
	}

	/**
	 * Sort an array of objects by field.
	 *
	 * @param array $data An array of objects.
	 *
	 * @param string $col The column to sort the objects by.
	 *
	 * @return array A sorted array of objects by given field.
	 */
	function sort_objects_by_field($data, $col, $asc = true)
	{
		$sort = array();
		foreach ($data as $i => $obj) {
			$sort[$i] = $obj->get($col);
		}

		if ($asc) {
			return array_multisort($sort, SORT_ASC, $data);
		} else {
			return array_multisort($sort, SORT_DESC, $data);
		}
	}

	/**
	 * Sort array of arrays by field.
	 *
	 * @param array $data An array of arrays.
	 *
	 * @param string $col The key to sort the arrays by.
	 *
	 * @return array A sorted array of arrays by given key.
	 */
	function sort_arrays_by_key($data, $col, $asc = true)
	{
		$sort = array();
		foreach ($data as $i => $obj) {
			$sort[$i] = $obj[$col];
		}

		if ($asc) {
			return array_multisort($sort, SORT_ASC, $data);
		} else {
			return array_multisort($sort, SORT_DESC, $data);
		}
	}
}
