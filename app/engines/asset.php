<?php
/**
 * Asset engine.
 */

namespace Launchsite\engines;

/**
 * Asset class.
 */
class asset extends \Launchsite\abstracts\engine
{
	/**
	 * Output a file.
	 *
	 * @param string $file_path The path to the file to be output.
	 *
	 * @param string $output_name The prefered name of the file that is output.
	 *
	 * @return bool true means file has been output, false means not.
	 */
	public function output_file($file_path, $output_name = false)
	{
		$name = $output_name === false ? basename($file_path) : $output_name;

		if (is_readable($file_path)) {
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.$name.'"');
			header("Pragma: public");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header('Content-Length: ' . filesize($file_path));

			readfile($file_path);

			return true;
		} else {
			return false;
		}
	}

	public function show_image($file_path, $output_name = false)
	{
		$name = $output_name === false ? basename($file_path) : $output_name;

		if (is_readable($file_path)) {
			header('Content-Type: image');
			header("Pragma: public");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header('Content-Length: ' . filesize($file_path));

			readfile($file_path);

			return true;
		} else {
			return false;
		}
	}

	public function get_asset_info($type, $id)
	{
		$asset = new \Launchsite\models\asset();

		try {
			$asset = $asset->get_info($type, $id);

			$asset['full_path'] = SITE . 'assets' . DIRECTORY_SEPARATOR . $asset['path'] . $id;

			return $asset;
		} catch (Exception $e) {
			$this->get_engine('response')->add_error("Loading asset threw exception: " . $e->getMessage());
		}
	}
}
