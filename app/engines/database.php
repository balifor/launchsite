<?php
/**
 * Database abstraction layer.
 *
 * Allows for easy use of mysql or postgres, others to be added.
 */

namespace Launchsite\engines;

Use \Exception;

/**
 * DB class.
 *
 * A class to enable a Database connection.
 */
class database extends \Launchsite\abstracts\engine implements \Launchsite\interfaces\database_engine 
{
	/**
	 * Active Database Instance.
	 *
	 * @var db.
	 */
	public $db;

	/**
	 * Default config name
	 */
	public $default_config;

	public function load_engine()
	{
		$this->default_config = $this->launchsite()->get_config('DEFAULT_DB_CONFIG');
	}

	/**
	 * Database Instance List.
	 *
	 * @var array
	 */
	public $dbs = array();

	/**
	 * Get data from a database.
	 *
	 * @param string $query The query string.
	 *
	 * @param array @vars An array of parameters.
	 *
	 * @return bool|array Returns an array of results on success, false on failure.
	 */
	public function select($query, $vars = false)
	{
		return $this->db->select($query, $vars);	
	}

	/**
	 * Put data into a database.
	 *
	 * @param array $vars A key value array of column => value.
	 *
	 * @param string $table The table to insert into.
	 *
	 * @return bool true on success.
	 */
	public function insert($vars, $table)
	{   
		return $this->db->insert($vars, $table);
	}

	/**
	 * Update data in a database.
	 *
	 * @param $vars Where statment in array column => value. 
	 *
	 * @param $table The table to run the update on.
	 *
	 * @return true on success.
	 */
	public function update($vars, $table)
	{
		return $this->db->update($vars, $table);
	}

	/**
	 * Run a query.
	 *
	 * @param string $query The query string.
	 *
	 * @return array An array of results on success, error on failure.
	 */
	public function query($query)
	{
		return $this->db->query($query);
	}

	/**
	 * Prepare a query, execute it and return the result.
	 *
	 * @param string $sql The paramatised SQL to run.
	 *
	 * @param array $vars The parameters values.
	 *
	 * @return array An array of results on success, error on failure.
	 */
	public function prepared_query($sql, $vars)
	{
		return $this->db->prepared_query($sql, $vars);
	}

	/**
	 * Run a select and turn results into objects.
	 *
	 * @param string $class The name of the class to return results as.
	 *
	 * @param string $query The query string to get data.
	 *
	 * @param array $vars An array of paramaters for the SQL statement. 
	 *
	 * @return array Returns an array of the results as objects.
	 */
	public function objectify($class, $query, $vars = false)
	{
		return $this->db->objectify($class, $query, $vars);
	}

	/**
	 * Begin a transaction.
	 *
	 * @return bool.
	 */
	public function begin()
	{
		return $this->db->begin();
	}

	/**
	 * Commit a transaction.
	 *
	 * @return bool.
	 */
	public function commit()
	{
		return $this->db->commit();
	}

	/**
	 * Roll back a transaction.
	 *
	 * @return bool.
	 */
	public function rollback()
	{
		return $this->db->rollback();
	}

	/**
	 * Set the currently active DB
	 *
	 * @var $name The name of the db to set as active.
	 *
	 * @var $db The db to set as active.
	 *
	 * @return bool
	 */
	public function set_db($name, $db) {
		$this->dbs[$name] = $db;
		$this->db = $db;
	}

	/**
	 * Get a Database.
	 *
	 * @param mixed $name The name of the database to get.
	 *
	 * @param bool $to_db The name of the database to get.
	 *
	 * @return mixed A database.
	 */
	public function get_db($name = false, $to_db = true)
	{
		if ($name == false && isset($this->db)) {
			return $this->db;
		} elseif ($name == false) {
			$name = $this->default_config;
		}

		if (isset($this->dbs[$name]) && is_object($this->dbs[$name])) {
			return $this->dbs[$name];
		} else {
			$conf = $this->launchsite()->get_config('DB');
			if (isset($conf[$name])) {
				try {
					$this->dbs[$name] = $this->get_database($conf[$name]['ENGINE']);
					$this->dbs[$name]->set_details($name, $conf[$name]);
					$this->dbs[$name]->get_connection($to_db);

					return $this->dbs[$name];
				} catch (\PDOException $e) {
					$this->get_engine('response')->add_error("Couldn't connect to db $name, it threw an exception: " . $e->getMessage());
					throw $e;
				}
			} else {
				$this->get_engine('response')->add_error("DB config doesn't exist.");
				throw new Exception("Couldn't load DB: $name");
			}
		}
	}

	/**
	 * Function to get a database type.
	 *
	 * @param string $name The type of database to find.
	 *
	 * @return mixed $db The database being returned.
	 */
	private function get_database($name)
	{
		$response = $this->get_engine('response');

		//If an autoloadable class name is found, use it
		if (class_exists($name)) {
			try {
				$db = new $name();
				return $db;
			} catch (Exception $e) {
				$response->add_error("Loading database: $name threw an exception: " . $e->getMessage(), 500, 'db_error.php');
				throw $e;
			}
		}

		//Check it's default locations
		if (is_readable($this->launchsite()->get_config('SITE_DATABASES') . $name . '.php')) {
			require_once $this->launchsite()->get_config('SITE_DATABASES') . $name . '.php';
		}

		if (class_exists($name)) {
			try {
				$db = new $name;
				return $db;
			} catch (Exception $e) {
				$response->add_error("Loading database: $name threw an exception: " . $e->getMessage(), 500, 'db_error.php');
				throw $e;
			}
		}

		//Check if its a fallback to a Launchsite controller.
		if (class_exists('Launchsite\databases\\' . $name)) {
			try {
				$framework_db = 'Launchsite\databases\\' . $name;
				$db = new $framework_db;
				return $db;
			} catch (Exception $e) {
				$response->add_error("Loading database: $name threw an exception: " . $e->getMessage(), 500, 'db_error.php');
				throw $e;
			}
		}

		$this->get_engine('response')->add_error("Couldn't find database: $name", 500, 'db_error.php');
	}
}
