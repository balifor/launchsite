<?php
/**
 * Templating engine.
 *
 * A system to load views using HTML and php.
 */

namespace Launchsite\engines;

/**
 * Templating Class.
 *
 * A class to hold functions related to a page.
 */
class templating extends \Launchsite\abstracts\view implements \Launchsite\interfaces\templating_engine
{
	/**
	 * Paths to html page files.
	 *
	 * @var array An array of paths to html pages.
	 */
	private $view_paths = array();

	/**
	 * Paths to HTML layout files.
	 *
	 * @var array An array of paths to html layouts.
	 */
	private $template_paths = array();

	/**
	 * The page variables.
	 *
	 * @var array. 
	 */
	public $vars = array();

	/**
	 * Subdir to find page HTML in.
	 *
	 * @var string The name of the controller being used, maps to views subdir. 
	 */
	private $view_dir;

	/**
	 * Name of page HTML file.
	 *
	 * @var string The name of the method being used, maps to the view file to use.
	 */
	private $view_file;

	/**
	 * Name of the HTML template
	 *
	 * @var string The name of the template file to use.
	 */
	private $template_file;

	/**
	 * The page content that has been loaded.
	 *
	 * @var string The contents of the page to be displayed.
	 */
	public $page_content;

	/**
	 * Load the engine.
	 *
	 * @return bool 
	 */
	public function load_engine()
	{
		//Setup view paths
		$view_paths = array();
		$site_views = $this->launchsite()->get_config('SITE_VIEWS');
		$framework_views = $this->launchsite()->get_config('FRAMEWORK_VIEWS');
		if (is_array($site_views)) {
			foreach ($site_views as $path) {
				$view_paths[] = $path;
			}
		} elseif (is_string($site_views)) {
			$view_paths[] = $site_views;
		}

		$view_paths[] = $framework_views;

		foreach (array_unique($view_paths) as $path) {
			$this->add_view_path($path);
		}

		//Setup template paths
		$template_paths = array();
		$site_templates = $this->launchsite()->get_config('SITE_TEMPLATES');
		$framework_templates = $this->launchsite()->get_config('FRAMEWORK_TEMPLATES');
		if (is_array($site_templates)) {
			foreach ($site_templates as $path) {
				$template_paths[] = $path;
			}
		} elseif (is_string($site_templates)) {
			$template_paths[] = $site_templates;
		}

		$template_paths[] = $framework_templates;

		foreach (array_unique($template_paths) as $path) {
			$this->add_template_path($path);
		}

		//Setup session arrays for auto generates html
		//Pagination array
		!isset($_SESSION['PAGINATION']) ? $_SESSION['PAGINATION'] = array() : $_SESSION['PAGINATION'];
		//Tab management
		!isset($_SESSION['TABS']) ? $_SESSION['TABS'] = array() : $_SESSION['TABS'];
		//Filter management
		!isset($_SESSION['FILTERS']) ? $_SESSION['FILTERS'] = array() : $_SESSION['FILTERS'];

		return true;
	}

	/**
	 * Add a template path.
	 *
	 * @param string $path A full path to the directory containing layouts.
	 *
	 * @return void.
	 */
	public function add_template_path($path)
	{
		$this->template_paths[] = $path;
	}

	/**
	 * Add a view path.
	 *
	 * @param string $path A full path to the directory containing page contents.
	 *
	 * @return void.
	 */
	function add_view_path($path)
	{
		$this->view_paths[] = $path;
	}

	/**
	 * Get the controller, method and vars of the request and load the associated page.
	 *
	 * @param string $controller The name of the controller being used in the request.
	 *
	 * @param string $method The name of the method being run in the request.
	 *
	 * @param array $vars The array of vars that are generated from the controller.
	 *
	 * @param string $type The type of file to use in the page.
	 *
	 * @throws \Exception If there is a PHP error in the loaded view or layout.
	 *
	 * @return void Stores the contents of the page in $this->page_contents.
	 */
	public function setup_page($controller, $method, $vars, $type = 'view')
	{
		//Set the path to the html to display
		if (is_array($method)) {
			$this->view_file = $method[0];

			if (isset($method[1])) {
				$this->template_file = $method[1];
			}
		} else {
			$this->view_file = $method;
			$this->template_file = false;
		}

		$this->view_dir = $this->get_html_dir($type, $controller);
		$view_path = strlen($this->view_dir) ? $this->view_dir . DIRECTORY_SEPARATOR : '';

		//Load the page contents
		if (strlen($this->template_file)) {
			$this->page_contents = $this->load_page(array($view_path . $this->view_file, $this->template_file), $vars, $type);
		} else {
			$this->page_contents = $this->load_page($view_path . $this->view_file, $vars, $type);
		}
	}

	/**
	 * Load a view and then load the template, to create a page.
	 *
	 * @param mixed $contents Can be either a string path to view file, or an array with the elements view path and template name, to include a specific template.
	 *
	 * @param array $vars An array of variables returned from the controller.
	 *
	 * @param string $type The location for the view files. 
	 *
	 * @return string $page_contents A string containing the HTML of the page.
	 */
	public function load_page($contents, $vars = array(), $type = 'view')
	{
		$vars['contents'] = $contents;

		if (!is_array($vars) && !empty($vars)) {
			$vars = array('controller_var' => $vars);
		}

		if (is_array($contents)) {
			$vars['CONTENTS'] = $this->get_contents($contents[0], $vars, $type);

			if ($vars['CONTENTS'] === false) {
				$vars['CONTENTS'] = $this->get_contents('no_view', $vars, 'error');
			}

			$page_contents = $this->render_view($this->get_template_path($contents[1]), $vars);
		} else {
			$vars['CONTENTS'] = $this->get_contents($contents, $vars, $type);

			if ($vars['CONTENTS'] === false) {
				$vars['CONTENTS'] = $this->get_contents('no_view', $vars, 'error');
			}

			$page_contents = $this->render_view($this->get_template_path(), $vars);
		}

		$this->page_content = $page_contents;

		return $page_contents;
	}

	/**
	 * Find a view based on controller name.
	 *
	 * @param string $type The type of file to use.
	 *
	 * @param mixed $path False if none or type relative file path.
	 *
	 * @return string The path to the file.
	 */
	public function get_html_path($type = 'view', $path = false)
	{
		$tried_paths = array();

		$contents = $this->view_dir . DIRECTORY_SEPARATOR . $this->view_file;
		if ($path) {
			$contents = $path;
		}

		switch (strtolower($type)) {
			case 'view':
				$contents_paths[] = self::launchsite()->get_config('SITE_VIEWS');
				$contents_paths[] = self::launchsite()->get_config('FRAMEWORK_VIEWS');
				break;
			case 'error':
				$contents_paths[] = self::launchsite()->get_config('SITE_ERRORS');
				$contents_paths[] = self::launchsite()->get_config('FRAMEWORK_ERRORS');
				break;
			case 'root':
				$contents_paths[] = APPLICATION_PATH;
				break;
			default:
				$contents_paths[] = self::launchsite()->get_config('SITE_HTML');
				$contents_paths[] = self::launchsite()->get_config('FRAMEWORK_HTML');
				break;
		}

		foreach($contents_paths as $contents_path) {
			$path = $contents_path . $contents;
			
			foreach($this->launchsite()->get_config('VIEW_FILE_TYPES') as $file_ext) {
				$path_ext = $path . $file_ext;
				if (is_file($path_ext)) {
					return $path_ext;
				}
			}
		}

		return false;
	}

	/**
	 * Get HTML directory
	 *
	 * @param string $type The type of HTML file to find the directory for.
	 *
	 * @param string $controller The controller to match to the directory.
	 *
	 * @return string $path The path to the HMTL directory
	 */
	public function get_html_dir($type, $controller = '')
	{
		switch ($type) {
			case 'view':
				//Setup view folders
				$controller_class_array = explode('\\', $controller);
				$controller_class = end($controller_class_array);

				return str_replace('_controller', '', $controller_class);
			break;
			case 'error':
				return '';
			break;
			default:
				return $type;
			break;
		}
	}

	/**
	 * Get a view as a string adding in page variables.
	 *
	 * @param string $path The path to the file to render.
	 *
	 * @param array $vars The variables to be used in the page.
	 *
	 * @return string The file contents.
	 */
	public function render_view($path, $vars = array())
	{
		if (is_file($path)) {
			ob_start();

			require $path;

			return ob_get_clean();
		} else {
			$this->get_engine('response')->add_error("No view file found for path: $path", 500, 'no_view.php');
			return false;
		}
	}

	/**
	 * Function to get a view as page contents.
	 *
	 * @param string $contents The path to the contents.
	 *
	 * @param array $vars An array of the variables used in the page.
	 *
	 * @param string $type optionally use a known relative path. Defaults to views directory.
	 *
	 * @return string The contents of the page.
	 */
	public function get_contents($contents, $vars = false, $type = 'view')
	{
		$path = $this->get_html_path($type, $contents);
		if ($path) {
			$return = $this->render_view($path, $vars);

			if ($return !== false) {
				return $return;
			}
		}

		ob_get_clean();
		$vars = array(
			'Contents' => $contents, 
			'Vars' => $vars
		);

		$this->get_engine('logging')->log($vars, 'no_view.txt');
		$response = $this->get_engine('response');
		$response->add_error("Can't find a view file for contents: " . print_r($contents, 1), 500, 'no_view.php');
		$response->set_vars($vars);
		return false;
	}

	/**
	 * Get the site template.
	 *
	 * @param string $template The name of the template to use.
	 *
	 * @return string The path to the template file.
	 */
	public function get_template_path($template = false)
	{
		$site_templates = self::launchsite()->get_config('SITE_TEMPLATES');
		$default_template = self::launchsite()->get_config('DEFAULT_TEMPLATE');
		$framework_templates = self::launchsite()->get_config('FRAMEWORK_TEMPLATES');
		$framework_default_template = self::launchsite()->get_config('FRAMEWORK_DEFAULT_TEMPLATE');

		if (is_file($site_templates . $template)) {
			$template =  $site_templates . $template;
		} elseif (is_file($framework_templates . $template)) {
			$template = $framework_templates . $template;
		} elseif (is_file($site_templates . $default_template)) {
			$template = $site_templates . $default_template;
		} elseif (is_file($framework_templates . $framework_default_template)) {
			$template = $framework_templates . $framework_default_template;
		} else {
			$this->get_engine('response')->add_error("No Template file found for: $template", 500, 'no_view.php');
			$template = '';
		}

		return $template;
	}

	/**
	 * Show the page.
	 *
	 * @return void Echo's out the page contents.
	 */
	function show_page()
	{                
		echo $this->page_content;
	}

	/**
	 * Render the page in its context.
	 *
	 * @return bool True for should, false for shouldn't.
	 */
	static function should_render()
	{
		$render = true;

		//Command Line
		if (PHP_SAPI == 'cli') {
			$render = false;
		}

		//AJAX Requests
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$render = false;
		}

		return $render;
	}

	/**
	 * Load the error page. If the site doesn't have one, fall back to the framework error pages.
	 *
	 * @param string $error_page The error page to use.
	 */
	static function get_error_page_path($error_page = '')
	{
		//Get error dirs
		$error_dir = self::launchsite()->get_config('SITE_ERRORS');
		$framework_error_dir = self::launchsite()->get_config('FRAMEWORK_ERRORS');

		//Get framework error dirs
		$default_error_page = self::launchsite()->get_config('DEFAULT_ERROR_PAGE');
		$framework_default_error_page = self::launchsite()->get_config('FRAMEWORK_DEFAULT_ERROR_PAGE');

		//Default to framework if site has no page or default
		if (file_exists($error_dir . $error_page)) {
			$page = $error_dir . $error_page;
		}
		elseif (file_exists($error_dir . $default_error_page)) {
			$page = $error_dir . $default_error_page;
		}
		elseif (file_exists($framework_error_dir . $error_page)) {
			$page = $framework_error_dir . $error_page;
		}
		else {
			$page = $framework_error_dir . $framework_default_error_page;
		}

		return $page;
	}
}
