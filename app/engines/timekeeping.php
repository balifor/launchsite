<?php
/**
 * Timekeeping engine.
 */

namespace Launchsite\engines;

/**
 * Engine class.
 */
class timekeeping extends \Launchsite\abstracts\engine
{
	/**
	 * A function that is called when the engine is first loaded.
	 *
	 * @return mixed 
	 */
	public function load_engine()
	{
		//On load here
	}

	/**
	 * Function to quickly reformat time.
	 *
	 * @param string $time A time string in the format described by parameter 2.
	 *
	 * @param string $in_format A \Date valid format that the $time parameter is in.
	 *
	 * @param string $out_format A \Date valid format to return.
	 *
	 * @return string The formatted date.
	 */
	static function reformat_time($time, $in_format, $out_format)
	{
		$date = date_create_from_format($in_format, $time);

		return $date->format($out_format);
	}
}
