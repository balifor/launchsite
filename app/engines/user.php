<?php
/**
 * Current user engine.
 *
 * Implements a basic user system.
 */

namespace Launchsite\engines;

/**
 * Current user class.
 *
 * Class to allow for users on the site.
 */
class user extends \Launchsite\abstracts\engine implements \Launchsite\interfaces\user_engine
{
	/**
	 * A list of auth groups the user belongs to.
	 *
	 * @var array A list of all auth groups the user belongs to.
	 */
	private $current_user = false;

	/**
	 * Check if the user is logged in.
	 *
	 * @return bool True if logged in false if not.
	 */
	public function logged_in()
	{
		if (!isset($_SESSION['current_user_id'])) {
			return false;
		} elseif (strlen($_SESSION['current_user_id'])) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return the user from the session.
	 *
	 * @return user Return either the logged in user if exists or blank user.
	 */
	public function get_current_user()
	{
		if ($this->logged_in()) {
			if (isset($this->current_user) && is_object($this->current_user)) {
				return $this->current_user;
			} else {
				$user_class = $this->get_engine('user')->get_user_class();
				$user = new $user_class();

				$current_user = $user->load_current_user();
				if ($current_user) {
					$this->current_user = $current_user;
				}

				return $current_user;
			}
		} else {
			return false;
		}
	}

	/** 
	 * Return the users id from the session.
	 *
	 * @return bool false if not exists, int if it does.
	 */
	public function get_current_user_id()
	{
		if ($this->logged_in()) {
			return $this->current_user->get('id');;
		} else {
			return false;
		}
	}

	/**
	 * Set the current user.
	 *
	 * Add the user to the session if not CLI and load their auths.
	 *
	 * @param mixed $user The user class to use.
	 *
	 * @return bool True on success, false on failure.
	 */
	public function set_current_user($user)
	{
		$id = $user->get('id');
		if ($id) {
			$_SESSION['current_user_id'] = $id;

			$this->current_user = $user;

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get the user model to use.
	 *
	 * @return User
	 */
	public function get_user_class()
	{
		$user_class = $this->launchsite()->get_config('USER_CLASS');

		if (class_exists($user_class)) {
			return $user_class;
		} else {
			$this->get_engine('response')->add_error("Couldn't find the user class: $user_class, using default instead.");
		
			return $user_class;
		}		
	}
}
