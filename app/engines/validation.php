<?php
/**
 * Validation engine.
 *
 * An easy way to validate data.
 */

namespace Launchsite\engines;

/**
 * Validator class.
 *
 * Some common validations.
 */
class validation extends \Launchsite\abstracts\engine implements \Launchsite\interfaces\validation_engine
{
	/**
	 * Validate an email address.
	 *
	 * @param string $email The email address to validate.
	 *
	 * @param mixed $if_false_return Optional return value on false.
	 *
	 * @return string The email or an empty string if the optional return value isn't set.
	 */
    static function validate_email($email, $if_false_return = false)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) != false) {
            return $email;
        } else {
            return $if_false_return;
        }
    }
}
