<?php
/**
 * example engine.
 *
 * Create or replace an engine for the framework.
 */

namespace Launchsite\engines;

/**
 * example class.
 */
class example extends \Launchsite\abstracts\engine
{
	/**
	 * A function that is called when the engine is first loaded.
	 *
	 * @return mixed 
	 */
	public function load_engine()
	{
		//On load here
	}
}
