<?php
/**
 * Caching engine.
 *
 * Allows the caching of information for the site.
 */

namespace Launchsite\engines;

/**
 * Cache class.
 *
 * A class to simplify caching.
 */
class cache extends \Launchsite\abstracts\engine implements \Launchsite\interfaces\cache_engine
{       
	/**        
	 * Add an item to the cache.
	 *
	 * @param string $file_path The path to the file inside the cache directory.
	 *
	 * @param string $data The data to be stored in the cache file.
	 *
	 * @param bool $append Optionally append data instead of replacing.
	 *
	 * @return bool True on success, false on failure.
	 */
	function cache($file_path, $data, $append = false)
	{
		$cache_path = $this->launchsite()->get_config('CACHE_PATH'); 
		$path_array = explode(DIRECTORY_SEPARATOR, $file_path);
		$file = array_pop($path_array);
		$path = $cache_path . implode(DIRECTORY_SEPARATOR, $path_array);

		if (!file_exists($path)) {
			mkdir($path, 0777, true);
		}
		
		try {
			if ($append === false) {
				$result = file_put_contents($cache_path . $file_path, $data);
			} else {
				$result = file_put_contents($cache_path . $file_path, $data, FILE_APPEND);
			}

			return true;
		} catch (Exception $e) {
			return false;
		}
	}

	/**
	 * Remove an item from the cache.
	 *
	 * @param string $file_path The file path of the file to be removed from the cache.
	 *
	 * @return bool True on success, false on failure.
	 */
	function delete_cached($file_path)
	{
		try {
			$cache_path = $this->launchsite()->get_config('CACHE_PATH'); 
			if (is_readable($cache_path . $file_path)) {
				unlink($cache_path . $file_path);
				return true;
			} else {
				return false;
			}
		} catch (Exception $e) {
			return false;
		}
	}

	/**
	 * Get an item from the cache.
	 *
	 * @param string $file_path The path to the file in the cache directory. 
	 *
	 * @param string $time_limit A string in strtotime format, I.E: 5 mins.
	 *
	 * @return bool True on success, false on failure. 
	 */
	function get_cached($file_path, $time_limit = false)
	{
		try {
			$cache_path = $this->launchsite()->get_config('CACHE_PATH'); 
			if ($time_limit === false ) {
				return file_get_contents($cache_path . $file_path);
			} elseif (is_readable($cache_path . $file_path)) {
				if (filemtime($cache_path . $file_path) < strtotime('-' . $time_limit)) {
					return file_get_contents($cache_path . $file_path);
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception $e) {
			return false;
		}
	}
}
