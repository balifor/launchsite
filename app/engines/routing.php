<?php
/**
 * Routing engine.
 *
 * A system to allow for creation of HTTP method specific routes with pretty URLs.
 */

namespace Launchsite\engines;

/**
 * Routing class.
 *
 * A class to allow human readable URLs and link authorisation.
 */
class routing extends \Launchsite\abstracts\engine implements \Launchsite\interfaces\routing_engine
{
	/**
	 * A list of known routes.
	 *
	 * @var array.
	 */
	private $routes = array();

	/**
	 * The current route.
	 *
	 * @var array.
	 */
	private $current_route = array();

	/**
	 * Get request applicable routes.
	 *
	 * Get an array of routes.
	 *
	 * @param bool $all Return all routes, or just method matched.
	 */
	public function get_routes($all = false)
	{
		//Get routes that accept any method.
		$routes = isset($this->routes['ANY']) ? $this->routes['ANY'] : array();

		if ($all) {
			$return = $this->routes;
		} else {
			//Get the request method.
			if (isset($_SERVER['REQUEST_METHOD'])) {
				$method = $_SERVER['REQUEST_METHOD'];
			} else {
				$method = 'CLI';
			}

			//Get routes that match the request method.
			if (isset($this->routes[$method])) {
				$return = array_merge($this->routes[$method], $routes);
			} else {
				$return = $routes;
			}
		}

		return $return;
	}

	/**
	 * Find a route by its name.
	 *
	 * @param string $name The name of the route to get.
	 *
	 * @param string $type The HTTP method type to match.
	 *
	 * @return string|bool Returns a route array if found of false if not.
	 */
	public function get_named($name, $type = 'ANY')
	{
		foreach ($this->routes as $route_type => $route_array) {
			if (strtoupper($type) == $route_type) {
				foreach ($route_array as $route) {
					if ($route['route_name'] == $name) {
						return $route;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Generate a URL. Find the route by name and use an array to replace :VARs.
	 *
	 * @param string $named The url name to get.
	 *
	 * @param array $url_vars If the URL has variables in it, add them to this array.
	 *
	 * @param HTTP method that the route is accessible on.
	 *
	 * @return string The url for the route.
	 */
	public function url($named, $url_vars = false, $type = 'ANY')
	{
		$matched = $this->get_named($named, $type);

		if ($matched != false) {
			if ($url_vars == false) {
				return $matched['url'];
			} else {
				$url_parts = explode('/', $matched['url']);

				$count = 0;
				$new_url = array();
				if (count($url_parts)) {
					foreach ($url_parts as $part) {
						if (substr($part, 0, 1) == ':') {
							$var_parts = explode(':', $part);

							if (isset($var_parts[1]) && isset($url_vars[$var_parts[1]])) {
								$new_url[] = $url_vars[$var_parts[1]];
							} elseif (isset($url_vars[$count])) {
								$new_url[] = $url_vars[$count];
							} elseif (isset($var_parts[1])) {
								$new_url[] = $var_parts[1];
							} else {
								$new_url[] = 'default';
							}

							$count++;
						} else {
							$new_url[] = $part;
						}
					}

					return implode('/', $new_url);
				} else {
					return $matched[0];
				}
			}
		} else {
			return '/';
		}
	}

	/**
	 * Basic create a route function.
	 *
	 * @param string $name The name of the route.
	 *
	 * @param array $route The route array containing url, controller and method.
	 *
	 * @param string $type The HTTP method the route is created for.
	 *
	 * @return bool True on success, false on failure.
	 */
	public function add_route($name, $route, $type = 'ANY')
	{
		$full_route['route_name'] = $name;

		if (isset($route[0]) && isset($route[1]) && isset($route[2])) {
			$full_route['url'] = '/' . ltrim($route[0], '/');
			$full_route['controller'] = $route[1];
			$full_route['method'] = $route[2];
		} else {
			return false;
		}

		$this->routes[strtoupper($type)][$name] = $full_route;

		return true;
	}

	/**
	 * Go to a named route.
	 *
	 * @param string $name The name of the route to go to.
	 *
	 * @param array $url_vars Optional if the url has variables, supply them here.
	 *
	 * @param string $type The HTTP method of the route to find.
	 *
	 * @return void Go to a new URL.
	 */
	public function go_to($name, $url_vars = false, $type = 'ANY')
	{
		$url = $this->url($name, $url_vars, $type);
		header("Location: $url");
		exit();
	}

	/**
	 * Get the name of the current route.
	 *
	 * @return string The name of the current route.
	 */
	public function get_current_route_name()
	{
		$current = $this->match_route();

		return $current['route_name'];
	}

	/**
	 * Check whether the given route name is the current route. 
	 *
	 * @param string $route_name The route name to match.
	 *
	 * @param mixed $return Optionally change the true return value.
	 *
	 * @return bool|mixed Returns true on match or false on no match, true return is changeable.
	 */
	public function if_current_route_is($route_name, $return = true)
	{
		if ($this->get_current_route_name() == $route_name) {
			return $return;
		} else {
			return false;
		}
	}

	/**
	 * Check whether the given routes names match the current route. 
	 *
	 * @param array $route_names An array of routes to check. 
	 *
	 * @param mixed $return Optionally change the true return value.
	 *
	 * @return bool|mixed Returns true on match or false on no match, true return is changeable.
	 */
	public function if_current_route_in($route_names, $return = true)
	{
		if (in_array($this->get_current_route_name(), $route_names)) {
			return $return;
		} else {
			return false;
		}
	}

	/**
	 * Match the requested url to a route.
	 *
	 * @param string $url Find the route for the given URL.
	 *
	 * @return bool|array Returns array if found or false if not.
	 */ 
	public function match_route($url = false)
	{
		global $argv;

		//Check to see if the route has already been found
		if (!$url && !empty($this->current_route)) {
			return $this->current_route;
		}

		//Get the url
		if ($url === false) {
			if (isset($_SERVER) && isset($_SERVER['REQUEST_URI'])) {
				$url = parse_url($_SERVER['REQUEST_URI']);
			} else {
				$url['path'] = $argv[1];
			}
		} else {
			$url = parse_url($url);
		}

		$url = $url == false ? $this->get_url() : $url;

		//Check if it is document root
		if (in_array($url['path'], array('/index.php', '/', ''))) {
			$request_route = '/';
		} else {
			$request_route = $url['path'];
		}

		//Iterate over relevant routes
		foreach ($this->get_routes() as $route_array) {
			//Get the route parts
			$this_route_array = explode('/', trim($route_array['url'], '/'));
			$request_route_array = explode('/', trim($request_route, '/'));

			//Cut down the processing
			if (count($this_route_array) === count($request_route_array)) {
				//Catch URL variables
				$count = 0;
				$i = 1;
				foreach ($this_route_array as $part) {
					if (substr($part, 0, 1) == ':') {
						$var_parts = explode(':',$part);

						if (count($var_parts) > 1) {
							$var_name = $var_parts[1];
						} else {
							$var_name = '';
						}

						if (strlen($var_name)) {
							$vars[$var_name] = $request_route_array[$count];
						} else {
							$vars['url_var_'.$i] = $request_route_array[$count];
						}

						$check_route_array[] = $request_route_array[$count];
						$i++;
					} else {
						$check_route_array[] = $part;
					}

					$count++;
				}

				//Recreate the url path including variables
				$this_route = implode('/', $check_route_array);
				unset($check_route_array);

				//Get method and controller values from route array
				if (trim($this_route, '/') == trim($request_route, '/')) {
					if (isset($vars) && count($vars) > 0) {
						$return = array_merge($route_array, array('vars' => $vars));
					} else {
						$return = $route_array;                          
					}

					if ($url === false) {
						$this->current_route = $return;
					}

					return $return;
				} else {
					unset($vars);
				}
			}
		}

		return false;
	}

	/**
	 * Get the URL
	 */
	public function get_url()
	{
		global $argv;

		//Get the url
		if ($this->get_engine('request')->get_request_type() != 'cli') {
			$url = parse_url($_SERVER['REQUEST_URI']);
		} else {
			$url['path'] = $argv[1];
		}

		return $url;
	}
}
