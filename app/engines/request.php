<?php
/**
 * Request engine.
 *
 * Handles the request.
 */

namespace Launchsite\engines;

/**
 * Request class.
 *
 * A class to handle a request.
 */
class request extends \Launchsite\abstracts\engine implements \Launchsite\interfaces\response_engine
{
	/**
	 * The type of request
	 *
	 * @var string $request_type
	 */
	public $request_type;

	/**
	 * The url for the request.
	 *
	 * @var string $url
	 */
	public $url;

	/**
	 * The request headers
	 *
	 * @var array $headers
	 */
	public $headers = array();

	/**
	 * The current route.
	 *
	 * @var array $current_route
	 */
	public $current_route;

	/**
	 * Variables for the route.
	 *
	 * @var array $vars
	 */
	public $vars = array();

	/**
	 * Function to set the url.
	 *
	 * @param string $url The url for the request. 
	 *
	 * @return void
	 */
	public function set_url($url)
	{
		$this->url = $url;
	}

	/**
	 * Function to get the headers.
	 *
	 * @return array
	 */
	public function get_headers()
	{
		if (function_exists('getallheaders')) {
			return getallheaders();
		} else {
			$headers = ''; 

			foreach ($_SERVER as $name => $value) { 
				if (substr($name, 0, 5) == 'HTTP_') { 
					$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
				} 
			} 

			return $headers;
		}
	}

	/**
	 * Function to set the current route.
	 *
	 * @param array $route The route for the request. 
	 *
	 * @return void
	 */
	public function set_route($route)
	{
		$this->current_route = $route;
	}

	/**
	 * Function to run the request.
	 *
	 * @param string $url The requested url.
	 *
	 * @return void
	 */
	public function run_request($url = false)
	{
		//Get the response
		$response = $this->get_engine('response');

		//Set the response type based on request type.
		$this->get_request_type();

		//Keep url
		$this->set_url($url);

		//Get the headers
		$this->headers = $this->get_headers();

		//Match url to a route and get url variables
		$this->set_route($this->get_engine('routing')->match_route($url));
       
		//Check if 404
		if ($this->current_route === false) {
			$response->add_error("No route found for url: $url", 404, '404.php');
		} else {
			//Pass them to the request
			$response->set_route($this->current_route);

			$route_controller = $this->current_route['controller'];
			$route_method = $this->current_route['method'];
			$route_name = $this->current_route['method'];
			$vars = isset($this->current_route['vars']) ? $this->current_route['vars'] : array();
 
			//Get the controller to run from the route.
			$controller = $this->get_controller($route_controller);

			if ($controller) {
				//Check if 401
				if (!$controller->check_login()) {
					if ($controller->use_api) {
						$response->add_error("Login Required", 401, '401.php');
					} else {
						if ($this->launchsite()->get_config('REDIRECT_TO_LOGIN')) {
							$this->get_engine('routing')->go_to('login');
						} else {
							$response->add_error("Login Required", 401, '401.php');
						}
					}
				} else {
					//Check if 403
					if (!$controller->check_authed()) {
						$response->add_error("Not Authorised", 403, '403.php');
					} else {
						//Run the requested method.
						$this->vars = $this->run_method($controller, $route_method, $vars);
					}
				}
			}
		}
		
		return $this->vars;
	}

	/**
	 * Return an instance of the controller from the route.
	 *
	 * @param string $controller_class The controller class to load.
	 *
	 * @return mixed The controller to be run.
	 */
	protected function get_controller($controller_class)
	{
		$response = $this->get_engine('response');

		//If an autoloadable class name is found, use it
		if (class_exists($controller_class)) {
			$use_controller = $controller_class;
		} else {
			//Check it's default locations
			if (is_readable($this->launchsite()->get_config('SITE_CONTROLLERS') . $controller_class . '.php')) {
				require_once $this->launchsite()->get_config('SITE_CONTROLLERS') . $controller_class . '.php';
			}
			
			if (class_exists($controller_class)) {
				$use_controller = $controller_class;
			} else {
				//Check if its a fallback to a Launchsite controller.
				if (class_exists('Launchsite\controllers\\' . $controller_class)) {
					$use_controller = 'Launchsite\controllers\\' . $controller_class; 
				} else {
					$response->add_error("Couldn't find controller: $controller_class", 500, 'bad_controller.php');
					return false;
				}
			}
		}

		try {
			$controller = new $use_controller();
			return $controller;
		} catch (Exception $e) {
			$response->add_error("Loading controller: $framework_controller threw an exception: " . $e->getMessage(), 500, 'bad_controller.php');
			return false;
		}
	}

	/**
	 * Run the method called in the route.
	 *
	 * @param mixed $controller The controller class to run the method on.
	 *
	 * @param string $method The method to run.
	 *
	 * @param array $vars An array of variables from the route.
	 *
	 * @return bool True on success, false on failure.
	 */
	protected function run_method($controller, $method, $vars = array())
	{
		$response = $this->get_engine('response');

		if (is_callable(array($controller, $method))) {
			try {
				$return = call_user_func_array(array($controller, $method), $vars);
				return $return;
			} catch (\Exception $e) {
				$response->add_error("Running the route method threw an exception: " . $e->getMessage(), 501, 'bad_method.php');
			} catch (\PDOException $e) {
				$response->add_error("Running the route method threw an exception: " . $e->getMessage(), 501, 'bad_method.php');
			}
		} else {
			$controller_class = get_class($controller);
			$response->add_error("The controller: $controller_class, doesn't have a method called: $method", 501, 'bad_method.php');
		}
	}

	/**
	 * Check if this is a common request type.
	 *
	 * @return string The request type.
	 */
	public function get_request_type()
	{
		switch(true)
		{
			case !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest':
				$return = 'json';
				break;
			case PHP_SAPI == 'cli' || defined('STDIN'):
				$return = 'cli';
				break;
			case (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443:
			default:
				$return = 'html';
				break;
		}

		//Set a default response type
		$this->get_engine('response')->set_type($return);
		
		//Set the request type
		$this->request_type = $return;

		return $return;
	}
}
