<?php
/**
 * Email engine.
 *
 * The email management engine.
 */

namespace Launchsite\engines;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * Email Engine class.
 */
class email extends \Launchsite\abstracts\engine
{
	/**
	 * Array of email hosts
	 */
	public $hosts = array();

	/**
	 * A function that is called when the engine is first loaded.
	 *
	 * @return mixed 
	 */
	public function load_engine()
	{
		require_once $this->launchsite()->get_config('FRAMEWORK_LIB') . 'phpmailer/src/Exception.php';
		require_once $this->launchsite()->get_config('FRAMEWORK_LIB') . 'phpmailer/src/PHPMailer.php';
		require_once $this->launchsite()->get_config('FRAMEWORK_LIB') . 'phpmailer/src/SMTP.php';
	}

	/**
	 * Get an email host.
	 *
	 * @param mixed $name The name of the database to get.
	 *
	 * @return mixed An email host.
	 */
	public function get_host($name = 'MAIN')
	{
		if (isset($this->hosts[$name]) && is_object($this->hosts[$name])) {
			return $this->hosts[$name];
		} else {
			$conf = $this->launchsite()->get_config('EMAIL');
			if (isset($conf[$name])) {
				$this->hosts[$name] = new PHPMailer();

				$this->hosts[$name]->IsSMTP();
				$this->hosts[$name]->Host = $conf[$name]['HOST'];
				$this->hosts[$name]->SMTPAuth = $conf[$name]['AUTH'];
				$this->hosts[$name]->Port = $conf[$name]['PORT'];

				if ($conf[$name]['AUTH_TYPE'] != '') {
					$this->hosts[$name]->SMTPSecure = $conf[$name]['AUTH_TYPE'];
				}

				return $this->hosts[$name];
			} else {
				if ($name != 'MAIN') {
					$this->get_engine('response')->add_error("No Email config for: $name exists.");
				}

				$this->hosts[$name] = new PHPMailer();
				return $this->hosts[$name];
			}
		}
	}

	/**
	 * Send an Email.
	 *
	 * @param mixed $to A string or array of email addresses.
	 *
	 * @param string $subject The subject line for the email.
	 *
	 * @param string $message The message to send.
	 *
	 * @param string $from_address The from address for the email.
	 *
	 * @param string $from_name The name of the from email address user.
	 *
	 * @param array $attachments An array of attachments.
	 *
	 * @return bool True on success, false on failure.
	 */
	public function email($to, $subject, $message, $from_address = 'root@example.com', $from_name = '', $attachments = array(), $host = 'MAIN')
	{
		$mail = $this->get_host($host);
		$mail->SetFrom($from_address, $from_name);
		$mail->AddReplyTo($from_address, $from_name);
		$mail->Subject = $subject;
		$mail->MsgHTML($message);

		if (is_array($to)) {
			foreach ($to as $address) {
				$mail->AddAddress($address);
			}
		} else {
			$mail->AddAddress($to);
		}

		if (count($attachments)) {
			foreach ($attachments as $attachment) {
				$mail->AddAttachment($attachment);
			}
		}

		if (!$mail->Send()) {
			$this->get_engine('response')->add_error($mail->ErrorInfo);
			return false;
		} else {
			return true;
		}
	}
}
