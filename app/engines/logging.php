<?php
/**
 * Logging engine.
 *
 * A class to hold a log of the request run through.
 */

namespace Launchsite\engines;

/**
 * Logging class.
 *
 * A class to log to the file system.
 */
class logging extends \Launchsite\abstracts\engine implements \Launchsite\interfaces\logging_engine
{
	/**
	 * The log of the request run through.
	 *
	 * @var array.
	 */
	private $log = array();

	/**
	 * Show system resource usage as html.
	 *
	 * @return string The HTML for the resource usage block.
	 */
	public function resource_usage()
	{
		$sys_mem_usage = self::convert_to_mb(memory_get_peak_usage(true));
		$php_mem_usage = self::convert_to_mb(memory_get_peak_usage(false));
		$php_request_time = self::time_since($_SERVER['REQUEST_TIME']);
		$time_since_launch = microtime() - self::launchsite()->start_time;
		$html = <<<HTML
			<div class="table-responsive">
				<table id='resource_usage' class="table table-bordered">
					<tr>
						<td>PHP Memory Usage:</td>
						<td>{$php_mem_usage}</td>
					</tr>
					<tr>
						<td>SYS Memory Usage:</td>
						<td>{$sys_mem_usage}</td>
					</tr>
					<tr>
						<td>Request Start Time:</td>
						<td>{$php_request_time}</td>
					</tr>
					<tr>
						<td>Launchsite Start Time:</td>
						<td>{$time_since_launch}</td>
					</tr>
				</table>
			</div>
HTML;
		return $html;
	}

	/**
	 * Convert int of bits to readable number.
	 *
	 * @param int $size The number of bits.
	 *
	 * @return string A readable number for the number of bits.
	 */
	public function convert_to_mb($size)
	{
		$unit = array('b','kb','mb','gb','tb','pb');
		return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
	}

	/**
	 * Log to the file system.
	 *
	 * @param string $type The type of log message.
	 *
	 * @param mixed $message The contents of the log message.
	 *
	 * @param string $to_file The name of the file you want to store the log in if at all.
	 *
	 * @param mixed $email Either an array or string of email addresses to send to.
	 *
	 * @return void.
	 */
	public function log($message, $to_file = false, $email = false, $print = false)
	{
		if (!is_string($message)) {
			$message = print_r($message, 1);
		}

		$time = date('d-m-Y H:i:s', strtotime('now'));

		$this->add_log(array('time' => $time, 'message' => $message));

		if ($print) {
			echo 'Time: ' . $time . PHP_EOL;
			echo 'Message: ' . $message . PHP_EOL . PHP_EOL;
		}

		if ($to_file) {
			$this->log_to_file($message, $to_file);
		}
	}

	/**
	 * Set a log item.
	 *
	 * @param string $key The type of log to add.
	 *
	 * @param string $value The contents of the log.
	 *
	 * @return void 
	 */
	protected function add_log($value)
	{
		$this->log[] = $value;
	}

	/**
	 * Get the log.
	 *
	 * @param string $key The type of log to get.
	 *
	 * @return mixed Array of log entries if it exists, null if not.
	 */
	public function get_log($key = false)
	{
		if (!$key) {
			return $this->log;
		} elseif (isset($this->get_log[$key])) {
			return $this->log[$key];
		} else {
			return null;
		}
	}

	/**
	 * Log to a file.
	 *
	 * @param string $message The message to be logged.
	 *
	 * @param string $file The file to log too. Defaults to mainlog.txt.
	 *
	 * @return string The log that was put in the file.
	 */
	public function log_to_file($message, $file = false, $use_full = false)
	{	
		$time = date('d-m-Y H:i:s', strtotime('now'));

		$mem_usage = $this->convert_to_mb(memory_get_usage());
		$peak_mem_usage = $this->convert_to_mb(memory_get_peak_usage());

		$session = '';
		if (!empty($_SESSION)) {
			$session = print_r($_SESSION, 1);
		}

		$config = print_r($this->launchsite()->get_config(), 1);

		$server = print_r($_SERVER, 1);

		$path = $this->launchsite()->get_config('LOGS_PATH');   
		if ($file !== false && strlen($file)) {
			$path .= $file;
		} else {            
			$path .= 'mainlog.txt';
		}

		$full_log = '';
		if ($use_full) {
			$full_log = <<<LOG_IT
	<LS-SESSION>
		{$session}
	</LS-SESSION>
	<LS-CONFIG>
		{$config}
	</LS-CONFIG>
	<LS-SERVER>
		{$server}
	</LS-SERVER>

LOG_IT;
		}

		$log = <<<LOG_IT
<LS-SITE-LOG>
	<LS-SYS>
		TIME: {$time}
		MEMORY USAGE: {$mem_usage}
		PEAK MEMORY USAGE: {$peak_mem_usage}
	</LS-SYS>
	<LS-MESSAGE>
		{$message}
	</LS-MESSAGE>
	$full_log
</LS-SITE_LOG>

LOG_IT;

		if (is_readable($this->launchsite()->get_config('LOGS_PATH'))) {
			file_put_contents($path, $log, FILE_APPEND);
		} else {
			mkdir($this->launchsite()->get_config('LOGS_PATH'));
			file_put_contents($path, $log, FILE_APPEND);
		}
	}

	/**
	 * Function to return time since a given time.
	 * 
	 * @param int $time The time you want to get the time since.
	 *
	 * @return string A textual representation of the amount of time since.
	 */
	static function time_since($time)
	{
		if (!is_numeric($time)) {
			$time = strtotime($time);
		}

		$current = time();

		$diff = $current - $time;

		$tokens = array 
			(
			 31536000 => 'year',
			 2592000 => 'month',
			 604800 => 'week',
			 86400 => 'day',
			 3600 => 'hour',
			 60 => 'minute',
			 1 => 'second'
			);

		foreach ($tokens as $unit => $text) {
			if ($diff === 0) {
				$diff = 1;
			}

			if ($diff < $unit) {
				continue;
			}   else {
				$numberOfUnits = floor($diff / $unit);
				return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '') . ' ago';
			}
		}
	}

	/**
	 * Get the details of an exception.
	 *
	 * @param \Exception Exception to get details of.
	 *
	 * @return array An array of the exceptions details.
	 */
	static function get_details($exception)
	{
		$vars['Exception Type'] = get_class($exception);
		$vars['Message'] = $exception->getMessage();
		$vars['Code'] = $exception->getCode();
		$vars['File'] = $exception->getFile();
		$vars['Line'] = $exception->getLine();
		$vars['Trace'] = $exception->getTraceAsString();

		return $vars;
	}
}
