<?php
/**
 * Filesystem engine.
 */

namespace Launchsite\engines;

/**
 * Engine class.
 */
class filesystem extends \Launchsite\abstracts\engine
{
	/**
	 * A function that is called when the engine is first loaded.
	 *
	 * @return mixed 
	 */
	public function load_engine()
	{
		//On load here
	}

	/**
	 * Recurisively copy a directory.
	 *
	 * @param string $source The source file/folder.
	 *
	 * @param string $destination The destination of the file/folder.
	 */
	public function recursive_copy($source, $destination)
	{
		if (is_dir($source)) {
			if(!is_dir($destination)) {
				mkdir($destination, 0777, true);
			}

			$files = scandir($source);
			foreach ($files as $file) {
				if ($file != "." && $file != "..") {
					$this->recursive_copy("$source/$file", "$destination/$file");
				}
			}
		} else if (file_exists($source)) {
			copy($source, $destination);
		} else {
			return false;
		}

		return true;
	}
}
