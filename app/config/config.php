<?php
/**
 * Framework Config.
 *
 * The default config for the framework.
 * They can be overwritten in SITE . config/config.php.
 */

	use \Launchsite\launcher\launcher as launcher;

	/**
	 * Get an instance of the launcher.
	 */
		$launchsite = launcher::launch();

		//Framework Autoload paths.
		$launchsite->set_config('FRAMEWORK_AUTOLOAD_PATHS', array(
			'Launchsite' => FRAMEWORK . 'app' . DIRECTORY_SEPARATOR,
		));

	/**
	 * Default user class for the user engine to use.
	 */
		//User Class.
		$launchsite->set_config('USER_CLASS', '\Launchsite\models\user');

	/**
	 * Default engines to run launchsite.
	 */
		//Engines to use.
		$launchsite->set_config('ENGINES', array(
			'api'         => '\Launchsite\engines\api',
			'asset'	      => '\Launchsite\engines\asset',
			'cache'       => '\Launchsite\engines\cache',
			'database'    => '\Launchsite\engines\database',
			'email'		  => '\Launchsite\engines\email',
			'filesystem'  => '\Launchsite\engines\filesystem',
			'logging'     => '\Launchsite\engines\logging',
			'request'     => '\Launchsite\engines\request',
			'response'    => '\Launchsite\engines\response',
			'routing'     => '\Launchsite\engines\routing',
			'sorting'     => '\Launchsite\engines\sorting',
			'templating'  => '\Launchsite\engines\templating',
			'timekeeping' => '\Launchsite\engines\timekeeping',
			'user'        => '\Launchsite\engines\user',
			'validation'  => '\Launchsite\engines\validation',
		));

	/**
	 * Default asset types
	 */
		//Asset paths
		$launchsite->set_config('ASSET_INFO', array(
			'images' => array(
				'path' => 'images' . DIRECTORY_SEPARATOR,
				'table' => 'images',
			),
			'downloads' => array(
				'path' => 'downloads' . DIRECTORY_SEPARATOR,
				'table' => 'downloads',
			),
		));

	/**
	 * Default site setup
	 */
		//The name of the site.
		$launchsite->set_config('SITE_NAME', 'LaunchSite');

		//Timezone
		$launchsite->set_config('TIMEZONE', 'Europe/London');

		//The admin email address.
		$host = isset($_SERVER['HTTP_HOST)']) ? $_SERVER['HTTP_HOST'] : 'localhost';
		$launchsite->set_config('SYS_ADMIN_EMAIL', 'admin@' . $host);

	/**
	 * Default environment setup
	 */
		//Sets whether the site is in dev or production, (live or dev)
		$launchsite->set_config('ENV', null !== $launchsite->get_config('ENV') ? $launchsite->get_config('ENV') : 'dev');

		//Debug.
		$launchsite->set_config('DEBUG', null !== $launchsite->get_config('DEBUG') ? $launchsite->get_config('DEBUG') : true);

		//Debug level.
		$launchsite->set_config('DEBUG_LEVEL', E_ALL | E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

	/**
	 * Default login setup
	 */
		//Allow Registration?
		$launchsite->set_config('ALLOW_REGISTRATION', true);

		//How long before login expires.
		$launchsite->set_config('SESSION_TIMEOUT', 60 * 60); //1 hour

		//Routes that don't require login access to use.
		$launchsite->set_config('NO_LOGIN_REQUIRED_ROUTES', array('ajax login', 'login', 'reset', 'recover', 'register', 'update password', 'manage tabs', 'get managed tab', 'get api token'));

		//Redirect to login or show 401
		$launchsite->set_config('REDIRECT_TO_LOGIN', true);

	/**
	 * Default password management
	 */
		//Length of time before a password reset token expires.
		$launchsite->set_config('PASSWORD_RESET_LIMIT', '1 hour');

	/**
	 * Page display options
	 */
		//Default per page options for pagination.
		$launchsite->set_config('PER_PAGE_OPTIONS', array('1', '5', '10', '25', '50'));

	/**
	 * File Types
	 */
		//File types to use when searching for pages.
		$launchsite->set_config('VIEW_FILE_TYPES', array('', '.php', '.html'));

		//Allowed file upload types.
		$launchsite->set_config('ALLOWED_UPLOAD_FILE_TYPES', array('pdf', 'gif', 'png', 'jpg', 'jpeg', 'xls', 'doc', 'docx', 'xls', 'csv', 'txt'));


	/**
	 * Example Email setup
	 *//*
		$launchsite->set_config('EMAIL', array(
			'HOST' => 'localhost',
			'PORT' => '25',
			'AUTH_TYPE' => '',
			'AUTH' => false.
			'USER' => '',
			'PASS' => '',
		), 'MAIN');
	*/

	/**
	 * Example DB setup
	 *//*
		  $launchsite->set_config('DB', array(
		  'DB_TYPE' => 'pdo_db',
		  'TYPE' => 'mysql',
		  'HOST' => 'localhost',
		  'NAME' => 'example',
		  'PORT' => '3306',
		  'USER' => 'root',
		  'PASS' => '',
		  ), 'MAIN');
	*/
		$launchsite->set_config('DEFAULT_DB_CONFIG', 'MAIN');

	/**
	 * Default site template paths
	 */
		//Site views and templates
		$launchsite->set_config('SITE_VIEWS', SITE . 'app/views/');
		$launchsite->set_config('SITE_ERRORS', SITE . 'app/views/errors/');
		$launchsite->set_config('SITE_EMAILS', SITE . 'app/views/emails/');
		$launchsite->set_config('SITE_TEMPLATES', SITE . 'app/views/templates/');
		$launchsite->set_config('DEFAULT_TEMPLATE', 'template.php');
		$launchsite->set_config('DEFAULT_ERROR_PAGE', 'default.php');
		$launchsite->set_config('DEFAULT_ERROR_TEMPLATE', 'template.php');

	/**
	 * Framework template paths
	 */
		//Framework views and templates
		$launchsite->set_config('FRAMEWORK_VIEWS', FRAMEWORK . 'app/views/');
		$launchsite->set_config('FRAMEWORK_ERRORS', FRAMEWORK . 'app/views/errors/');
		$launchsite->set_config('FRAMEWORK_EMAILS', FRAMEWORK . 'app/views/emails/');
		$launchsite->set_config('FRAMEWORK_TEMPLATES', FRAMEWORK . 'app/views/templates/');
		$launchsite->set_config('FRAMEWORK_DEFAULT_TEMPLATE', 'template.php');
		$launchsite->set_config('FRAMEWORK_DEFAULT_ERROR_PAGE', 'default.php');
		$launchsite->set_config('FRAMEWORK_DEFAULT_ERROR_TEMPLATE', 'template.php');

	/**
	 * Framework Lib
	 */
		$launchsite->set_config('FRAMEWORK_LIB', FRAMEWORK . 'lib/');

	/**
	 * Default logging config
	 */
		//Log folder path.
		$launchsite->set_config('LOGS_PATH', SITE . 'logs/');

		//Email logs to admin email?  
		$launchsite->set_config('EMAIL_LOG', true);

		//And These emails.
		$launchsite->set_config('EMAIL_LOG_TO', array());

	/**
	 * Default paths to folders
	 */
		//Cache folder path.
		$launchsite->set_config('CACHE_PATH', SITE . 'cache/');

		//Controllers path.
		$launchsite->set_config('SITE_CONTROLLERS', SITE . 'app/controllers/');

		//Databases path.
		$launchsite->set_config('SITE_DATABASES', SITE . 'app/databases/');

		//Migrations path.
		$launchsite->set_config('MIGRATIONS', SITE . 'app/migrations/');
