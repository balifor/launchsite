<?php
/**
 * Routes for the application.
 */

namespace Launchsite\config;

use Launchsite\launcher\launcher;
$router = launcher::launch()->get_engine('routing');

/**
 * Framework Routes
 */
    /**
     * Default site routes 
     */
        $router->add_route('root', array('/', '\Launchsite\controllers\site_controller', 'index'));
        $router->add_route('about', array('/about', '\Launchsite\controllers\site_controller', 'about'));
        $router->add_route('privacy policy', array('/privacy_policy', '\Launchsite\controllers\site_controller', 'privacy_policy'));
        $router->add_route('cookie policy', array('/cookie_policy', '\Launchsite\controllers\site_controller', 'cookie_policy'));

	/**
	 * API routes
	 */
		$router->add_route('get api token', array('/get_token', '\Launchsite\controllers\api_controller', 'get_token'));

	/**
	 * Asset routes
	 */
		$router->add_route('download asset', array('/asset/download/:type/:id', '\Launchsite\controllers\asset_controller', 'download_asset'));
		$router->add_route('show image', array('/asset/show/:type/:id', '\Launchsite\controllers\asset_controller', 'show_image'));
           
	/**
	 * Authentication routes
	 */
		$router->add_route('add auth', array('/add_auth', '\Launchsite\controllers\authentication_controller', 'add_auth'), 'cli');
		$router->add_route('add auth group', array('/add_auth_group', '\Launchsite\controllers\authentication_controller', 'add_auth_group'), 'cli');
		$router->add_route('add auth to group', array('/add_auth_to_group', '\Launchsite\controllers\authentication_controller', 'add_auth_to_group'), 'cli');

	/**
	 * DB access routes
	 */
		$router->add_route('create db', array('/create_db', '\Launchsite\controllers\db_controller', 'create_db'), 'cli');
		$router->add_route('drop db', array('/drop_db', '\Launchsite\controllers\db_controller', 'drop_db'), 'cli');
		$router->add_route('copy db', array('/copy_db', '\Launchsite\controllers\db_controller', 'copy_db'), 'cli');
		$router->add_route('show dbs', array('/show_dbs', '\Launchsite\controllers\db_controller', 'show_dbs'), 'cli');
		$router->add_route('show tables', array('/show_tables', '\Launchsite\controllers\db_controller', 'show_tables'), 'cli');
		$router->add_route('run query', array('/run_query', '\Launchsite\controllers\db_controller', 'run_query'), 'cli');

    /**
     * Login routes
     */
        $router->add_route('login', array('/login', '\Launchsite\controllers\login_controller', 'login'));
        $router->add_route('ajax login', array('/ajax_login', '\Launchsite\controllers\login_controller', 'ajax_login'), 'post');
        $router->add_route('logout', array('/logout', '\Launchsite\controllers\login_controller', 'logout'));
        $router->add_route('register', array('/register', '\Launchsite\controllers\login_controller', 'register'), 'post');
        $router->add_route('reset', array('/reset', '\Launchsite\controllers\login_controller', 'password_reset'), 'post');
        $router->add_route('recover', array('/recover', '\Launchsite\controllers\login_controller', 'recover'), 'post');
        $router->add_route('update password', array('/update_passwords', '\Launchsite\controllers\login_controller', 'update_password'), 'post');
 
	/**
	 * Migration routes
	 */
		$router->add_route('migration version', array('/get_migration_version', '\Launchsite\controllers\migration_controller', 'get_migration_version'), 'cli');
		$router->add_route('set migration version', array('/set_migration_version', '\Launchsite\controllers\migration_controller', 'set_migration_version'), 'cli');
		$router->add_route('make migration', array('/create_migration', '\Launchsite\controllers\migration_controller', 'create_migration'), 'cli');
		$router->add_route('migrate to', array('/migrate_to', '\Launchsite\controllers\migration_controller', 'migrate_to'), 'cli');

	/**
	 * Setup routes
	 */
		$router->add_route('setup site', array('/create_site', '\Launchsite\controllers\system_controller', 'create_site'), 'cli');

    /**
     * System routes
     */
        $router->add_route('manage tabs', array('/managed_tabs/:prefix/:tab', '\Launchsite\controllers\system_controller', 'manage_tabs'), 'post');

	/**
	 * User routes
	 */
		$router->add_route('my profile', array('/my_profile', '\Launchsite\controllers\user_controller', 'my_profile'));
		$router->add_route('update user field', array('/user/update_field', '\Launchsite\controllers\user_controller', 'update_field'), 'post');
        $router->add_route('add user', array('/cli/add_user', '\Launchsite\controllers\user_controller', 'add_user'), 'cli');
