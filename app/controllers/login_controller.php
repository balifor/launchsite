<?php
/**
 * User login management.
 *
 * A controller for login routes.
 */

namespace Launchsite\controllers;

/**
 * User login management.
 *
 * Extendable controller for a basic login system.
 */
class login_controller extends \Launchsite\abstracts\controller 
{
	/**
	 * Method response types.
	 *
	 * @var array
	 */
	public $response_types = array(
		'login' => 'html',
		'ajax_login' => 'json',
		'ajax_logout' => 'json',
		'register' => 'json',
		'password_reset' => 'json',
		'recover' => 'json',
		'update_password' => 'json',
	);

	/**
	 * Load the login page.
	 *
	 * @return void.
	 */
	function login() { }

	/**
	 * Basic Ajax Login.
	 *
	 * @return void Echo a JSON encoded array of data to be displayed.
	 */
	function ajax_login()
	{
		if (strlen($_POST['email']) && strlen($_POST['pass'])) {
			$user_class = $this->get_engine('user')->get_user_class();
			$user = new $user_class();
	
			$result = $user->check_login($_POST['email'], $_POST['pass']);
			if ($result['success']) {
				$conf_timeout = $this->launchsite()->get_config('SESSION_TIMEOUT');
				$_SESSION['EXPIRES'] = time() + $conf_timeout;
				return array('GOTO' => $this->get_engine('routing')->url('root'), 'SESSION' => $_SESSION['EXPIRES'] - time(), 'ID' => session_id());
			} else {
				$vars['login_error'] = $result['message'];
				$message = $this->get_engine('templating')->get_contents('login/login_form', $vars);

				return array('login_block' => $message);
			}
		} else {
			$vars['login_error'] = 'Please enter your login details';
			$message = $this->get_engine('templating')->get_contents('login/login_form', $vars);

			return array('login_block' => $message);
		}
	}

	/**
	 * Basic Logout.
	 *
	 * @return void Echo's a JSON encoded array. 
	 */
	function logout()
	{
		$this->render = false;

		session_destroy();

		header("Location: /login");
		exit;
	}

	/**
	 * Basic Registration.
	 *
	 * @return void Echo a JSON encoded array.
	 */
	function register()
	{
		if (!$this->launchsite()->get_config('ALLOW_REGISTRATION')) {
			$vars['registration_error'] = 'Registration is currently disabled.';
			$message = $this->get_engine('templating')->get_contents('login/register_form', $vars);

			return array('register_block' => $message);			
		}

		$errors = array();
		$missing = array();

		$user_class = $this->get_engine('user')->get_user_class();
		$user = new $user_class();

		$vars['registration_data'] = $_POST;

		if (!strlen($_POST['email'])) {
			$missing[] = 'Email';
		}

		if (!strlen($_POST['username'])) {
			$missing[] = 'Username';
		}

		if (!strlen($_POST['pass'])) {
			$missing[] = 'Password';
		}

		if (!strlen($_POST['confirm_pass'])) {
			$missing[] = 'Confirm Password';
		}

		if (count($missing)) {
			$vars['registration_error'] = 'Please fill in all fields, currently missing: ' . implode(', ', $missing);
			$message = $this->get_engine('templating')->get_contents('login/register_form', $vars);

			return array('register_block' => $message);
		}

		if ($this->get_engine('validation')->validate_email($_POST['email'])) {
			if ($user->unique('email', $_POST['email'])) {
				$user->set('email', $_POST['email']);
			} else {
				$errors[] = "That email already has an account.";
			}
		} else {
			$errors[] = 'Please use a valid email address';
		}

		if ($user->unique('username', $_POST['username'])) {
			$user->set('username', $_POST['username']);
		} else {
			$errors[] = 'Sorry, that username is already in use.';
		}

		if ($_POST['pass'] != $_POST['confirm_pass']) {
			$errors[] = 'Your Passwords do not match!';
		} else {
			$user->set('password', password_hash($_POST['pass'], PASSWORD_DEFAULT));
		}

		if (count($errors)) {
			$vars['registration_error'] = "We're sorry, there's an issue with your registration: " . implode(', ', $errors);
			$message = $this->get_engine('templating')->get_contents('login/register_form', $vars);

			return array('register_block' => $message);
		} 

		try {	
			$user->save();

			$this->get_engine('user')->set_current_user($user);
			$conf_timeout = $this->launchsite()->get_config('SESSION_TIMEOUT');
			$_SESSION['EXPIRES'] = time() + $conf_timeout;

			$message = $this->get_engine('templating')->get_contents('login/register_complete', $vars);

			return array('register_block' => $message);
		} catch (Exception $e) {
			$vars['registration_error'] = $e->getMessage();

			$message = $this->get_engine('templating')->get_contents('login/register_form', $vars);
			return array('register_block' => $message);
		}           
	}

	/**
	 * Basic password Reset.
	 *
	 * @return void Echo's out a JSON encoded array.
	 */
	function password_reset()
	{
		$vars['reset_data'] = $_POST;

		if (!strlen($_POST['email'])) {
			$vars['reset_error'] = 'Let us know what your email address is below:';
			$message = $this->get_engine('templating')->get_contents('login/reset_token_form', $vars);
			return array('reset_block' => $message);
		} 
		
		if (!$this->get_engine('validation')->validate_email($_POST['email'])) {
			$vars['reset_error'] = "The email address you've given doesn't seem to work";
			$message = $this->get_engine('templating')->get_contents('login/reset_token_form', $vars);
			return array('reset_block' => $message);
		} 
		
		$user_class = $this->get_engine('user')->get_user_class();
		$users = new $user_class();

		try {
			$user = $users->find(array('email' => $_POST['email'], 'deleted' => false), $user_class);
		} catch (\Exception $e) {
			$user = array();
		}

		if (empty($user)) {
			$vars['reset_error'] = "We don't have that email on record";
			$message = $this->get_engine('templating')->get_contents('login/reset_token_form', $vars);

			return array('reset_block' => $message);
		} 

		if (count($user) > 1) {
			$log = "Problem with the users table, there's multiple users with the same email address" . PHP_EOL;
			$log .= print_r($user, 1) . PHP_EOL;
			$this->get_engine('logging')->log($log, 'users.txt');

			$vars['reset_error'] = "There's a problem with your account, we'll get back to you as soon as possible";
			$message = $this->get_engine('templating')->get_contents('login/password_reset_form', $vars);

			return array('#reset_block' => $message);
		}
		
		if (count($user) === 1) {
			$token = md5(rand());
			$user[0]->set('token', $token);
			$user[0]->set('token_time', time());

			try {
				$user[0]->save();
			} catch(Exception $e) {
				$log = "Couldn't set a token for user:" . $user[0]->get('id') . PHP_EOL;
				$log .= print_r($e, 1) . PHP_EOL;
				$this->get_engine('logging')->log($log, 'users.txt');

				$vars['reset_error'] = "There's a problem with your account, we'll get back to you as soon as possible";
				$message = $this->get_engine('templating')->get_contents('login/password_reset_form', $vars);

				return array('reset_block' => $message);
			}

			$vars['token'] = $token;

			$result = $this->get_engine('email')->email($user[0]->get('email'), 'Password Reset', $this->get_engine('templating')->get_contents('emails/password_reset.php', $vars), $this->launchsite()->get_config('SYS_ADMIN_EMAIL'), $this->launchsite()->get_config('SITE_NAME'));

			if (!$result) {
				$log = "Couldn't send a password reset email to user:" . $user[0]->get('id') . PHP_EOL;
				$this->get_engine('logging')->log($log, 'email.txt');

				$vars['reset_error'] = "There's been a problem mailing you, we'll get back to you as soon as possible";
				$message = $this->get_engine('templating')->get_contents('login/reset_token_form', $vars);

				return array('reset_block' => $message);
			} else {
				$vars['reset_email'] = $_POST['email'];
				$message = $this->get_engine('templating')->get_contents('login/reset_complete', $vars);

				return array('reset_block' => $message);
			}
		} 
	}

	/**
	 * Reset Password.
	 *
	 * @return void Echo's a JSON encoded array.
	 */
	function recover()
	{
		$vars['recovery_data'] = $_POST;
		$vars['update_password_data'] = $_POST;

		$missing = array();
		if (!isset($_POST['email']) || !strlen($_POST['email'])) {
			$missing[] = 'Email';
		}

		if (!isset($_POST['token']) || !strlen($_POST['token'])) {
			$missing[] = 'Token';            
		}

		if (count($missing)) {
			$vars['recovery_error'] = "Please fill in all fields, you're currently missing: " . implode(', ', $missing);
			$message = $this->get_engine('templating')->get_contents('login/recovery_form', $vars);

			return array('recover_block' => $message);
		}

		$user_class = $this->get_engine('user')->get_user_class();
		$users = new $user_class();

		try {
			$user = $users->find(array('email' => $_POST['email'], 'deleted' => false), $user_class);
		} catch (\Exception $e) {
			$vars['recovery_error'] = "There was a problem looking up the email: " . $e->getMessage();
			$message = $this->get_engine('templating')->get_contents('login/recovery_form', $vars);
			
			return array('recover_block' => $message);
		}

		if (empty($user)) {
			$vars['recovery_error'] = "We don't have that email on record";
			$message = $this->get_engine('templating')->get_contents('login/recovery_form', $vars);

			return array('recover_block' => $message);
		} elseif (count($user) === 1) {
			if(!strlen($user[0]->get('token')) || $_POST['token'] != $user[0]->get('token') || strtotime('-' . $this->launchsite()->get_config('PASSWORD_RESET_LIMIT')) > $user[0]->get('token_time'))
			{
				$vars['recovery_error'] = "That token is invalid";
				$message = $this->get_engine('templating')->get_contents('login/recovery_form', $vars);

				return array('recover_block' => $message);
			} else {
				$message = $this->get_engine('templating')->get_contents('login/update_password', $vars);

				return array('recover_block' => $message);
			}
		} elseif (count($user) > 1) {
			$log = "Problem with the users table, there's multiple users with the same email address" . PHP_EOL;
			$log .= print_r($user, 1) . PHP_EOL;
			$this->get_engine('logging')->log($log, 'users.txt');

			$vars['recovery_error'] = "There's a problem with your account, we'll get back to you as soon as possible";
			$message = $this->get_engine('templating')->get_contents('login/recovery_form', $vars);

			return array('recover_block' => $message);
		}
	}

	/**
	 * Update password.
	 *
	 * @return void Echo's a JSON encoded array.
	 */
	function update_password()
	{
		$vars['update_password_data'] = $_POST;

		$missing = array();
		if (!strlen($_POST['pass'])) {
			$missing[] = 'Password';
		}

		if (!strlen($_POST['confirm_pass'])) {
			$missing[] = 'Confirm Password';
		}

		if (count($missing)) {
			$vars['update_password_error'] = 'Please fill in all fields, currently missing: ' . implode(', ', $missing);
			$message = $this->get_engine('templating')->get_contents('login/update_password', $vars);

			return array('recover_block' => $message);
		}

		if ($_POST['pass'] != $_POST['confirm_pass']) {
			$vars['update_password_error'] = 'Your Passwords do not match!';
			$message = $this->get_engine('templating')->get_contents('login/update_password', $vars);

			return array('recover_block' => $message);
		}

		$user_class = $this->get_engine('user')->get_user_class();
		$users = new $user_class();

		try {
			$user = $users->find(array('email' => $_POST['email'], 'deleted' => false), $user_class);
		} catch (\Exception $e) {
			$vars['update_password_error'] = 'There was a problem looking up the user: ' . $e->getMessage();
			$message = $this->get_engine('templating')->get_contents('login/update_password', $vars);

			return array('recover_block' => $message);
		}

		if (empty($user)) {
			$vars['update_password_error'] = "We don't have that email on record";
			$message = $this->get_engine('templating')->get_contents('login/update_password', $vars);

			return array('recover_block' => $message);
		} elseif (count($user) === 1) {
			if(!strlen($user[0]->get('token')) || $_POST['token'] != $user[0]->get('token') || strtotime('-' . $this->launchsite()->get_config('PASSWORD_RESET_LIMIT')) > $user[0]->get('token_time'))
			{
				$vars['update_password_error'] = "That token is invalid";
				$message = $this->get_engine('templating')->get_contents('login/update_password', $vars);

				return array('recover_block' => $message);
			} else {
				$user[0]->set('password', password_hash($_POST['pass'], PASSWORD_DEFAULT));

				try {
					$user[0]->save();
					$this->get_engine('user')->set_current_user($user[0]);
					$conf_timeout = $this->launchsite()->get_config('SESSION_TIMEOUT');
					$_SESSION['EXPIRES'] = time() + $conf_timeout;
				} catch (Exception $e) {
					$this->get_engine('logging')->log("Problem updating a users password" . print_r($_POST, 1) . PHP_EOL . print_r($e, 1) . PHP_EOL . print_r($user, 1), 'users.txt');

					$vars['update_password_error'] = "We had a problem updating your password, we'll get back to you as soon as possible";
					$message = $this->get_engine('templating')->get_contents('login/update_password', $vars);

					return array('update_password_block' => $message);
				}

				$message = $this->get_engine('templating')->get_contents('login/password_updated', $vars);

				return array('recover_block' => $message);
			}
		} elseif (count($user) > 1) {
			$log = "Problem with the users table, there's multiple users with the same email address" . PHP_EOL;
			$log .= print_r($user, 1) . PHP_EOL;
			$this->get_engine('logging')->log($log, 'users.txt');

			$vars['update_password_error'] = "There's a problem with your account, we'll get back to you as soon as possible";
			$message = $this->get_engine('templating')->get_contents('login/update_password', $vars);

			return array('recover_block' => $message);
		}
	}

	public function get_token()
	{
		$api = $this->get_engine('api');
		 
	}
}
