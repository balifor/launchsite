<?php
/**
 * The controller for user routes.
 *
 * Handles the user sections.
 */

namespace Launchsite\controllers;

/**
 * User controller.
 */
class user_controller extends \Launchsite\abstracts\controller
{
	public $response_types = array(
		'update_field' => 'json',
		'add_user' => 'cli',
	);

	public function my_profile() {
		$user = $this->get_engine('user')->get_current_user();
		
		return array(
			'user' => $user		
		);
	}

	public function update_field() {

		$field = isset($_POST['field']) ? $_POST['field'] : false;
		$value = isset($_POST['value']) ? $_POST['value'] : false;

		$response = array('success' => false, 'message' => '');
		if (!$field || !$value) {
			$response['message'] = "Requires both field and value.";
			
			return $response;
		}

		$user = $this->get_engine('user')->get_current_user();
		if (!$user) {
			$response['message'] = "Not Logged in";

			return $response;
		}

		try {
			//throw Exception('testing');
			$user->set($field, $value);
			$user->save();

			$response['success'] = true;
			$response['field'] = $field;
			$response['value'] = $value;

			return $response;

		} catch (Exception $e) {
			$this->get_engine('logging')->log("Error updating user field: " . $e->getMessage(), 'db_errors.txt');
			$response['message'] = "Problem updating field.";
			$response['field'] = $field;
			$response['value'] = $user->get($field);
			return $response;
		}
	}

    /**
     * Add a user via the command line.
     *
     * @var string $argv[2] The username.
     *
     * @var string $argv[3] The email.
     *
     * @var string $argv[4] The password.
     */
    public function add_user()
    {
        global $argv;

        //Get the username.
        if (!isset($argv[2])) {
            $this->get_engine('response')->add_error("Please add a username as argv 2", 500);
            return;
        } else {
            $username = $argv[2];
        }

        //Get the email.
        if (!isset($argv[3])) {
            $this->get_engine('response')->add_error("Please add an email address as argv 3", 500);
            return;
        } else {
            $email = $argv[3];
        }
        //Get the password.
        if (!isset($argv[4])) {
            $this->get_engine('response')->add_error("Please add a password as argv 4", 500);
            return;
        } else {
            $password = $argv[4];
        }

        $user_class = $this->get_engine('user')->get_user_class();
        $user = new $user_class();

        $user->set('username', $username);
        $user->set('email', $email);
        $user->set('password', password_hash($password, PASSWORD_DEFAULT));

        if (isset($argv[5])) {
            $user->set('firstname', $argv[5]);
        }

        if (isset($argv[6])) {
            $user->set('lastname', $argv[6]);
        }

        try {
            $user->save();
            $this->get_engine('response')->add_message("User saved");
        } catch (\PDOException $e) {
            $this->get_engine('response')->add_error("Error saving the user: " . $e->getMessage(), 500);
            return;
        }
    }
}
