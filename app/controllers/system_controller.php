<?php
/**
 * System route controller.
 *
 * A controller for routes that come with launchsite.
 */

namespace Launchsite\controllers;

/**
 * System Management.
 *
 * Extendable controller for system routes.
 */
class system_controller extends \Launchsite\abstracts\controller 
{
	/**
	 * Method response types.
	 *
	 * @var array
	 */
	public $response_types = array(
		'manage_tabs' => 'json',
		'create_site' => 'cli',
	);

	/**
	 * Manage tabs.
	 *
	 * @param $vars An array of prefix => value, tab => value. Sets the current tab for $this->get_engine('templating')->managed_tabs().
	 *
	 * @return void. 
	 */
	public function manage_tabs($prefix, $tab)
	{
		$_SESSION['TABS'][$prefix] = $tab;
	}

	/**
	 * Copy the site skeleton to DOCROOT
	 */
	public function create_site()
	{
		if ($this->get_engine('filesystem')->recursive_copy(FRAMEWORK . 'skel' . DIRECTORY_SEPARATOR, SITE)) {
			$this->get_engine('response')->add_message('Successfully created skeleton site at: ' . SITE);
		} else {
			$this->get_engine('response')->add_error("Failed to create site at: " . SITE, 500, 'no_directory.php');
		}
	}
}
