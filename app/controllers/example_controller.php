<?php
/**
 * The example controller.
 *
 * A controller for functions for example.
 */

namespace Launchsite\controllers;

/**
 * Example controller.
 */
class example_controller extends \Launchsite\abstracts\controller 
{
	/**
	 * Method response types.
	 *
	 * @var array
	 */
	public $response_types = array(

	);
}
