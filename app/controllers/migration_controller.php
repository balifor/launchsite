<?php
/**
 * A controller for migrations.
 *
 * Functions used for migrations.
 */

namespace Launchsite\controllers;

use \Launchsite\models\migration;

/**
 * Migration controller.
 *
 * Migrations.
 */
class migration_controller extends \Launchsite\abstracts\controller 
{
	/**
	 * Method response types.
	 *
	 * @var array
	 */
	public $response_types = array(
		'create_migration' => 'cli',
		'migrate_to' => 'cli',
		'get_migration_version' => 'cli',
		'set_migration_version' => 'cli',
	);

	/**
	 * Create a Migration
	 */
	public function create_migration()
	{
		//Get the current max available migration. 
		$migration = new migration();
		$version = $migration->get_max_available();
		$version++;

		$contents = <<<PHP
<?php
/**
 * Migration version: {$version}
 */
class migration_{$version} extends \Launchsite\models\migration
{
	/**
	 * Run the update migration and increment db version.
	 */
	public function update()
	{
		\$sql = <<<SQL

SQL;

		try {
			return \$this->get_db()->query(\$sql);
		} catch (\Exception \$e) {
			return \$e->getMessage();
		}
	}

	/**
	 * Run the migration rollback and decrement db version.
	 */
	public function rollback()
	{
		\$sql = <<<SQL

SQL;

		try {
			return \$this->get_db()->query(\$sql);
		} catch (\Exception \$e) {
			return \$e->getMessage();
		}
	}
}

PHP;
		
		if (!file_put_contents($this->launchsite()->get_config('MIGRATIONS') . 'migration_' . $version . '.php', $contents)) {
			$this->get_engine('response')->add_error("Couldn't create migration version: $version");
		} else {
			$this->get_engine('response')->add_message("Created migration version: $version");
		}
	}

	/**
	 * Method to drop db.
	 *
	 * @param string $argv[2] The version to migrate to.
	 *
	 * @param string $argv[3] The name of the database.
	 */
	public function migrate_to()
	{
		global $argv;

		//Get the version to go to
		if (isset($argv[2])) {
			if (!is_numeric($argv[2]) && $argv[2] != 'max') {
				$this->get_engine('response')->add_error("Please supply a version number to migrate to, or use max.", 500);
				return;
			} else {
				$version = $argv[2];
			}
		} else {
			$version = 'max';
		}

		//Check if its not the main db
		$db_to_use = false;
		if (isset($argv[3])) {
			$db_to_use = $argv[3];
		}
		
		//Check the max available version
		$migration = new migration();

		$available_version = $migration->get_max_available();

		$starred = false;
		if($version == 'max') { 
			$starred = true;
			$version = $available_version;
		}

		//Check the requested version exists.
		if ($available_version >= $version) {
			//Get the current version
			$current_version = $migration->get_current_version();

			//Check if the version requested is before or after the current version.
			if ($version > $current_version) {
				//Run upgrade
				$migrations = $migration->get_migrations($current_version, $version);
				
				$db = $this->get_engine('database')->get_db($db_to_use);
				foreach (range($current_version, $version) as $run_version) {

					if ($run_version == 0 || $run_version == $current_version) {
						continue;
					}

					$db->begin();
					$result = $migrations[$run_version]->update();

					if (is_string($result)) {
						
						$db->rollback();
						$this->get_engine('response')->add_error("There was a problem running migration: $run_version - " . $result, 500, 'db_error.php');
						return;	

					} else {

						do {
							if ($result->errorCode() != 0) {
								
								$db->rollback();
								$this->get_engine('response')->add_error("There was a problem running migration: $run_version - " . $result, 500, 'db_error.php');
								return;
							}
						} while ($result->nextRowset());

						$this->get_engine('response')->add_message("Successfully migrated to version: $run_version");
						$migration->set_current_version($run_version);
						$db->commit();
					}
				}

				return;
			} elseif ($version == $current_version) {
				//Let the user know the database is already at this version.
				$this->get_engine('response')->add_message("Database is already at version: $version");
				return;
			} else {
				if ($starred) {
					//Let the user know this would run a downgrade to $available_version
					$this->get_engine('response')->add_error("This would run a downgrade to version: $available_version", 302);
					return;
				} else {
					//Run rollback migrations to version.
					$migrations = $migration->get_migrations($version + 1, $current_version);

					$db = $this->get_engine('database')->get_db();
					foreach (range($current_version, $version + 1) as $run_version) {
						$to_version = $run_version - 1;
						if ($run_version == 0) {
							$migration->set_current_version(0);
							continue;
						}

						$db->begin();
						$result = $migrations[$run_version]->rollback();

						if (is_string($result)) {
							$db->rollback();
							$this->get_engine('response')->add_error("There was a problem running migration: $run_version", 500, 'db_error.php');
							return;
						} else {

							do {
								if ($result->errorCode() != 0) {

									$db->rollback();
									$this->get_engine('response')->add_error("There was a problem running migration: $run_version - " . $result, 500, 'db_error.php');
									return;
								}
							} while ($result->nextRowset());

							$migration->set_current_version($to_version);
							$db->commit();
							$this->get_engine('response')->add_message("Successfully rolled back version: $to_version");
						}
					}

					return;
				}
			}

		} else {
			$this->get_engine('response')->add_error("That migration is not available. Max available: $available_version", 404);
			return;
		}
	}

	/**
	 * Get the current migration version.
	 */
	public function get_migration_version()
	{
		global $argv;

		//Check if its not the main db
		$db_to_use = false;
		if (isset($argv[2])) {
			$db_to_use = $argv[2];
		}

		$migration = new migration();
		$version = $migration->get_current_version($db_to_use);

		$this->get_engine('response')->add_message("The database is currently at version: $version");
	}

	/**
	 * Set the migration version.
	 *
	 * @param string $version The version to set as.
	 */
	public function set_migration_version()
	{
		global $argv;

		//Get the version to go to
		if (!isset($argv[2]) || !is_numeric($argv[2])) {
			$this->get_engine('response')->add_error("Please supply a version number to set migration count to.", 500);
			return;
		} else {
			$version = $argv[2];
		}

		//Check if its not the main db
		$db_to_use = false;
		if (isset($argv[3])) {
			$db_to_use = $argv[3];
		}

		$migration = new migration();

		if ($migration->set_current_version($version, $db_to_use)) {
			$this->get_engine('response')->add_message("Successfully set version to: $version");
			return;
		} else {
			$this->get_engine('response')->add_error("Could not set version to: $version", 404);
			return;
		}
	}
}
