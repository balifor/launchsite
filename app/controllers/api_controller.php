<?php
/**
 * The API controller.
 *
 * A controller for functions for controlling the API.
 */

namespace Launchsite\controllers;

/**
 * Example controller.
 */
class api_controller extends \Launchsite\abstracts\controller 
{
	/**
	 * Method response types.
	 *
	 * @var array
	 */
	public $response_types = array(
		'index' => 'html',
		'get_token' => 'json'
	);

	public function index() {}

	public function get_token()
	{
		$username = isset($_POST['username']) ? $_POST['username'] : false;
		$secret = isset($_POST['client_secret']) ? $_POST['client_secret'] : false;

		$api = $this->get_engine('api');
		$response = $this->get_engine('response');

		if (!$username || !$secret) {
			return $api->generate_error(401, 'Please supply full login details');
		}



		$token = $api->generate_token();
	}
}
