<?php
/**
 * A controller for DB interactions.
 */

namespace Launchsite\controllers;

use \Launchsite\models\migration;

/**
 * DB controller.
 */
class db_controller extends \Launchsite\abstracts\controller 
{
	/**
	 * Method response types.
	 *
	 * @var array
	 */
	public $response_types = array(
		'create_db' => 'cli',
		'show_dbs' => 'cli',
		'show_tables' => 'cli',
		'drop_db' => 'cli',
		'run_query' => 'cli',
	);

	/**
	 * Method to create db.
	 *
	 * @param string $argv[2] The name of the database.
	 *
	 * @param string $argv[3] The name of the database config to use.
	 */
	public function create_db()
	{
		global $argv;

		if (!isset($argv[2])) {
			$this->get_engine('response')->add_error("Please add a name for the database.", 500);
			return;
		} else {
			$name = $argv[2];
		}

		if (isset($argv[3])) {
			$config_name = $argv[3];
		}

		$db = $this->get_engine('database')->get_db($config_name, false);

		$db_query = $db->db_skeleton_query($name);
		$migrations_query = $db->migration_skeleton_query();

		try {
			$db->query($db_query);
			$db->query($migrations_query);
			$this->get_engine('response')->add_message("Successfully created DB: $name");
		} catch (\PDOException $e) {
			$this->get_engine('response')->add_error("There was an error running the query: " . $e->getMessage(), 500);
		}
	}

	/**
	 * Show a list of DBs
	 *
	 * @param string $argv[2] The name of the database to use.
	 */
	public function show_dbs()
	{
		if (isset($argv[2])) {
			$use_db = $argv[2];
		}

		$db = $this->get_engine('database')->get_db($use_db, false);

		try {
			$databases = $db->show_databases();
			$this->get_engine('response')->add_message("DBS: " . print_r($databases,1));
		} catch (Exception $e) {
			$this->get_engine('response')->add_error("There was an error running the query: " . $e->getMessage(), 500);
		}
	}

	/**
	 * Show a list of tables
	 *
	 * @param string $argv[2] The name of the database to use.
	 */
	public function show_tables()
	{
		if (isset($argv[2])) {
			$use_db = $argv[2];
		}

		$db = $this->get_engine('database')->get_db($use_db);

		try {
			$tables = $db->show_tables();
			$this->get_engine('response')->add_message("Tables: " . print_r($tables,1));
		} catch (Exception $e) {
			$this->get_engine('response')->add_error("There was an error running the query: " . $e->getMessage(), 500);
		}
	}

	/**
	 * Method to drop db.
	 *
	 * @param string $argv[2] The name of the database.
	 *
	 * @param string $argv[3] The name of the host to use.
	 */
	public function drop_db()
	{
		global $argv;

		//Check the db name is set
		if (!isset($argv[2])) {
			$this->get_engine('response')->add_error("Please add a name for the database.", 500, 'db_error.php');
			return;
		} else {
			$name = $argv[2];
		}

		//Check if its not the main db
		$db_to_use = isset($argv[3]) ? $argv[3] : false;

		$line = readline("Are you sure you want to do this?  Type 'yes' to continue: ");
		if(trim(strtolower($line)) != 'yes'){
			$this->get_engine('response')->add_message("Cancelling.");
			return;
		}

		echo "\nIf you're sure...\n";

		try {
			$this->get_engine('database')->get_db($db_to_use)->drop_db($name);
			$this->get_engine('response')->add_message("Successfully dropped DB: $name");
		} catch (\PDOException $e) {
			$this->get_engine('response')->add_error("There was an error running the query: " . $e->getMessage(), 500, 'db_error.php');
		}
	}

	/**
	 * Function to run a query on the database
	 *
	 * @param string $argv[2] The SQL query to run I.E: "SELECT * FROM users;"
	 *
	 * @param string $argv[3] The database to use if not default
	 */
	public function run_query()
	{
		global $argv;

		//Check the query is set
		if (!isset($argv[2])) {
			$this->get_engine('response')->add_error("Please add a query to run.", 500, 'db_error.php');
			return;
		} else {
			$query = $argv[2];
		}

		//Check if its not the main db
		$db_to_use = isset($argv[3]) ? $argv[3] : false;

		try {
			$result = $this->get_engine('database')->get_db($db_to_use)->query($query);
			$this->get_engine('response')->add_message("Query: $query");
			$this->get_engine('response')->add_message("Result: " . print_r($result, 1));
			$this->get_engine('response')->add_message("Result: " . print_r($result->fetchAll(), 1));
		} catch (\PDOException $e) {
			$this->get_engine('response')->add_error("There was an error running the query: " . $e->getMessage(), 500, 'db_error.php');
		}
	}
}
