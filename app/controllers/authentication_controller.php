<?php
/**
 * A controller for managing authentications.
 *
 * Functions used for managing authentication.
 */

namespace Launchsite\controllers;

use \Launchsite\models\auth;
use \Launchsite\models\auth_group;

/**
 * Authentication controller.
 *
 * Auth Management.
 */
class authentication_controller extends \Launchsite\abstracts\controller 
{
	/**
	 * Method response types.
	 *
	 * @var array
	 */
	public $response_types = array(
		'add_auth' => 'cli',
		'add_auth_group' => 'cli',
	);	

	/**
	 * Add an authorisation.
	 *
	 * @var string $argv[2] The name of the auth to add.
	 */
	public function add_auth()
	{
		global $argv;

		if (!isset($argv[2])) {
			$this->get_engine('response')->add_error("Please add a name for the auth.", 500, 'db_error.php');
			return;
		} else {
			$name = $argv[2];
		}

		$auth = new auth();

		$auth->set('name', $name);

		try {
			$auth->save();
		} catch (\PDOException $e) {
			$this->get_engine('response')->add_error("Couldn't save the new auth: $name" . $e->getMessage(), 500, 'db_error.php');
		}
	}

	/**
	 * Add an authorisation group.
	 *
	 * @var string $argv[2] The name of the auth group to add.
	 */
	public function add_auth_group()
	{
		global $argv;

		if (!isset($argv[2])) {
			$this->get_engine('response')->add_error("Please add a name for the auth group.", 500, 'db_error.php');
			return;
		} else {
			$name = $argv[2];
		}

		$auth_group = new auth_group();

		$auth_group->set('name', $name);

		try {
			$auth_group->save();
		} catch (\PDOException $e) {
			$this->get_engine("Couldn't save the new auth_group: $name" . $e->getMessage(), 'db_error.php', 500);
		}
	}
}
