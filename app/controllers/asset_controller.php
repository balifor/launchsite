<?php
/**
 * The asset controller.
 *
 * A controller for functions for assets.
 */

namespace Launchsite\controllers;

/**
 * Asset controller.
 */
class asset_controller extends \Launchsite\abstracts\controller 
{
	/**
	 * Method response types.
	 *
	 * @var array
	 */
	public $response_types = array(
		'download_asset' => 'raw',
		'show_image' => 'raw',
	);

	/**
	 * Load an asset from outside docRoot
	 */
	public function download_asset($type, $id)
	{
		$asset = $this->get_engine('asset');

		$info = $asset->get_asset_info($type, $id);

		if (!$info) {
			$this->get_engine('response')->add_error('No file info found.', 404);
		}

		$asset->output_file($info['full_path'], $info['name'] . '.' . $info['extension']);
	}

	/**
	 * Load an asset from outside docRoot
	 */
	public function show_image($type, $id)
	{
		$asset = $this->get_engine('asset');

		$info = $asset->get_asset_info($type, $id);

		if (count($info) < 2) {
			$this->get_engine('response')->add_error('No file info found.', 404);
		} else {
			$asset->show_image($info['full_path'], $info['name'] . '.' . $info['extension']);
		}
	}
}
