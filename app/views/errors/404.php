<div class="section">
    <h2>404 - Not Found</h2>
    <hr>
    <div class='centered'>
        <p>The URL: "<?=isset($vars['path']) ? $vars['path'] : '';?>" was not found on this server.</p>
        <br />
        <p>Try either:</p>
        <button onclick="window.history.go(-1)">Back</button>
        <?=self::link($this->get_engine('routing')->url('root'), '<button>Home Page</button>')?>
    </div>
</div>
