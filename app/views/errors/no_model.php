<div class="page_title">
    <h1>No Model</h1>    
</div>
<div class="mobile-col-12 tablet-col-12 large-col-12">
    <div class='section'>
        <h2>There's no model of that name.</h2>
        <?php foreach($vars as $key => $var) { ?>
        <div class="mobile-col-12 tablet-col-3 large-col-2">
            <?=$key?>
        </div>
        <div class='mobile-col-12 tablet-col-9 large-col-10'>
            <pre><?=var_dump($var);?></pre>
        </div>
        <?php } ?>
    </div>
</div>
