<div class="page_title">
    <h1>Login</h1>
</div>
<div class="mobile-col-12 tablet-col-3  large-col-3">
    <div class="sidenav">
		<?php 
		$allow_registration = $this->launchsite()->get_config('ALLOW_REGISTRATION');
		if ($allow_registration) {
        	echo $this->manage_tabs('login_page', array('Login' => 'login_panel', 'Register' => 'register_panel', 'Recover' => 'recover_panel'), 'login_panel', true);
		} else {
        	echo $this->manage_tabs('login_page', array('Login' => 'login_panel', 'Recover' => 'recover_panel'), 'login_panel', true);
		}
		 ?>        
    </div>
</div>
<div class="mobile-col-12 tablet-col-9 large-col-9">
    <div class="mobile-col-12 tablet-col-12 large-col-12">
        <div class="section" id="login_panel">
            <h2>Login</h2>
            <div id="login_block">
                <?=$this->get_contents('/login/login_form.php')?>
            </div>
        </div>
		<?php if ($allow_registration) { ?> 
        <div class="section" id="register_panel">
            <h2>Register</h2>
            <div id="register_block">
                <?=$this->get_contents('/login/register_form.php')?>
            </div>
        </div>
		<?php } ?>
        <div id="recover_panel">
            <div class="section">
                <h2>Recover Account</h2>
                <div class="block">
                    <h3>Reset Password</h3>
                    <div id="reset_block">
                        <?=$this->get_contents('/login/reset_token_form.php')?>
                    </div>
                </div>
                <div class="block">
                    <h3>Recover Account</h3>
                    <div id="recover_block">
                        <?=$this->get_contents('/login/recovery_form.php')?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
