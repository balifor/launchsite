<?php if(isset($vars['recovery_error'])) {
	echo $this->generate_alert('error', $vars['recovery_error']);
} ?>

<form id="recover_form">
	<div class="form-group">
    	<label for="email">Email</label>
    	<input class="form-control" type="text" name="email" <?= isset($vars['recovery_data']) && isset($vars['recovery_data']['email']) ? "value='" . $vars['recovery_data']['email'] . "'" : '';?>>
    </div>
	<div class="form-group">
    	<label for="token">Token</label>
    	<input class="form-control" type="text" name="token" <?= isset($vars['recovery_data']) && isset($vars['recovery_data']['token']) ? "value='" . $vars['recovery_data']['token'] . "'" : '';?>>
    </div>
	<div class="form-group">
		<?=$this->ajax_submit('recover_form', $this->get_engine('routing')->url('recover', false, 'post'), 'Recover')?>
    </div>
</form>
