<?php 
if(isset($vars['reset_error'])) {
		echo $this->generate_alert('error', $vars['reset_error']);
} ?>

<form id="reset_form">
	<div class="form-group">
		<label for="email">Email</label>
		<input class="form-control" type="text" placeholder="Email" name="email" <?= isset($vars['reset_data']) && isset($vars['reset_data']['email']) ? "value='" . $vars['reset_data']['email'] . "'" : '';?>>
	</div>
    <div class="form-group">
		<?=$this->ajax_submit('reset_form', $this->get_engine('routing')->url('reset', false, 'post'), 'Reset')?>
    </div>
</form>
