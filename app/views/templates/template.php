<!DOCTYPE HTML>
<html>
    <head>
        <title><?= isset($vars['page_title']) ? $vars['page_title'] : 'Launch Site'?></title>
        
        <!-- Meta Tags, Content type, Description and Keywords -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="<?= isset($vars['meta_author']) ? $vars['meta_author'] : 'Ben Little'?>">
        <meta name="description" content="<?= isset($vars['meta_description']) ? $vars['meta_descriptions'] : 'Develop your web applications in your browser'?>" />
        <meta name="keywords" content="<?= isset($vars['meta_keywords']) ? $vars['meta_keywords'] : 'Developer, Web Development'?>" />

        <!-- Site JS -->
        <script type="text/javascript" src="/javascript/launchsite.js"></script>
        
        <!-- Site CSS -->
        <link rel="stylesheet" type="text/css" href="/css/processed/screen.css" />

		<!-- jQuery library -->
		<script src="/lib/jquery/jquery.min.js"></script>

		<!-- Bootstrap -->
		<!-- Latest compiled JavaScript -->
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="/lib/bootstrap/css/bootstrap.min.css">

		<!-- AngularJS -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>

    </head>
    <body>
        <div class="container-fluid">
			<div class="content-wrap">
				<div id="header-box" class="row topnav text-inverse">
					<div class="col-xs-offset-2 col-xs-4"> 
						<h1 class="site-header"><?=$this->nav_link('root', $this->launchsite()->get_config('SITE_NAME'));?></h1> 
					</div>
					<div class="col-xs-6 text-center"> 
						<ul class="nav navbar-nav list-group">
							<li></li>
							<?php if ($this->get_engine('user')->logged_in()) { ?>
							<li><?=$this->nav_link('admin index', 'Admin');?></li>
							<li><?=$this->nav_link('my profile', 'My Profile');?></li>
							<li><?=$this->nav_link('logout', 'Logout');?></li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div id="content-box" class="row">
					<div class="col-xs-offset-2 col-xs-8"> 
                   		<?= $vars['CONTENTS']; ?>
					</div>
				</div>
				<?php if ($this->launchsite()->get_config('ENV') == 'dev' && (count($this->get_engine('response')->errors) || count($this->get_engine('response')->messages))) { ?>
				<div class="row">
					<div class="col-xs-12">
						<?=$this->request_errors_table();?>
						<?=$this->request_messages_table();?>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="footer-push">
				<div id="footer-box" class="row">
					<div class="col-xs-offset-2 col-xs-4 spacer"> 
					</div>
					<div class="col-xs-4 spacer">
					</div>
				</div>
			</div>
		</div>
    </body>
</html>
