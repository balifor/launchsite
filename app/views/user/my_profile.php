<div class="row">
	<div class="col-xs-12">
		<h2 class="page-header">Welcome <?=$vars['user']->get('username')?>!</h2>
		<?=$this->manage_tabs('profile_page', array('My Info' => 'info_panel'), 'info_panel')?>
	</div>
</div>

<div class="row" id="info_panel">
	<div class="col-xs-12">
		<h3 class="section-header">My Info</h3>
	</div>
	<div class="col-xs-12">
		<?=$this->get_contents('user/editable_personal_info.php', $vars);?>
	</div>
</div>
