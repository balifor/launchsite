<script type="text/javascript">
	function make_user_field_editable(field) {
		let edit = document.getElementById(field+'_edit_button');
		let update = document.getElementById(field+'_update_button');
		let field_td = document.getElementById('field_'+field);

		let value = field_td.innerHTML;

		let input = document.createElement('input');
		input.id = field+'_input'
		input.type = 'text';
		input.name = field;
		input.value = value;

		field_td.innerHTML = '';
		field_td.appendChild(input);

		edit.style.visibility = 'hidden';
		update.style.visibility = 'visible';
	}

	function update_user_field(field) {
		let value = document.getElementById(field+'_input').value;

		launchsite.post('<?=$this->get_engine('routing')->url('update user field', array(), 'post')?>', {field: field, value: value}, update_info_ui);
	}

	function update_info_ui(xhr) {
		console.log(xhr);

		let message = '';
		if (xhr.status == 200) {
			let json_res = JSON.parse(xhr.responseText);

			if (!json_res) {
				message = `
				<div class="alert alert-danger alert-dismissable" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					Error with the request.
				</div>
				`;
			}
			
			let field_td = document.getElementById('field_'+json_res.field);
			field_td.innerHTML = json_res.value;

			let edit = document.getElementById(json_res.field+'_edit_button');
			edit.style.visibility = 'visible';

			let update = document.getElementById(json_res.field+'_update_button');
			update.style.visibility = 'hidden';

			let msg_val = 'Success';
			if (json_res.message.length > 0) {
				msg_val = json_res.message;
			}

			message = `
			<div class="alert alert-success alert-dismissable" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				${msg_val}
			</div>
			`;
		} else {
			message = `
			<div class="alert alert-danger alert-dismissable" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				Request failed.
			</div>
			`;
		}

		let div = document.getElementById('user_field_update_messages');
		div.style.visibility = 'visible';
		div.innerHTML = message;
	}

</script>

<div class="col-xs-12" id="user_field_update_messages" style="visibility:hidden;">
</div>

<table class="table table-striped table-bordered">
	<tr>
		<td><strong>Username</strong></td>
		<td id="field_username"><?=$vars['user']->get('username')?></td>
		<td>
			<button class="btn btn-info" id="username_edit_button" onclick="make_user_field_editable('username')">Edit</button>
			<button class="btn btn-success" id="username_update_button" style="visibility:hidden;" onclick="update_user_field('username')">Update</button>
		</td>
	</tr>
	<tr>
		<td><strong>Email</strong></td>
		<td id="field_email"><?=$vars['user']->get('email')?></td>
		<td>
			<button class="btn btn-info" id="email_edit_button" onclick="make_user_field_editable('email')">Edit</button>
			<button class="btn btn-success" id="email_update_button" style="visibility:hidden;" onclick="update_user_field('email')">Update</button>
		</td>
	</tr>
	<tr>
		<td><strong>First Name</strong></td>
		<td id="field_firstname"><?=$vars['user']->get('firstname')?></td>
		<td>
			<button class="btn btn-info" id="firstname_edit_button" onclick="make_user_field_editable('firstname')">Edit</button>
			<button class="btn btn-success" id="firstname_update_button" style="visibility:hidden;" onclick="update_user_field('firstname')">Update</button>
		</td>
	</tr>
	<tr>
		<td><strong>Middle Name</strong></td>
		<td id="field_middle_name"><?=$vars['user']->get('middle_name')?></td>
		<td>
			<button class="btn btn-info" id="middle_name_edit_button" onclick="make_user_field_editable('middle_name')">Edit</button>
			<button class="btn btn-success" id="middle_name_update_button" style="visibility:hidden;" onclick="update_user_field('middle_name')">Update</button>
		</td>
	</tr>
	<tr>
		<td><strong>Last Name</strong></td>
		<td id="field_lastname"><?=$vars['user']->get('lastname')?></td>
		<td>
			<button class="btn btn-info" id="lastname_edit_button" onclick="make_user_field_editable('lastname')">Edit</button>
			<button class="btn btn-success" id="lastname_update_button" style="visibility:hidden;" onclick="update_user_field('lastname')">Update</button>
		</td>
	</tr>
	<tr>
		<td><strong>Gender</strong></td>
		<td id="field_gender"><?=$vars['user']->get('gender')?></td>
		<td>
			<button class="btn btn-info" id="gender_edit_button" onclick="make_user_field_editable('gender')">Edit</button>
			<button class="btn btn-success" id="gender_update_button" style="visibility:hidden;" onclick="update_user_field('gender')">Update</button>
		</td>
	</tr>
	<tr>
		<td><strong>Mobile Phone</strong></td>
		<td id="field_mobile_phone"><?=$vars['user']->get('mobile_phone')?></td>
		<td>
			<button class="btn btn-info" id="mobile_phone_edit_button" onclick="make_user_field_editable('mobile_phone')">Edit</button>
			<button class="btn btn-success" id="mobile_phone_update_button" style="visibility:hidden;" onclick="update_user_field('mobile_phone')">Update</button>
		</td>
	</tr>
	<tr>
		<td><strong>Landline Phone</strong></td>
		<td id="field_landline"><?=$vars['user']->get('landline')?></td>
		<td>
			<button class="btn btn-info" id="landline_edit_button" onclick="make_user_field_editable('landline')">Edit</button>
			<button class="btn btn-success" id="landline_update_button" style="visibility:hidden;" onclick="update_user_field('landline')">Update</button>
		</td>
	</tr>
</table>
