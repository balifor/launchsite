<?php
/**
 * Wrapper function.
 *
 *	A wrapper function to access the launcher. 
 */

/**
 * Global wrapper function.
 * 
 * Optionally included wrapper function to globally access the launcher.
 */
function launchsite()
{
	require_once 'launcher.php';
	return \Launchsite\launcher\launcher::launch();
}
