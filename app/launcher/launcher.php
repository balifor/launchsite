<?php
/**
 * Framework Launcher.
 *
 * A static instance that launches the app and holds the config.
 */

namespace Launchsite\launcher;

/**
 * The launcher for the framework.
 *
 * A singleton that holds the config for the app and handles requests.
 * It's accessible via any class that extends core with self::launchsite().
 */
class launcher
{
	/**
	 * The static instance of the launcher.
	 *
	 * @var launcher.
	 */
	private static $launcher;

	/**
	 * Request start time for debugging purposes.
	 *
	 * @var int The start time of the app.
	 */
	public $start_time;

	/**
	 * A list of config files for debug.
	 *
	 * @var array An array of paths to the config files.
	 */
	public $config_files;

	/**
	 * The site config, you can use the framework config file for examples of each setting.
	 *
	 * @var array The config array.
	 */
	private $config = array();

	/**
	 * A psr4 autoloader that you can add paths to.
	 *
	 * @var autoloader.
	 */
	public $autoloader;

	/**
	 * Engines for the framework.
	 *
	 * @var array
	 */
	private $engines = array();

	/**
	 * The current route array being requested.
	 *
	 * @var array.
	 */
	public $current_route;

	/**
	 * The request variables.
	 *
	 * @var array
	 */
	private $vars = array();

	/** 
	 * An array of dbs available.
	 *
	 * @var array.
	 */
	private $dbs = array();

	/**
	 * Singleton constructor.
	 */
	protected function __construct() { }

	/**
	 * Singleton clone.
	 */	
    private function __clone() { }

	/**
	 * Singleton wakeup.
	 */	
    private function __wakeup() { }

	/**
	 * Create a singleton launcher for the app.
	 *
	 * @return laucnher The launcher instance.
	 */
	public static function launch()
	{
		if (null === static::$launcher) {
			static::$launcher = new static();
			static::$launcher->start_time = microtime(true);
		}

		return static::$launcher;
	}

	/**
	 * Handle a request.
	 *
	 * @param string $url The URL to handle a request for.
	 *
	 * @return void Echo's out a page.
	 */
	public function handle_request($url = false)
	{
		//Bootstrap the app
		$this->bootstrap();

		//Run the request
		$vars = $this->get_engine('request')->run_request($url);

		//Respond to the request
		$vars = $this->get_engine('response')->respond($vars); 

		return $vars;
	}

	/**
	 * Load up the an instance of the framework.
	 *
	 * @return void 
	 */
	public function bootstrap()
	{
		mb_internal_encoding('UTF-8');

		//Set the error handlers
		$this->set_error_handlers();

		//Set default SITE and FRAMEWORK constants
		$this->set_default_constants();

		//Get site config.
		$this->get_config_files();

		//Set debug level
		$this->set_debug_level();

		//Set the Tiemzone
		date_default_timezone_set($this->get_config('TIMEZONE'));

		//Autoload classes.
		$this->get_autoloader();

		//Start the session.
		$this->start_session();

		//Load the route files
		$this->load_routes();
	}

	/**
	 * Set debug level.
	 *
	 * @return bool true on complete.
	 */
	protected function set_debug_level()
	{
		if ($this->get_config('DEBUG')) {
			error_reporting($this->get_config('DEBUG_LEVEL'));
			ini_set("display_errors", 'On');
		} else {
			ini_set("display_errors", 0);
		}

		return true;
	}

	/**
	 * Set the error handlers.
	 *
	 * @return bool True on completion.
	 */
	protected function set_error_handlers()
	{
		//Set the fatal error handler
		//register_shutdown_function(array($this, "error_catcher"));

		//Set the error handler
		set_error_handler(array($this, "error_catcher"));
	
		//Set up the exception handler
		//set_exception_handler(array($this, 'handle_exception'));

		return true;
	}

	/**
	 * Show a fatal error.
	 *
	 * @param string $type The array code.
	 *
	 * @param string $message The Exception message.
	 *
	 * @param string $file The file the error occured in.
	 *
	 * @param string $line The line the error occured on.
	 *
	 * @return string.
	 */
	public function error_catcher($type, $message, $file, $line)
	{
		$_ERRORS = Array(
				0x0001 => 'E_ERROR',
				0x0002 => 'E_WARNING',
				0x0004 => 'E_PARSE',
				0x0008 => 'E_NOTICE',
				0x0010 => 'E_CORE_ERROR',
				0x0020 => 'E_CORE_WARNING',
				0x0040 => 'E_COMPILE_ERROR',
				0x0080 => 'E_COMPILE_WARNING',
				0x0100 => 'E_USER_ERROR',
				0x0200 => 'E_USER_WARNING',
				0x0400 => 'E_USER_NOTICE',
				0x0800 => 'E_STRICT',
				0x1000 => 'E_RECOVERABLE_ERROR',
				0x2000 => 'E_DEPRECATED',
				0x4000 => 'E_USER_DEPRECATED'
				);

		if (!@is_string($name = @array_search($type, @array_flip($_ERRORS)))) {
			$name = 'E_UNKNOWN';
		};
    
		return(print(@sprintf("%s Error in file \xBB%s\xAB at line %d: %s\n", $name, @basename($file), $line, $message)));
	}

	/**
	 * Function to handle exceptions.
	 *
	 * @param \Exception $exception An exception to handle.
	 *
	 * TODO: write this.
	 *
	 * @return bool True on complete.
	 */
	protected function handle_exception($exception)
	{
		return true;
	}

	/**
	 * Set default configs.
	 *
	 * @return bool True on complete.
	 */
	protected function set_default_constants()
	{
		//Ensure SITE is defined
		if (!defined('SITE')) {
			$path = explode(DIRECTORY_SEPARATOR, getcwd());
			array_pop($path);
			array_pop($path);
			$application_path = implode(DIRECTORY_SEPARATOR, $path) . DIRECTORY_SEPARATOR;
			define('SITE', $application_path);
		}

		//Ensure FRAMEWORK is defined
		if (!defined('FRAMEWORK')) {
			define('FRAMEWORK', SITE . 'launchsite' . DIRECTORY_SEPARATOR);
		}
	
		return true;
	}

	/**
	 * Set a config item.
	 *
	 * @param string $key The config array key to get the data as.
	 *
	 * @param mixed $value The value to store in the config array.
	 */
	public function set_config($key, $value, $subkey = false)
	{
		if ($subkey) { 
			if (!isset($this->config[$key])) {
				$this->config[$key] = array();
			}

			$this->config[$key][$subkey] = $value;
		} else {
			$this->config[$key] = $value;
		}
	}

	/**
	 * Add an item to an existing config array.
	 *
	 * @param string $key The config array key to get the data as.
	 *
	 * @param mixed $value The value to store in the config array.
	 *
	 * @param string $add_as Add with this key.
	 */
	public function add_to_config($key, $value, $add_as = false)
	{
		if (!is_array($this->config[$key])) {
			$this->config[$key] = array($this->config[$key]);
		}

		if ($add_as) {
			$this->config[$key][$add_as] = $value;
		} else {
			$this->config[$key][] = $value;
		}
	}

	/**
	 * Merge an item into an existing config array.
	 *
	 * @param string $key The config array key to get the data as.
	 *
	 * @param mixed $value The value to store in the config array.
	 */
	public function merge_into_config($key, $value)
	{
		if (!is_array($this->config[$key])) {
			$this->config[$key] = array($this->config[$key]);
		}

		$this->config[$key] = array_merge($this->config[$key], $value);
	}

	/**
	 * Get a config item.
	 *
	 * @param string $key The config array by key to return.
	 *
	 * @return mixed Either full array or named key if exists, null if not.
	 */
	public function get_config($key = false)
	{
		if (!$key) {
			return $this->config;
		} elseif (isset($this->config[$key])) { 
			return $this->config[$key];
		} else {
			return null;
		}
	}

	/**
	 * Load config files.
	 *
	 * @return bool True on success.
	 */
	protected function get_config_files()
	{
		//Get the main config files at the default paths.
		$this->config_files['CONFIGS'] = array_unique(array(FRAMEWORK . 'app' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.php', SITE . 'app' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.php'));
		foreach ($this->config_files['CONFIGS'] as $path) {
			//Check the file exists and is readable.
			if (is_readable($path)) {            
				require_once $path;
			}
		}

		//Get any extra config files set it main config.
		$extra_config_files = $this->get_config('EXTRA_CONFIG_FILES'); 
		if (!is_null($extra_config_files)) {
			if (count($extra_config_files)) {
				foreach ($extra_config_files as $path) {
					//Check the file exists and is readable.
					if (is_readable($path)) {            
						require_once $path;
					}					
				}
			}
		}

		return true;
	}

	/**
	 * Start autoloading classes.
	 *
	 * @return bool True on completion.
	 */
	protected function get_autoloader()
	{
		//Require the autoloader class.
		$autoloader_path = 'autoloader.php';
		require_once $autoloader_path;

		//Register the autoloader
		$this->autoloader = new autoloader();
		$this->autoloader->register();

		$autoload_paths = $this->get_config('FRAMEWORK_AUTOLOAD_PATHS'); 
		$site_paths = $this->get_config('SITE_AUTOLOAD_PATHS');
		if ($site_paths && count($site_paths)) {
			$autoload_paths = array_unique(array_merge($autoload_paths, $site_paths));
		}

		foreach ($autoload_paths as $namespace => $base_dir) {
			$this->autoloader->addNamespace($namespace, $base_dir);
		}

		//Register the site autoloader if it exists
		$site_autoloader = $this->get_config('SITE_AUTOLOADER'); 
		if (!is_null($site_autoloader)) {
			if(is_readable($site_autoloader))	
				require $site_autoloader;
		}

		return true;
	}

	/**
	 * Get an engine the framework uses.
	 *
	 * @param string $name The name of the engine to return.
	 *
	 * @return mixed The engine handler.
	 */
	public function get_engine($name)
	{
		//Try and load the engine.
		if (isset($this->engines[$name])) {
			return $this->engines[$name];
		} else {
			$engines = $this->get_config('ENGINES');

			if (isset($engines[$name])) {
				if (class_exists($engines[$name])) {
					$engine = new $engines[$name]();
					$engine->load_engine();
					
					$this->engines[$name] = $engine;
					return $this->engines[$name];
				} else {
					return null;
				}
			} else {
				return null;
			}
		}
	}

	/**
	 * Start up the session.
	 *
	 * @return void
	 */
	public function start_session()
	{
		$request = $this->get_engine('request');
		if ($request->get_request_type() != 'cli') {
			//Start session
			session_start();
		}

		//Session time limits
		$conf_timeout = $this->get_config('SESSION_TIMEOUT');
		$session_timeout = is_numeric($conf_timeout) ? $conf_timeout : false; 
		if ($session_timeout != false) {
			if (isset($_SESSION['EXPIRES'])) {
				if ($_SESSION['EXPIRES'] != false) {
					if ($_SESSION['EXPIRES'] < time()) {
						if ($request->get_request_type() != 'cli') {
							session_destroy();
							session_start();
							header("Location: /");
							exit;
						}
					} else {
						$_SESSION['EXPIRES'] = time() + $conf_timeout;
					}
				}
			} else {
				if ($request->get_request_type() != 'cli') {
					if ($this->get_engine('user')->logged_in()) {
						session_destroy();
						session_start();
					}
				}
			}
		}
	}

	/**
	 * Load the routes
	 *
	 * @return void
	 */
	public function load_routes()
	{
		//Load other routes files
		$config_files = array_unique(array(
					FRAMEWORK . 'app' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'routes.php',
					SITE . 'app' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'routes.php')
				);

		//Get all config files
		foreach ($config_files as $path) {
			//Check the file exists and is readable.
			if (is_readable($path)) {
				require_once $path;
			}
		}
	}
}
