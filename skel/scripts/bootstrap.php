<?php
/**
 * Bootstrap the app 
 */
	//Get the path
	$path = explode(DIRECTORY_SEPARATOR, __DIR__);
	array_pop($path);

	//Base of application
	$site_path = implode(DIRECTORY_SEPARATOR, $path) . DIRECTORY_SEPARATOR;
	define('SITE', $site_path);
	
	//Base of launchsite directory
	$framework_path = $site_path . 'launchsite' . DIRECTORY_SEPARATOR;
	define('FRAMEWORK', $framework_path);
	$application_path = implode(DIRECTORY_SEPARATOR, $path) . DIRECTORY_SEPARATOR;

	require_once FRAMEWORK . 'app' . DIRECTORY_SEPARATOR . 'launcher' . DIRECTORY_SEPARATOR . 'launcher.php';

	//Get launcher
	$launcher = \Launchsite\launcher\launcher::launch();

	//Bootstrap the app.
	$launcher->bootstrap();
