<?php
/**
 * The example model.
 *
 * Models an Example..
 */

namespace Site\models;

/**
 * Example class.
 *
 * Template.
 */
class example extends \Launchsite\abstracts\model 
{
	/**
	 * The database table for the class.
	 *
	 * @var string.
	 */
    public $table = 'example_table';
}
