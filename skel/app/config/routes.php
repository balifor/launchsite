<?php
/**
 * The routes for the project.
 *
 * Call in the router and add routes.
 */

	use Launchsite\launcher\launcher;
	$router = launcher::launch()->get_engine('routing');

	/**
	 * Site Routes
	 */
	    /**
	     * Home Page
	     */
	        $router->add_route('root', array('/', 'Site\controllers\site_controller', 'index'));
			$router->add_route('about', array('/about', '\Site\controllers\site_controller', 'about'));
			$router->add_route('privacy policy', array('/privacy_policy', '\Site\controllers\site_controller', 'privacy_policy'));
			$router->add_route('cookie policy', array('/cookie_policy', '\Site\controllers\site_controller', 'cookie_policy'));
