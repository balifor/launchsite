<?php
/**
 * Site Config
 */
	use \Launchsite\launcher\launcher as launcher;

	$launchsite = launcher::launch();
	
	$launchsite->set_config('SITE_NAME', 'Launchsite');
	$launchsite->set_config('TIMEZONE', 'Europe/London');

	$host = isset($_SERVER['HTTP_HOST)']) ? $_SERVER['HTTP_HOST'] : 'localhost';
	$launchsite->set_config('SYS_ADMIN_EMAIL', 'admin@example.com');


	/**
	 * Site Autoload Paths
	 */
		$launchsite->set_config('SITE_AUTOLOAD_PATHS', array(
			'Site' => SITE . 'app' . DIRECTORY_SEPARATOR,
    	));

	/**
	 * Host specific config
	 */
		$host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
		switch($host)
		{
			case 'example.com':
				
				$launchsite->set_config('ENV', 'live');
				$launchsite->set_config('DEBUG', false);

				$launchsite->set_config('DB', array(
					'ENGINE' => 'pdo_db',
					'TYPE' => 'mysql',
					'HOST' => 'localhost',
					'NAME' => 'example',
					'PORT' => '3306',
					'USER' => 'root',
					'PASS' => '',
				), 'MAIN');

			break;
			default :

				$launchsite->set_config('ENV', 'dev');
				$launchsite->set_config('DEBUG', true);

				$launchsite->set_config('DB', array(
					'ENGINE' => 'pdo_db',
					'TYPE' => 'mysql',
					'NAME' => 'site',
					'SOCKET' => '/Applications/MAMP/tmp/mysql/mysql.sock',
					//'HOST' => 'localhost',
					//'PORT' => '3306',
					'USER' => 'root',
					'PASS' => 'root',
				), 'MAIN');

			break;
		}
