<!DOCTYPE HTML>
<html>
    <head>
        <title><?= isset($vars['page_title']) ? $vars['page_title'] : $this->launchsite()->get_config('SITE_NAME')?></title>
        
        <!-- Meta Tags, Content type, Description and Keywords -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="<?= isset($vars['meta_author']) ? $vars['meta_author'] : 'Ben Little'?>">
        <meta name="description" content="<?= isset($vars['meta_description']) ? $vars['meta_descriptions'] : 'Develop your web applications in your browser'?>" />
        <meta name="keywords" content="<?= isset($vars['meta_keywords']) ? $vars['meta_keywords'] : 'Developer, Web Development'?>" />

        <!-- Site JS -->
        <script type="text/javascript" src="/javascript/launchsite.js"></script>
        
        <!-- Site CSS -->
        <link rel="stylesheet" type="text/css" href="/css/processed/screen.css" />

		<!-- jQuery library -->
		<script src="/lib/jquery/jquery.min.js"></script>

		<!-- Bootstrap -->
		<!-- Latest compiled JavaScript -->
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="/lib/bootstrap/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container-fluid">
			<div id="topbar" class="row jumbotron">
				<div class="col-xs-12">

					<div class="col-xs-6 text-center"> 
						<img src="/images/dragon.gif" alt="dragon" />
					</div>

					<div class="col-xs-6 text-center"> 
						<ul class="push_right no_bullets">
							<?php if ($this->get_engine('user')->logged_in()) { ?>
							<li>Welcome <?=$this->get_engine('user')->get_current_user()->get('username')?>!</li>
							<li><?=$this->link($this->get_engine('routing')->url('my profile'), 'Profile')?> | <?=$this->link($this->get_engine('routing')->url('logout'), 'Logout')?></li>
							<?php } else { ?>
							<li><?=$this->link($this->get_engine('routing')->url('login'), 'Login')?></li>
							<?php } ?>
						</ul>
						<h1><?=$this->link($this->get_engine('routing')->url('root'), 'Home')?></h1>
						<p>- Tagline</p>
						<?php if ($this->get_engine('user')->logged_in()) { ?>
						<ul class="main_nav_links list-inline">
							<li class="list-inline-item"><?=$this->link($this->get_engine('routing')->url('about'), 'About')?></li>
						</ul>
						<?php } ?>
					</div>

				</div>
            </div>
            <div class="content-wrap">

					<div id="contentbox" class="row">
						<div class="col-xs-offset-1 col-xs-10"> 
                    		<?= $vars['CONTENTS']; ?>
						</div>
					</div>

					<?php if ($this->launchsite()->get_config('ENV') == 'dev') { ?>
					<div class="sysinfo row">
						<div class="col-xs-12">
							<?=$this->request_errors_table();?>
							<?=$this->request_messages_table();?>
						</div>
					</div>
					<?php } ?>

			</div>
			<div class="footer-wrap">

				<div id="footerbar" class="row">
					<div class="col-xs-offset-2 col-xs-8">
						<div class="col-xs-12 col-lg-6 spacer"> 
							<?=$this->link($this->get_engine('routing')->url('about'), 'About')?> |
							<?=$this->link($this->get_engine('routing')->url('privacy policy'), 'Privacy Policy')?> | 
							<?=$this->link($this->get_engine('routing')->url('cookie policy'), 'Cookie Policy')?>
						</div>
						<div class="col-xs-12 col-lg-6 spacer">
						</div>
					</div>
				</div>

			</div>
        </div>
    </body>
</html>
