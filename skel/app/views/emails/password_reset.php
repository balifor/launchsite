<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <table>
            <tr>
                <td>
                    <?=$this->get_contents('emails/header.php');?>
                </td>
            </tr>
            <tr>
                <td>
                    <h1>Password Reset</h1>
                    <p>We've had a request to reset your password</p>
                    <p>The token to reset your password is: <?=$vars['token']?></p>
                    <p>Use it in the next hour to reset your password</p>
                </td>
            </tr>
            <tr>
                <td>
                    <?=$this->get_contents('emails/footer.php');?>
                </td>
            </tr>
        </table>
    </body>
</html>
