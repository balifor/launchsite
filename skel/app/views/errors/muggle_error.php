<div class="section">
    <h2>Sorry, there's been an error.</h2>
    <hr>
    <p>The web team is already on the case.</p>
    <br />
    <p>Try either:</p>
    <button onclick="window.history.go(-1)">Back</button>
    <?=self::link($this->get_engine('routing')->url('root'), '<button>Home Page</button>')?>
</div>
