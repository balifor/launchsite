<div class="page_title">
    <h2>Error</h2>    
</div>
<div class="mobile-col-12 tablet-col-12 large-col-12">
    <div class='section'>
        <h3>Sorry there's been an error</h3>
        <?php foreach($vars as $key => $var) { ?>
        <div class="mobile-col-12 tablet-col-3 large-col-2">
            <?=$key?>
        </div>
        <div class='mobile-col-12 tablet-col-9 large-col-10'>
            <pre><?=var_dump($var);?></pre>
        </div>
        <?php } ?>
    </div>
</div>