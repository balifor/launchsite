<div class="section">
    <h2>403 - Not Authed</h2>
    <hr>
    <p>You dont have permission to view the URL:</p>
    <p>"<?=isset($vars['path']) ? $vars['path'] : '';?>"</p>
    <br />
    <p>Try either:</p>
    <button onclick="window.history.go(-1)">Back</button>
    <?=self::link($this->launchsite()->get_rouger()->url('root'), '<button>Home Page</button>')?>
</div>
