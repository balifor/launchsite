<h4>Update Password</h4>
<?php if(isset($vars['update_password_error'])) {
	echo $this->generate_alert('error', $vars['update_password_error']);
} ?>

<form id="update_password_form">
    <div class="form-group">
    	<label for="password">Password: </label>
    	<input class="form-control" type="password" placeholder="Password" name="pass">
	</div>
    <div class="form-group">
    	<label for="password">Confirm Password: </label>
    	<input class="form-control" type="password" placeholder="Confirm Password" name="confirm_pass">
	</div>
    <div class="form-group">
		<input type="hidden" name="email" <?= isset($vars['update_password_data']) && isset($vars['update_password_data']['email']) ? "value='" . $vars['update_password_data']['email'] . "'" : '';?>>
		<input type="hidden" name="token" <?= isset($vars['update_password_data']) && isset($vars['update_password_data']['token']) ? "value='" . $vars['update_password_data']['token'] . "'" : '';?>>
		<?=$this->ajax_submit('update_password_form', $this->launchsite()->get_engine('routing')->url('update password', false, 'post'), 'Update Password')?>
    </div>
</form>
