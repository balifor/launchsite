<?php 
if(isset($vars['registration_error'])) { 
	echo $this->generate_alert('error', $vars['registration_error']);
} ?>

<form id="register_form">
	<div class="form-group">
		<label for="email">Email: </label>
		<input class="form-control" type="text" placeholder="Email" name="email" <?= isset($vars['registration_data']) && isset($vars['registration_data']['email']) ? "value='" . $vars['registration_data']['email'] . "'" : '';?>>
	</div>
	<div class="form-group">
    	<label for="username">Username: </label>
    	<input class="form-control" type="text" placeholder="Username" name="username" <?= isset($vars['registration_data']) && isset($vars['registration_data']['username']) ? "value='" . $vars['registration_data']['username'] . "'" : '';?>>
    </div>
	<div class="form-group">
		<label for="password">Password: </label>
		<input class="form-control" type="password" placeholder="Password" name="pass" <?= isset($vars['registration_data']) && isset($vars['registration_data']['pass']) ? "value='" . $vars['registration_data']['pass'] . "'" : '';?>>
    </div>
	<div class="form-group">
		<label for="password">Confirm Password: </label>
		<input class="form-control" type="password" placeholder="Confirm Password" name="confirm_pass" <?= isset($vars['registration_data']) && isset($vars['registration_data']['confirm_pass']) ? "value='" . $vars['registration_data']['confirm_pass'] . "'" : '';?>>
    </div>
	<div class="form-group">
		<?=$this->ajax_submit('register_form', $this->launchsite()->get_engine('routing')->url('register', false, 'post'), 'Register')?>
    </div>
</form>
