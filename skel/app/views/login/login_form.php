<?php
if(isset($vars['login_error'])) { 
	echo $this->generate_alert('error', $vars['login_error']);
} ?>

<form id="login_form">
	<div class="form-group">
    	<label for="email">Email</label>
		<?php $email = isset($vars['login_data']) && isset($vars['login_data']['email']) ? $vars['login_data']['email'] : ''; ?>
    	<input class="form-control" type="text" name="email" value="<?=$email;?>">
	</div>
	<div class="form-group">
    	<label for="pass">Password</label>
		<?php $pass = isset($vars['login_data']) && isset($vars['login_data']['pass']) ? $vars['login_data']['pass'] : ''; ?>
    	<input class="form-control" type="password" name="pass" value="<?=$pass?>">
	</div>
	<div class="form-group">
		<?=$this->ajax_submit('login_form', $this->get_engine('routing')->url('ajax login', false, 'POST'), 'Log In');?>
    </div>
</form>
