<?php
/**
 * Example engine.
 *
 * Create or replace an engine for the framework.
 */

namespace Site\engines;

/**
 * Engine class.
 *
 * Template.
 */
class example extends \Launchsite\abstracts\engine implements \Launchsite\interfaces\example_engine
{       

}
