<?php
/**
 * The controller for example routes.
 *
 * Handles the example.
 */

namespace Site\controllers;

/**
 * Example controller.
 */
class site_controller extends \Launchsite\abstracts\controller 
{
	public $use_api = false;
	public $login_required = false;

	/**
	 * Site home page
	 */
	public function index() {}

	/**
	 * Site about page
	 */
	public function about() {}

	/**
	 * Site privacy policy
	 */
	public function privacy_policy() {}

	/**
	 * Site cookie policy
	 */
	public function cookie_policy() {}
}
