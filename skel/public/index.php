<?php
//Define paths.
//Work backwards from current directory.
$path = explode(DIRECTORY_SEPARATOR, dirname(__FILE__));

array_pop($path);

//Base of site directory
$site_path = implode(DIRECTORY_SEPARATOR, $path) . DIRECTORY_SEPARATOR;
define('SITE', $site_path);

//Base of launchsite directory, update as necessary.
define('FRAMEWORK', $site_path . 'launchsite' . DIRECTORY_SEPARATOR);

//Prefer to use the wrapper function for launchsite?
//Switch the $launchsite initialisation below with this one.
//require_once FRAMEWORK . 'app' . DIRECTORY_SEPARATOR . 'launcher' . DIRECTORY_SEPARATOR . 'wrapper.php';
//$launchsite = launchsite(); 

//Comment out if using wrapper function.
require_once FRAMEWORK . 'app' . DIRECTORY_SEPARATOR . 'launcher' . DIRECTORY_SEPARATOR . 'launcher.php';
$launchsite = \Launchsite\launcher\launcher::launch();

//Handle the request
$launchsite->handle_request();
