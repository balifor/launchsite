///////////////////////////////////////
// Javascript Library for Launchsite //
//                                   //
// Relies on Jquery                  //
///////////////////////////////////////
class Launchsite {
/////////////////
// Constructor //
/////////////////
	constructor() { 
		this.sockets = [];
		this.tabs = {};
	}

///////////////////
// Ajax Function //
///////////////////
	/////////////////////////////////
	// Ajax GET function           //
	//                             //
	// url: URL to GET             //
	// callback: callback function //
	/////////////////////////////////
	get(url, callback) {
		let xhr = new XMLHttpRequest();
		
		xhr.open('GET', url);
		
		let response = {};
		xhr.onload = function() {
			return callback(xhr);
		};

		xhr.send();
	}

	/////////////////////////////////
	// Ajax POST function          //
	//                             //
	// url: URL to POST to         // 
	// data: key: value object     //
	// callback: callback function //
	/////////////////////////////////
	post(url, data, callback = false) {
		let xhr = new XMLHttpRequest();

		xhr.open('POST', url);

		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		
		let response = {}
		xhr.onload = function() {
			if (callback) {
				return callback(xhr);
			}
		};

		xhr.send(launchsite.uri_encode(data));
	}

	///////////////////////////////
	// URI encode an object      //
	//                           //
	// obj: the object to encode //
	///////////////////////////////
	uri_encode(obj) {
		var encodedString = '';
		for (var prop in obj) {
			if (obj.hasOwnProperty(prop)) {
				if (encodedString.length > 0) {
					encodedString += '&';
				}

				encodedString += encodeURI(prop + '=' + obj[prop]);
			}
		}

		return encodedString;
	}

	//////////////////////////////////////
	// Ajax update for a form.          //
	//                                  //
	// url: URL to POST to              //
	// form: id of the form to use      //
	// callback: callback function      //
	//////////////////////////////////////
	form_update(url, input, callback = false) {
		event.preventDefault();

		let form = document.getElementById(input);
		let formData = new FormData(form),

		xhr = new XMLHttpRequest();

		xhr.open('POST', url);

		xhr.send(formData);

		if (callback) {
			xhr.onload = function() {
				return callback(xhr);
			};
		}
	}

	//////////////////////////////////////////////////////
	// Feeder function for page_update from form_update //
	//                                                  //
	// xhr: xhr object                                  //
	//////////////////////////////////////////////////////
	xhr_page_update(xhr) {
		launchsite.page_update(xhr.response);
	}

	/////////////////////////////////////////
	// Page Update function                //
	//                                     //
	// id: The id of the element to update //
    /////////////////////////////////////////
	page_update(data, callback) {

		if(typeof data == 'string' || data instanceof String) {
			if(data.trim() == '') {
				if(callback) {
					callback();
					return true;
                }
            }

			try {
				let obj = JSON.parse(data);
				console.log(obj);

				for (let key in obj) {
					if(obj[key] == 'RELOAD' || key == 'RELOAD') {
						location.reload();
					} else if(key == 'ALERT') {
						alert(obj[key]);
					} else if(key == 'GOTO') {
						window.location.replace(obj[key]);
						return false;
					} else if(obj[key] == 'FALSE' || key == 'FALSE') {
						return false;
					} else {
						console.log(document.getElementById(key));
						document.getElementById(key).innerHTML = obj[key];
					}
				}
			} catch(e) { 
				console.log(e);
			}
        
			if(callback) {
				callback(form, data);
			}
        
			return true;
		}
	}

//////////////
// Lightbox //
//////////////
	/////////////////////////////////
	// Add a lightbox element      //
	//                             //
	// id: The id for the lightbox //
	// html: the HTML to use       //
	/////////////////////////////////
	create_lightbox(id = 'main', html = '') {
		let ls = this;
		
		//Outer frame
		let outer = document.createElement('div');
		outer.classList.add('lightbox_background');
		outer.id = id;

		//Lightbox
		let lightbox = document.createElement('div');
		lightbox.classList.add('lightbox');

		//Taskbar
		let taskbar = document.createElement('div');
		taskbar.classList.add('lightbox_taskbar');

		//Close
		let close = document.createElement('span');
		close.classList.add('lightbox_close');
		close.id = id + '_close';
		close.appendChild(document.createTextNode('x'));
		close.addEventListener('click', function() {
			ls.close_lightbox(id);
		});

		//Inner box
		let inner = document.createElement('div');
		inner.classList.add('lightbox_contents')
		inner.id = id + '_contents';
		inner.innerHTML = html;

		//Add it to body
		taskbar.appendChild(close)
		lightbox.appendChild(taskbar);
		lightbox.appendChild(inner);
		outer.appendChild(lightbox);
		document.body.appendChild(outer);
	}

	//////////////////////////////////
	// Set contents of lightbox     // 
	//                              //
	// html: The HTML to use        //
	// id: id of lightbox to update //
	//////////////////////////////////
	lightbox_update(id = 'main', html = '') {
    	open_lightbox();
    
    	update(url);
    
    	return false;
	}

	////////////////////////////////
	// Close the lightbox         //
	//                            //
	// id: the id of the lightbox //
	////////////////////////////////
	close_lightbox(id = 'main') {
    	let lightbox = document.getElementById(id);
		lightbox.parentNode.removeChild(lightbox);
	}

/////////////
// Cookies //
/////////////
	////////////////////////////////
	// Set a cookie               //
	//                            //
	// name: name of the cookie   //
	// value: value of the cookie //
	// exdays: Expiry days        //
	////////////////////////////////
	set_cookie(name,value,exdays) {
    	let d = new Date();
    	d.setTime(d.getTime()+(exdays*24*60*60*1000));
    	let expires = "expires="+d.toGMTString();
    	
		document.cookie = name + "=" + value + "; " + expires;
	}

	//////////////////////////////////
	// Get a cookie                 //
	//                              //
	// name: the name of the cookie //
	//////////////////////////////////
	get_cookie(name) {
    	let cname = name + "=";
    	let ca = document.cookie.split(';');

    	for(let i=0; i<ca.length; i++) {
    	    let c = ca[i].trim();
    	    if (c.indexOf(name)==0) {
				return c.substring(name.length,c.length);
			}
    	}

    	return false;
	}

//////////
// Tabs //
//////////
	//////////////////////////////////
	// Managed cycling through tabs //
	//                              //
	// prefix: instance id          //
	// tabs: obj of tabs            //
	// match: tab to activate       //
	// update: manage the tab       //
	//////////////////////////////////
	generate_tabs(prefix, tabs, match = false) {

		if (match == false) {
			match = Object.keys(tabs)[0]; 
		}

		launchsite.tabs[prefix] = tabs;

		var list = document.getElementById(prefix);
		if (list != null) {
			list.classList.add('managed-tabs');

			for (let tab in tabs) {

				let this_tab = document.createElement('li');
				this_tab.id = tabs[tab] + '_tab';
				this_tab.classList.add('managed-tab');
				this_tab.innerHTML = tab;

				if (tabs[tab] == match) {
					this_tab.classList.add("managed-tabs-active-tab");
				}

				this_tab.addEventListener('click', function(event) {
					launchsite.switch_tab(prefix, tabs[tab]);
				});

				let this_panel = document.getElementById(tabs[tab]);
				if (this_panel !== null) {
					if (tabs[tab] == match) {
						this_panel.style.display = 'block';
					} else {
						this_panel.style.display = 'none';
					}
				}

				list.appendChild(this_tab);
			}
		}
	}

	switch_tab(prefix, match) {

		if (launchsite.tabs.hasOwnProperty(prefix)) {

			var tabs = launchsite.tabs[prefix];

			for (let tab in tabs) {

				let this_tab_list = document.getElementById(prefix);
				let this_tab = this_tab_list.querySelector('#' + tabs[tab] + '_tab');
				if (tabs[tab] == match) {
					this_tab.classList.add("managed-tabs-active-tab");
				} else {
					this_tab.classList.remove("managed-tabs-active-tab");
				}

				let this_panel = document.getElementById(tabs[tab]);
				if (this_panel !== null) {
					if (tabs[tab] == match) {
						this_panel.style.display = 'block';
					} else {
						this_panel.style.display = 'none';
					}
				}
			}
		}
	}

////////////////
// Websockets //
////////////////
	///////////////////////////////////////////
	// Connect to a websocket                //
	//                                       //
	// host: the host to connect to          //
	// onopen: function for on socket open   //
	// onmessage: function for on message    //
	// onclose: function for on socket close //
	// callback: optional callback function  //
	///////////////////////////////////////////
	ws_connect(id, host, callback = false) {
		try {
			this.ws_disconnect(id);
	
			this.sockets[id] = new WebSocket(host);

			if (callback) {
				callback(id);
			}

			return true;
		} catch (ex) {
			return false;
		}
	}

	//////////////////////////////
	// Get a socket by id       //
	//                          //
	// id: the id of the socket //
	//////////////////////////////
	ws_get_socket(id) {
		if (this.sockets[id] != undefined) {
			return this.sockets[id];
		}

		return false;
	}

	//////////////////////////////////////////
	// Disconnect a websocket               //
	//                                      //
	// id: id of the socket to disconnect   //
	// callback: optional callback function //
	//////////////////////////////////////////
	ws_disconnect(id, callback = false) {
		if (this.sockets[id] != undefined) {
			this.sockets[id].close();
			this.sockets[id] = null;
	
			if (callback) {
				callback();
			}

			return true;
		}
			
		return false;
	}

	///////////////////////////////////////
	// Reconnect a websocket             //
	//                                   //
	// id: id of the socket to reconnect //
	// callback: option callback         //
	///////////////////////////////////////
	ws_reconnect(id, callback = false) {
		if (this.sockets[id] != undefined) {
			this.sockets[id].close();
			this.sockets[id] = null;
	
			if (callback) {
				callback();
			}
	
			return true;
		}

		return false;
	}

	/////////////////////////////////
	// Send to a websocket         //
	//                             //
	// id: id of the socket to use //
	// msg: the message to send    //
	// callback: optional callback //
	/////////////////////////////////
	ws_send(id, msg, callback = false) {
		if (this.sockets[id] != null) {
			try {
				this.sockets[id].send(msg);

				if (callback) {
					callback(id, msg);
				}

				return true;
			} catch (ex) {
				return false;
			}
		}
			
		return false;
	}
}

///////////////////////////////
// domReady library function //
// Replacement for jquery    //
///////////////////////////////
(function(exports, d) {
	function domReady(fn, context) {

 		function onReady(event) {
			d.removeEventListener("DOMContentLoaded", onReady);
			fn.call(context || exports, event);
 		}

		function onReadyIe(event) {
			if (d.readyState === "complete") {
				d.detachEvent("onreadystatechange", onReadyIe);
				fn.call(context || exports, event);
			}
		}

		d.addEventListener && d.addEventListener("DOMContentLoaded", onReady) ||
		d.attachEvent      && d.attachEvent("onreadystatechange", onReadyIe);
	}

 	exports.domReady = domReady;
})(window, document);

/////////////////////////////////
// Start the Launchsite object //
/////////////////////////////////
domReady(function(event) {
	launchsite = new Launchsite();
});
