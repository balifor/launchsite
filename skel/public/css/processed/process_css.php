<?php
$css_file = basename($_SERVER['SCRIPT_FILENAME']);
$cache = false;
$cache_time = '1 hour';
$cached_version = 'cache/' . $css_file;
$buffer = '';

if ($cache && is_readable($cached_version) && filemtime($cached_version) < strtotime('-' . $cache_time)) {
	$buffer .= file_get_contents($cached_version);
} elseif (is_readable($css_file)) {
	ob_start();
	require_once($css_file);
	$buffer = ob_get_clean();
	//Remove comments
	$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
	// Remove space after colons
	$buffer = str_replace(': ', ':', $buffer);
	// Remove whitespace
	$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
	file_put_contents($cached_version, $buffer);
} else {
	header("HTTP/1.0 404 Not Found");
	exit();
}

header("Content-type: text/css; charset: UTF-8");
// Enable GZip encoding.
ob_start("ob_gzhandler");
// Enable caching
header('Cache-Control: public');
// Expire in one day
header('Expires: ' . gmdate('D, d M Y H:i:s', strtotime($cache_time)) . ' GMT');
// Set the correct MIME type, because Apache won't set it for us
header("Content-type: text/css");
// Write everything out
echo($buffer);
exit();
