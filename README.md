Launchsite framework

1) Download and unzip in your project folder (PROJECTROOT)

2) Run php PROJECTROOT/launchsite/public/index.php create_site

3) Point your web server document root to the newly created public directory

If you would like a database (mysql, postgresql supported) update the config at PROJECTROOT/app/config/config.php with your database details
You can then run php PROJECTROOT/launchsite/public/index.php create_db DBNAME
This will create a database and some basic tables.

If you would like to use migrations, you will need to use mysql

More setup commands can be found in: PROJECTROOT/app/config/routes.php